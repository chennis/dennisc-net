XML_PATH=build/writing/rss.xml
echo \<?xml version=\"1.0\" encoding=\"UTF-8\"?\>\<rss version=\"2.0\"\>\<channel\>\<title\>Dennis Chen\'s Writing\</title\>\<link\>https://dennisc.net/writing\</link\>\<description\>Content in the writing section of Dennis Chen\'s website\</description\>\<language\>en\</language\> > $XML_PATH 

find content/writing -print | grep .md | while IFS= read -r p; do
	# We don't want to generate an RSS entry for the overall index
	if [ ! $p = 'content/writing/index.md' ]
	then
		URL_PATH=$(echo $p | sed 's/content\/writing\///' - | sed 's/.md//' - | sed 's/\index//')
		pandoc -f markdown-smart --template=rss.template $p --variable path=$URL_PATH --variable date="$(date -d $(yq --front-matter=extract '.date' $p) +"%-d %b %Y")" >> $XML_PATH
	fi
done

echo \</channel\>\</rss\> >> $XML_PATH
