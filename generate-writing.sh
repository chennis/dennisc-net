# Args:
# $1 Path to content directory
# $2 Title
# $3 <desc> tag value
generate() {
	if [ $1 = "content/writing" ]
	then
		OUTPUT="build/writing/all.html"
	else
		OUTPUT=$(echo $1/index.html | sed 's/content/build/' -)
	fi

	HTML="$(find $1 -print | grep -E \.md | while IFS= read -r p; do
		# This "if" guard is only useful for not matching content/writing/index.md, which does not need to be auto-generated.
		if [ ! $p = 'content/writing/index.md' ]
		then
			URL_PATH=$(echo $p | sed 's/content\/writing\///' - | sed 's/\/index//')
			pandoc --template=writing.template --variable path=$URL_PATH --variable date="$(date -d $(yq --front-matter=extract '.date' $p) +"%B %-d, %Y")" $p
		fi
	done)"

	echo "<ul class=\"posts\">" $(echo "$HTML" | sort -k 4,4r -k 2,2rM -k 3,3rn) "</ul>" | \
		pandoc --template=html.template --variable pagetitle="$2" --variable desc="$3" |
		sed 's/.md//' - \
		> $OUTPUT
}

generate content/writing "All Writing" "All of Dennis Chen's writing."
generate content/writing/essays "Essays" "Dennis Chen's essays."
generate content/writing/tech "Tech" "Dennis Chen's tech writing."
generate content/writing/blog "Blog" "Dennis Chen's blog."
