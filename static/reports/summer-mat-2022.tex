\documentclass[points=false]{bounce}

\setcounter{secnumdepth}{2}

\title{The MAT Contest Report}
\author{Dennis Chen}
\date{Summer 2022}

\begin{document}

\maketitle

This is the second year we are running the MAT. We've learned a lot from the missteps we've made through MAT 2021, and I hope to share some of these lessons --- and the ways we applied them --- with you.

\section{Areas of Growth (i.e. website)}

\href{https://mat.mathadvance.org}{mat.mathadvance.org} is still a living nightmare. The frontend design, though generally sound, has major inconsistencies that come with hacking together Material UI and Tailwind rather than properly using a CSS stylesheet like everyone else.\footnote{For an example of how to do CSS properly, see \href{https://mathadvance.org}{mathadvance.org} proper.}

Largely this is because I haven't had the time or motivation to specifically work on the MAT website lately. I've also been the only developer in Math Advance up until now, which means that I've been pretty spread thin on our projects (and this is part of why \href{https://dennisc.net/writing/blog/mast-hiatus}{MAST has been placed on indefinite hiatus}). However, ever since I posted \href{https://dennisc.net/writing/tech/mathadvance-rust}{Math Advance is looking for Rust developers}, several talented people have offered to pick up one of our projects: MAT's website will finally be rewritten, though not solely by me, and mapm is receiving renewed attention in the form of an internal redesign and possibly a GUI.\footnote{I remain pessimistic on this front because the story of ``LaTeX in GUIs'' has been really terrible. Don't even get me started on Asymptote...} Our doors are always open for more people, as long as you're willing to work with \href{https://www.mathadvance.org/contribute}{our development ethos}.

\subsection{Future Plans}

Like OMMC, we intend to split our main marketing site and the contest portal in two. So maybe something like mat.mathadvance.org versus portal.mathadvance.org, or something of the sort.

We might build mat.mathadvance.org very similarly to dennisc.net, although the complexity of the MAT website may be high enough to possibly merit a SSG more complex than hacking together Pandoc themes. However, because we plan to make the design more consistent, the complexity will also decrease (though this is mostly internal, not visual).

Much more interesting, and definitely much more difficult, will be writing a proper contest portal. The technologies we use will look a lot like Glee's: Postgres as the sole database, Rust to handle backend logic and server-side generation of HTML. We'll use email/password authentication (no usernames) for contestants, because usernames are redundant (since there isn't public communication like a forum, users do not need to ``express themselves''), and we'll also do the same for administrators.

However, unlike most other websites, we do not plan to store administrators and contestants in the same database. This is largely because contestant registration will be the same song-and-dance as usual: register for an account and verify your email. However, for administrators, registration, email verification, and granting of permissions will happen in the same step: an existing administrator will send an invitation to the new one, which will be delivered via email (hence email verification), and through this invitation the new administrator can create their account. This is identical to Glee's registration process and is the model I plan to use when registration automatically should confer permissions.

By the way, we might host contests besides the MAT, and possibly even contests created by people other than Math Advance. If you are interested, I will probably be posting updates on my personal website if/when we make progress on our portal.

\section{Awards}

Our livestream was apparently a huge mess this year. The oversights we made were obvious in hindsight, but I will state them here for the record so we don't get bitten next year.

I should've checked who was actually available for the stream and given access to the people who were planning to host the stream.\footnote{If I was available for the awards ceremony, which I will probably be next summer, this would not be an issue.} This was largely a miscommunication issue and I don't know how reliably it can be sorted out in advance since plans change.

We had a stream to make sure everything was in order, but it should have happened days before, not hours.\footnote{Anything earlier is unrealistic.} Especially since YouTube randomly decided to only enable livestreams 24 hours after my channel requested it, which was too late for the scheduled time.

I don't know what happened on stream in detail, so no comment there.

\section{Tiebreakers}

I want to briefly mention a minor debacle that happened with tiebreakers this year: two contestants got the exact same score, distribution, and tiebreaker time (said time was 30:00), and they both scored within the top 25. Fortunately they didn't score top 10, so we didn't have to care about actually tiebreaking them (which is impossible).

This brings me to a fundamental issue with math contests and caring about rankings: if two people submit the exact same test, which is always possible, there is no way to break that tie. Of course, it is rare that this happens in practice at a high enough ranking to actually matter. Most contests have cursed weighting algorithms (think HMMT) and enough problems to almost guarantee that this situation doesn't happen, but MAT is a small enough contest where this is a realistic (though small) risk. The biggest contributor to this risk is that people's brains shut down: obviously at 29:00 it's almost always optimal to just submit the two problems you already solved, but most people won't be thinking strategy during the contest, they'll be thinking math.\footnote{Which is why simple contest formats are always ideal, unless strategy is meant to be one of the primary focuses of your contest --- think the \href{https://www.treecontest.org/}{Tree Contest}.}

It's not a big deal and I doubt contestants will ever get salty that they have to split (say) the 4th and 5th place prize, but if anyone has a better design for MAT tiebreakers that won't be too much of a departure from the current format, let me know.

\section{Fixups}

We've made some pretty significant mistakes with the inaugural MAT last year, and this time around I'm proud to say we've repeated none of them. I consider this year's MAT to be an astounding success. Even though we had 110 submissions, 40 less than last year (all numbers are approximates since I'm too lazy to find the exact ones), the other statistics are much healthier.

50\% of contestants got strictly more than 3 points, and only 7 contestants got 0's. The latter statistic represents a remarkable retention rate, and I'm glad that more participants came away with a positive experience. In fact, the drop in low scores and participants coincide so closely that the latter could almost be considered a positive development.

Of course, we will need to try and get more participants. 200 is my personal goal for the Winter MAT, and I hope to reach a 4-digit contestant count with it eventually.

\subsection{Errata}

None!

\subsection{Difficulty}

MAT has always been an AIME-level contest and always will be.\footnote{More precisely, the Summer MAT --- more on that later.} However, the reception to last year's test largely showed the test was inappropriately hard. I want to specifically call out P2, P4, P5, P7, and P8 from last year's contest as being too hard. Out of the main 9, that is over half of the problems. This year, the difficulty was substantially lower: P2 can be mistaken as easy by an experienced geometer, P4 is more doable than last year, P6 is quite intuitive, P8 isn't impossible, and P9 is fairly easy compared to last year as well. There's been a general decrease of difficulty throughout, though we were careful to not overdo it and make MAT a completely different contest.

However, in the future we intend for problems 3, 6, and 9 and tiebreaker 3 to be generally somewhat harder than they were this year. The reason for this is twofold: first, we have a lot of good hard problems that we want to release, and second, our focus on accessibility/outreach/etc will primarily be served by the Winter MAT from now.

Speaking of which...

\section{Single Points of Failure}

Currently I am a single point of failure for Math Advance in many ways, and this has been made especially apparent since I've been on vacation away from my SSH/GPG keys for the contest and the weeks before.

Largely this stems from technological issues, such as my server being a total mess (why on God's earth is it still running Ubuntu?), nobody having SSH keys to the server besides me, and Firebase being an absolute nightmare to work with. However, the larger and more fundamental issue is that nobody knows how to do tech stuff.

I need to teach someone who works with the core of our operations how to administer a system in general as well as how to administer our system in particular. Fortunately this time around I was able to keep in touch and do some very elementary system administration from my phone, and other elements just happened to not require manual intervention (read: they didn't break on me). However, none of these things are guaranteed next time, and relying on good fortune is not a sustainable strategy for a nonprofit. Safeguards must be put in place next year.

\section{Winter MAT}

I don't know whether we've officially announced this yet, but Math Advance is de facto not making any more mock contests (probably). This means that we'll have an excess of problems. We've also wanted to make an accessible contest for beginners that actually has good problems.\footnote{This is what the AMCs purport to be, but I think the AMCs are slipping in accessibility. Even if they aren't, the more the merrier, right? Two contests a year doesn't sound like a whole lot.} Thus, we'll be offering an easier version of the MAT next winter. It won't have tiebreakers since rankings won't be important in this competition, and because keeping the contest shorter makes it more accessible and decreases logistical difficulties.

The Summer MAT and Winter MAT should be considered separate contest types much the same way HMMT November and February are. Granted, the two MATs have more similarity than the two HMMTs, but they are not the same contest by any means either. Thus the two should be sourced distinctly. Any sourcings that look like ``MAT 2021/9'' should be updated to ``Summer MAT 2021/9'', etc.

The Winter MAT will be like the Summer MAT with every problem shifted down one spot on average. Of course, the beginning problems don't have that much room to shift down, so the difference will mostly be made up for in problems 3/6/9. Compared to this Summer MAT, Winter MATs might be something like half a problem easier, but we also plan to make Summer MAT slightly harder after this year, though it will never return to 2021 levels of difficulty.

\section{501(c)(3) status}

A big change from MAT last year and this year is that we officially hosted it as a 501(c)(3) non-profit this time around --- we have state and federal recognition. This allowed us to get \$500 from Jane Street, because their corporate policy (and the policy of pretty much every company) is that they can only donate to 501(c)(3)s.

Becoming a 501(c)(3) is a serious decision and should not be taken lightly. It is a significant undertaking, so it does tend to separate out the groups that last about a year from the ones that last longer. Unless you intend to consult overpriced lawyers that you cannot afford, you will have to gain an understanding of tax laws. Fortunately, the relevant laws and forms you need to fill out are not too much of a significant overhead if you've already filed your taxes (which every functional adult in the US has to do anyway).

Applying for federal recognition is also quite costly and it is difficult to recoup your expenses. The benefits sound impressive but in reality are somewhat limited: major institutions can send you money only if you are a 501(c)(3), your organization can officially give out volunteer hours, and it maybe helps with your college applications if you played a significant role in founding the organization. These benefits sound numerous, but they are fairly situational and in many cases you can get a better return for your time and money. 501(c)(3) status has represented significant overhead for us (although I think we still would be a 501(c)(3) if we could do it all over again), and for most people it is just not worth the time, energy, and money. Only apply if you \emph{know} you will be working with this organization for at least 3 more years or unless you know for a fact that you will have a successor ready by then.\footnote{If you can't name any names right now, you don't know.}

\end{document}
