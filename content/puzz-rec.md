---
title: "Variant Sudoku: Where to Start"
desc: Are you a newbie tired of the way-too-easy "diabolical" computer generated puzzles on sudoku.com? Do you want to take a step into the world of handcrafted sudoku puzzles? Then this page has (some of) the info you'll need.
---

So you're interested in variant sudoku but don't know where to start? Well, here are my recommendations; take what you want and leave the rest. I'm assuming you know the rules of common variants (at least killer sudoku), if not, you should read them on my [common variants page](sudoku-variants).

I generally recommend newbies start with killer sudokus, because the challenges they pose are not really with scanning or logic chains, which are hard for most people to spot. Killer sudokus are about bounding and sets. For an example of bounding, a 6-cell 23 cage must have a 1, 2, 3, and 4 because if it is missing any of of these digits, the total will be greater than 23.[^co23] And for an example of sets, a 21 cage must have two digits in the range 7-9 and one digit in the range 4-6.[^nashville] The logic in killer cages is varied, approachable, and yet if you do enough of them, it will start to become familiar.[^setting]

[^co23]: Which I use to great effect in [Class of 23](https://logic-masters.de/Raetselportal/Raetsel/zeigen.php?id=000CZD).

[^nashville]: Which ICHTUES highlights in his puzzle [Nashville](https://logic-masters.de/Raetselportal/Raetsel/zeigen.php?id=000A9O).

[^setting]: For similar reasons, I think the easiest variant to use when constructing your first puzzle is also killer. But if you have a specific idea with a different constraint, obviously go for that instead.

So in order of ascending difficulty, here are a couple of Killer Cages puzzles I recommend (any puzzle without a specified author is by me, and can be found on [my puzzle page](/puzzles)):

- [El Paso](https://logic-masters.de/Raetselportal/Raetsel/zeigen.php?id=000B9Y) by ICHTUES
- [Stairs](https://puzz.dennisc.net/#stairs)
- [Mesa](https://logic-masters.de/Raetselportal/Raetsel/zeigen.php?id=000CJ9) by ICHTUES
- [Wings](https://puzz.dennisc.net#wings)[^kropki]
- [Highway](https://puzz.dennisc.net/#highway)
- [Austin](https://logic-masters.de/Raetselportal/Raetsel/zeigen.php?id=000CEP) by ICHTUES
- ['Weven](https://logic-masters.de/Raetselportal/Raetsel/zeigen.php?id=000CUK) by riffclown
- [23 Killer](https://logic-masters.de/Raetselportal/Raetsel/zeigen.php?id=000CNL) by Florian Wortmann
- [Class of '23](https://puzz.dennisc.net/#co23)
- [27 Killer](https://logic-masters.de/Raetselportal/Raetsel/zeigen.php?id=000D3X) by Florian Wortmann
- [Indianapolis](https://logic-masters.de/Raetselportal/Raetsel/zeigen.php?id=000BZB) by ICHTUES

[^kropki]: Okay, there's *one* black dot in this puzzle, but it's close enough to a pure killer.

After doing a couple of killer cage puzzles, you may want to try some other variants like Thermo, German Whispers, etc. In fact, you know what? Here's a tier list of variants by approachability for beginners.[^serious]

[^serious]: Please don't take it too seriously, just do whatever puzzles look appealing to you. This is just reflective the order I think most people get introduced to constraints (an order which does have good reason). I'm actually mostly doing this for fun and not informational purposes, but shh.

## Variant tier list, by approachability (not necessarily puzzle quality)

### S

- Killer cages. It gets its own tier because it is just that good, for reasons I have already explained.

### A

- Thermos. A lot of bounding can be done, and frequently thermos will make for pairs/triples/etc. I recommend [Hooks and Swipes](/puzzles/hooks-and-swipes/sudokupad).
- German Whispers. Alternating high/low and banning 5 on the line can give you a lot of info.
- XV. Especially in conjunction with German Whispers, it gives a lot of information about high/low digits and frequently is used in pretty cool coloring puzzles.[^coloring]

[^coloring]: In a coloring puzzle, you just color squares with the same digit the same color, and only near the end do you actually figure out what digit each color corresponds to. It is less hard than it sounds.

### B

- Anti-knight. A lot of scanning, but pretty simple to understand.
- Anti-king. Less scanning than anti-knight, but not as common.
- Arrows. Particularly with fairly long arrows, the arrow bulb will contain a pretty big number and that is fairly restrictive.
- Kropki. Pretty restrictive when used in a break-in but never serves as a red herring since it's fairly obvious when they are useful. Parity can be used for a tricky break-in though.
- Odd/Even. This one is simple and can be used to create an almost-vanilla puzzle (if it is the emphasis).
- Quads. Literally free pencil marks, not too complicated usually.

### C

Anything in C is usually there just because it can be calculation intensive. I hope you know your triangular (and triangularish) numbers well...

- X-Sums.
- Sandwich.
- Little Killer. A lot of bounding, whether it be from the top or the bottom, and analyzing degrees of freedom.
- Between Lines. Logic involving them usually has a lot of cascading, which makes for super cool puzzles, but also might be daunting for a beginner.
- Renban. While it has a similar set restriction as killer cages (for example, a 5-long renban line must have a 5), the geometry is more free and its break-ins more subtle. See also the cascading comment for between Lines.

### D/F Tier

Nothing. That's why all these variants are common; it's because their logic is approachable enough that any solver (and setter) will be able to pick up these familiar tools and use them to solve (or set) a puzzle with unique new logic.

Once again, do not take this list too seriously, if you see a puzzle and it compels you to solve it, just solve it, because this list has a lot of generalizations.
