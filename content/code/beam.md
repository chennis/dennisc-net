---
title: Beam
desc: An informal specification for math presentations, plus some implementations of it that generate beamer slideshows.
---

## Implementations

There are implementations in C and Rust. You can track their development at the following links:

- [Beam Rust Git Repo](https://gitlab.com/chennisden/rust-beam)
- [Beam C Git Repo](https://codeberg.org/dennisc/beam) (I don't recommend you use this yet, because it's poorly written. As proof it doesn't work with musl libc. Also it implements an earlier version of the spec. Maybe this summer I will fix it.)

## Specifications

**The spec is not finalized and anything can change at this point.**

### Slides

- A new slide is started with the `~` character. Anything that goes after `~`
  in a line sets the title of the slide.
- Leading and trailing whitespace in a line is ignored.

### Special characters

- `#` will start a new section with the text that follows it on the same line.
- `~` starts a new slide, and the text that follows sets the title of the slide.
- `-` can be used to make a list of bullet points. Under the hood, it calls the
	"itemize" environment in LaTeX.
- `>` starts a block. The first line is used as the title (leave it blank for no title).
- `<` starts an exampleblock. Again, the first line is used as the title.
- `!` starts an alertblock. Again, the first line is used as the title.
- `@` sets the background image of the slide. It is possible, but
	discouraged, to add text to such a slide. Under the hood, it calls
	```
	\setbeamertemplate{background}{
		\includegraphics[width=\paperwidth,height=\paperheight]{IMAGEPATH}
	}
	```
- `*` can be used to insert an image into the slide. Under the hood, it calls
	```
	\begin{center}
		\includegraphics[width=6cm]{IMAGEPATH}
	\end{center}
	```
- `^` will directly input text verbatim into the preamble. This lets you set
	the theme, title, and author of a presentation. If a line starts with `^`,
	it will end the current slide, without exception.
- Character escaping is handled by LaTeX, not beam. That means you cannot have a
	line that starts with a raw `-`, `!`, or `>` character. This is
	regarded as a design flaw, but a very minor one.

You can also use % to act as a comment, since it's also a comment in LaTeX. Be
careful not to start a new slide by accident though.

## An Example

```
^ \documentclass[12pt]{beamer}
^ \title{beam}
^ \author{Dennis Chen}

\titlepage

# Introduction

~ What is beam?
- beam is a file format to write presentations with
- beam is a program that converts beam files into beamer

~ Why?
beam does for beamer what Markdown does for HTML
- beamer takes far too long to write.
- We only need a subset of beamer's features

# Features

~ Frametitles
Use the \textasciitilde character to set the frametitle.

~ Bullet points
- You've already seen it in this presentation.
- Bullet points invoke the \emph{itemize} environment.

~ Block
> How to make a block
> This symbol makes a block.

~ Exampleblock
< How to make an exampleblock
< This symbol makes an exampleblock.

~ Alertblock
! How to make an alertblock
! This symbol makes an alertblock.

~ Images
Use the @ character to set a background image.

~
@beach.jpg

~
You can also use the $\ast$ character to include an image in the slide.
*beach.jpg

# Miscellaneous

~ Stylization
beam should be written in all lowercase, even at the start of a sentence.
```

## Why not pandoc?

For those of you who don't already know that pandoc exists, it's the mainstream solution for the problem that beam solves. It uses the much more ubiquitous Markdown format as well. Why would I ever make beam then? Pandoc is actually a really good solution, but here are a couple reasons:

- Pandoc is 500MB and comes with a lot of other features, none of which you will ever use or care about.
- Pandoc beamer does not have a spec.
- It is awkward to make example and alert blocks in Pandoc: `## Exampleblock {.example}` 
	- On the flip side, these headers could probably be extended to `## Theorem {.theorem}` for the math-specific environments. I don't think they have been (I wouldn't know since there is no spec).
- Pandoc has over 10,000 lines of code. An implementation of beam is very unlikely to exceed 500.
- Pandoc forces you to give every slide a title. In practice, this makes for a lot of filler titles if you're trying to write fast.
- Pandoc might be interoperable with Markdown, but the minute metadata is introduced, it stops being universal (although you can strip it out and make it universal again). But has anyone ever wanted to convert the **exact same content** from a slideshow to, say, an article or vice versa? That wouldn't work, because articles and slideshows are written totally differently. 
- Due to the way Markdown works, if you want a block in a slide, it's got to be at the end.
