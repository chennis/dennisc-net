---
title: TeX packages/classes
desc: My TeX libraries and user-facing classes.
---

With regards to installation and usage,
if you are using Overleaf and want my help,
I will take a page from Evan Chen's book:

> First install LaTeX properly,
> then if it still doesn’t work write to me again.

Otherwise, feel free to email me at [dchen@dennisc.net](mailto:dchen@dennisc.net).
Discord or Hangouts work as well, if you know how to reach me there.

## Utility packages

These are some utility packages I've written.
Rather than directly changing the output,
they provide new logical constructs for you
to write your own TeX in.

- [scrambledenvs.sty](https://gitlab.com/chennis/scrambledenvs)

  It provides a macro to declare scrambled environments,
  e.g. scrambled hints or solutions.
  It was written to serve as a replacement to Evan Chen's
  [code for his geometry book](https://web.evanchen.cc/faq-latex.html#L-13).
  Install instructions, for the less LaTeX proficient out there, can be found on
  [this GitHub Gist](https://gist.github.com/chennis/84c2224b10f0b177e4726db0bec45381)
  (though it is fairly out of date -- among other things, it points to a GitHub repository,
  not GitLab).

- [palette](https://gitlab.com/chennis/palette)

  Provides tools to create and easily swap out color palettes.
  This package is meant for designers to use.

  The package provides `colorpalette.sty` and `symbolpalette.sty`.
  (If it was up to me, I would have called the latter `symbolsuite`,
  but then the name would not be at all relevant to "palette.")

## Classes

I've also designed some document classes which provide semantic environments
(such as theorem, proof, and solution environments),
as well as changing the overall document output drastically.
These classes are not documented at all,
so my suggestion is that you email me for an example `.tex` file if interested.

If you are in Math Advance,
you will notice that the contest and book classes are not available here.
While I am a big proponent of open source,
I'd like to have some control over where my professional designs come up.
If you want to study the code, please just let me know!
I'm willing to send over snippets on an individual basis.

I don't mind at all if you use my personal classes below for any reason.

- [mast.cls](https://gitlab.com/mathadvance/tex/mast)

  The class we use for MAST. Formerly known as `lucky.cls`,
  I separated this from my other classes because... I just don't use them.
  Also, this is now basically an exclusively mathadvance class,
  so it was moved to mathadvance's GitLab.

- [bounce.cls](https://gitlab.com/dennistex/bounce)

  The document class that I recommend and people seem to like the most.
  True to its name, the design is based on a theme of bounciness.

  Each piece of the class can be taken apart.
  For instance, if you just want to import
  and use the boxes, you can type `\usepackage{bounceboxes}`.

- [denniscv](https://gitlab.com/dennistex/denniscv)

  This is the document class I use to typeset [my CV](cv.pdf).
  The [TeX source](cv.tex) is also available.

- [chennistex](https://github.com/chennisden/chennistex)

  These are my old personal classes which will receive limited support.
  (You can tell this is the case because the redirect isn't getting moved to GitLab.)
  It is strongly recommended that you use `bounce.cls` or `mast.cls`
  unless you have a specific reason to use one of these classes.

  Currently, the code for `lucky.cls` is used in `mast.cls`, with minimal modifications.

## Posts

These are some LaTeX posts on my blog that may be useful for reference. Also includes some Asymptote.

- [LaTeX for Beginners](/writing/tech/latex)
- [Why latexmk?](/writing/tech/latexmk)
- [A High-Level Overview of TeX](/writing/tech/tex-overview)
- [Asymptote is the best Asymptote compiler](/writing/tech/asy)
- [Drawing right angles in Asymptote](/writing/tech/asy-right-angles)

Posts are listed in a suggested read order (start with the top).
