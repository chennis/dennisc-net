---
title: Code
desc: Some projects that I've written.
---

These are some projects that I've written.
Most of them are in TeX right now,
but there are a couple of other things in the works.

Most code is on [my GitLab](https://gitlab.com/chennis), but I plan to move everything to a personal instance of [Glee](/writing/tech/glee-design) once I've created it.

## Subpages

[TeX](/code/tex)

[beam](/code/beam), a program that allows you to write Beamer slides in Markdown-like syntax and compile it to a PDF.

## mapm

[mapm](https://gitlab.com/mathadvance/mapm)
is a problem management software
that makes it easy to prototype
different drafts of contests.

The [documentation](https://mapm.mathadvance.org)
and [my post](/writing/tech/mapm) are fairly comprehensive,
so I won't elaborate much on it here.

## Web

- [This website.](https://gitlab.com/chennisden/dennisc-net)
  
  Uses plain HTML and CSS, zero JS involved.
  Built with Pandoc and Bash, hosted on my own server
  with NGINX as an HTTP reverse proxy.

  This codebase is good and unlike MAST-web,
  I *do* recommend you emulate this website's code.
  (This doesn't necessarily mean the CSS stylesheet,
  content, or structure, just the way it uses Pandoc
  and Bash scripts.)

- [mathadvance.org](https://gitlab.com/mathadvance/mathadvance.org)

  The main Math Advance website. The above description applies as well.

- [mast-web](https://gitlab.com/mathadvance/mast-web)

  This is the MAST website.
  The frontend and API are written in Next JS,
  and backend has MongoDB and Redis added onto it.

  The codebase is horrible,
  please don't try to replicate whatever's in it.
  I plan on rewriting it *eventually*.
