---
title: The difference between my essays and college apps
date: 2022-08-27
---

Whenever I write an essay it's structured like this: I've got a point and I'm bringing in anecdotes about myself to back it up. The general point I'm trying to make is front and center.

But for college apps I'm supposed to focus on myself, using anecdotes about myself and making observations where I can.

So the difference is, rather than writing "this is my takeaway, here are some stories" it has to be "this is my story, here are some takeaways". It's a little difficult for me to write like that since I do it so rarely.
