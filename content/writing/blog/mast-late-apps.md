---
title: MAST Late Applications
desc: On opening MAST late apps.
date: 2022-04-21
---

I've decided to open MAST late apps for all people who satisfy all of the conditions below:

- Qualified for AIME
- They (NOT their parents) email me

This is for a couple reasons:

- Only AIME qualifiers have even asked me to join MAST (obviously there might be some selection bias since AIME qualifiers are far more likely to be confident enough to ask). But I don't think MAST will be particularly helpful for non-qualifiers anyway, and we will probably hold summer classes for less experienced contestants anyway.
- Emailing me demonstrates that you show some non-zero initiative of your own. I think communication is a lot easier when you don't have to play telephone through parents (so I typically do not bother).

MAST late apps will be open throughout the entire year! You won't be able to submit Season 4 late applications once Season 5 starts (you can replace 4 and 5 with n and n+1 in general), but otherwise, there are no deadlines.

Obviously this is not a blank check to ignore the regular application period. But if you learn about MAST in May and have already made the AIME, I see no reason to stop you from joining the program.
