---
title: Quality is the sole value of Math Advance
date: 2023-01-01
---

A few days ago, I happened to see yet another group of students who decided, "hey, let's try to write a math contest in [absurdly short period of time]." This is so ridiculous that I decided I finally had to say something about it. Too few organizations care about quality. Too few organizations actually set *standards* for themselves, and instead they just push out whatever seems like a good idea at the time.

The *mission* of Math Advance is to, well, advance math education. (It's in the name.) But it is not one of our *values*, and the difference is important. Because our mission is something we can compromise or change, something we can scale down on depending on circumstances, etc, while when it comes to values, we refuse to compromise.

If our unrelenting goal was to, say, introduce math contests to more people, we would be writing even easier math contests. But almost as a rule, those "super easy" math contests are almost entirely made of filler. I have the same opinion of those math contests as I do for computer-generated Sudoku puzzles: they can be mass produced, are typically somewhat lacking in quality, and while they can be used for learning, *I* am not touching it with a 10-foot pole.

That's the same opinion that Math Advance as an organization holds. And our goal is not just to produce *good* math contests or *good* teaching materials. It is not even to produce *the best* of something, because that standard can sometimes be woefully low. Our goal is to produce contests (for now, and maybe something else later on) *as good as possible*. This is why for the first Summer MAT, we spent almost a year constructing the test despite it only having 12 problems, and why we asked close to a dozen people, completely independent of the test-setting group, to provide feedback. Sometimes we will have to compromise other things to ensure quality: sometimes we have to reduce scope, as we did by putting MAST on hiatus, and sometimes we have to delay the release date of lots of good work (we have so many backlogged problems that we could probably run for 3 or 4 more years if no one proposed another problem from now).

When two values conflict, you are going to have to compromise on one of them. That is why the only value we have is the only value we need: *quality.* I believe there is a role for people who mass produce math contests and math problems. But if you, just like us, are sick of mass-producing problems for mass-produced contests that become irrelevant after two weeks, we have a place at you for Math Advance.
