---
title: Puzzle goals, part 3
date: 2023-03-01
---

It's March 1st, and my February goal was either a success or failure, depending on how you think about it.

It was a success in that I further developed my range with standard variants, like killer, diagonal, thermo, etc. It also was a success in that I wrote using 4 variants I never used before: diagonal, quad, X-lines, and starry sums. Two of these are new variants invented by me: X-Lines and Starry Sums, and I particularly think the second has a lot of potential.

It was a failure in the sense that I didn't use four new standard variants, so my range of setting does not even encompass many of the common variants. There's a lot I'm missing: sandwiches, between lines, etc, that just feel out of my alleyway right now (which I suppose is why I never set them). I am not too concerned about this fact though.

So supposedly I'm slated to write a classic sudoku this month, but a couple of reasons this might not happen:

- I'm interested in writing a Monster Cages puzzle, where the clue indicates the product of the sum of shaded cells and unshaded cells. (Or I can make it a Chaos Construction and make clues indicate the product of the region sums... that would be kind of cool.)
- Also my latest puzzle, "Intersections", has an obvious dual: "Mind the Gap", where X-Sum clues in the same row/col add to less than 45. I don't know if including diagonals would be very thematic so I won't use them.
- I also want to start learning more math/CS in preparation for college next year.
- I'd like to reignite some activity and come back to teaching in Math Advance.

So who knows if it'll happen in time. And as for the April goal, probably I will not end up writing a crossword. I don't really get how they work as a solver so my debut as a setter still seems pretty far.
