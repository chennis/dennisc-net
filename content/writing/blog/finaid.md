---
title: Regarding financial aid in my math courses
date: 2023-08-17
---

On the subject of financial aid for math classes: I used to say something along the lines of,

> "My parents don't want to pay" or "I ran out of allowance" is not going to get your tuition waived. If you have genuine financial need, then of course that is a different story.

In some alternate universe I would've started charging for classes while saying this. In that alternate universe, someone who needs financial aid might not have applied for my class, or worse, they would have paid for it. And this fall, I *am* charging for classes.[^almost] So let me clear this up for the record.

[^almost]: I almost wrote the same thing this year too, until I realized it was a bad idea. In fact, that's why I'm writing this post.

You know, I don't like charging students extortionary fees.[^parents] In fact, I charge a nominal fee for two reasons: so everyone takes the class seriously, and to raise funds for Math Advance.

[^parents]: Parents who try to get me to teach their kid are a different story; if I'm feeling mean, I might just quote you for a hundred an hour. Read the [FAQ](/faq), people!

I have bad memories of debating whether to pay 1500 a semester for some AIME classes and regretting it dearly, and I don't want to put anyone else in a similar situation. Most of my students are upper-middle class Americans who can see a price tag of $100 and say, "wow that's pretty cheap let's go for it" without a second thought.

But I recognize this is not everyone.

There are countries where 100 dollars goes a lot further than it does here. People have individual circumstances. The particulars are none of my business.

In any case, the message I was inadvertently sending was that I was concerned about abuse. And well-intentioned students might think, "wow there is a problem, and I should avoid trying to be part of it".

But there is no such problem. I can think of only one instance where someone acted in bad faith in my classes (a kid posted a handout on CourseHero, hardly what I'd call a systematic pattern of abuse), and zero from my work at Math Advance.

I don't mean to brag, but I will anyway: our program attracts good students. Really good students. Many of them qualify for MOP. It wouldn't surprise me too much if we start seeing IMO qualifications soon. And all of them are hardworking, honest students who respect the program and leave it better than they found it. If you are a prospective student reading this, then this description probably applies to you too. I will say it again: we attract good students, and you hopefully will be among that number. Not a single one of our students has turned out to be a bad actor, and if you are worrying that you might become the first, you most probably are not.

I am rather more concerned that there will be a lack of *use* than a pattern of abuse. So if you think you need financial aid, you need financial aid. And you will get it, no questions asked. Very rarely will a program accept "trust me bro" as a reason for financial aid, and for good reason. But for better reason, we do.

To make a long story short: if you think you maybe might kinda probably sort of need a little financial aid, then you should ask for financial aid. We need to be more proactive in making sure not all our students are upper middle class Americans, and this post is the first step.
