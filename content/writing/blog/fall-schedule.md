---
title: Schedule for Fall 2023 course
date: 2023-09-10
desc: Our Fall 2023 AIME course will occur on Sundays at 5 PM - 6 PM Eastern.
---

The schedule for the Fall 2023 AIME course has been finalized: Sundays 5 - 6 PM Eastern. Class starts on September 17th.

Also, if you are a student reading this and have not received any emails from me containing general advice or an invite to the class Discord, please email me at [dchen@mathadvance.org](mailto:dchen@mathadvance.org).
