---
title: Committing to CMU
date: 2023-04-22
---

So today I committed to CMU. I was deciding between UT Austin (Turing Scholars) and CMU. Some details:

- both CS
- UT Austin Turing actually has a higher mean salary than CMU (caveat: methodology for comparison was questionable)
- money isn't a *major* issue

Either way, I don't think I would've made a *wrong* decision. But I'm only ~40% sure that I made the *better* decision, for a couple of reasons:

- parents played a big influence in where I'm going. In some hypothetical world where my choices were MIT vs. Harvard, I'd be like "ew no I hate Harvard I am definitely going to MIT", and the strength of my preference would be enough to override any hypothetical parental desires. If my choice was made totally independent of their influence, I would almost certainly have chosen UT Austin, but I was barely more sure that UT was the better choice. Well, at least my mom will get to enjoy her Asian parent WeChat group.
- apparently CMU is really fucking small and that sucks. In some sense, it's cool that people density is higher, so you can just hang out with people and not be lost looking for another human soul, but I'd also be able to explore campus and still find something new even in my fourth year. With a school only four times the physical size of my high school, that's just not very likely. At least Pittsburgh is pretty big, so I can just step off campus and go downtown.
- CMU allegedly induces depression. Obviously this is overblown, but where there's smoke there's fire.

But hey, there's lots to be excited about too in CMU. For better or worse, there's no turning back at this point, so might as well be optimistic and make the most out of it. Some part of me is thinking of putting in a transfer application to MIT, but by the time I get there I'll probably really enjoy CMU and be too lazy to fill it out.

Also one of my friends from school is going to CMU, so that's cool too.

### UCL Rejection

I couldn't really fit this anywhere, but I felt that college talk was the best place to put it. I got rejected from a British university I applied to a long time ago, University College London. At this point I didn't even care enough to have a reaction either way, and almost decided not to check. (Which I did to Irvine, and I honestly might not have known they accepted me if they didn't email me about their honors program.)

## Roommate rizz

So a lot of people have asked to room with me, which is pretty cool because I'll have a roommate for sure and there's a group of people I'll definitely be able to talk with during orientation (though I want to meet new people, especially outside of SCS!)

But of course, roommate rizz doesn't count unless you get *rizz* rizz. In high school it's always been some combination of "it's complicated", "circumstances", and "waited too long to take any actual initiative." But things will never be the same when I get to college. This will be my life goal that I value even more than that sweet sweet Jane Street internship 🫡

(To be clear, I'm mostly joking, but I should try and be more extroverted. Right now I'm this weird mix of extrovert and introvert, where I'm not afraid to put myself out there and have eyes on me, but at the same time I might find it difficult to talk to specific people I want to talk to because of *circumstances* or any other litany of excuses. Also at WARP I discovered that I get really tired after doing stuff with other people and need to hole myself in my room for a few hours afterwards, so *consistency* rather than *intensity* seems to be the correct approach here.)

And for no reason whatsoever, I feel like the Tepper kids are probably the most sociable/normal/etc. Which, I guess, is part of the job description...

## Track and Field

My track season ended with a whimper.

Three weeks ago at Dublin, I nearly fell asleep during warmups and got a terrible time on the mile.

Two weeks ago at the Amador Valley Spring Fling, I hit a decent PR and thought, "maybe I can bring this momentum forward and close my last season with a good-ish time."

A week ago at Granada, I made a few tactical errors in the race and could only match my PR.

And Thursday, I had a terrible cough and severe allergies. It wasn't as bad as Dublin, but it was noticeably slower than my PR and I felt like I was dying the entire race (moreso than usual).

So rather than closing the season with a big PR, or even modest gains, it was just very meh for reasons entirely out of my control. And I ended up not being able to run the fun events I wanted to because the pollen was making me bawl my eyes out and I could barely see the track, let alone run on it.

So how do I feel? Well, I'd probably care more if I was good at distance running. It kinda feels the same way as my 3 on USAMO last year --- pretty embarrassing, but I'm not sure I was really invested in the result at that point. I enjoyed the process, it taught me some pretty valuable lessons, and I'm a better person because of running. Or less fat, at least.[^6] Very much like college applications, I've come out through the other side and nothing really *terrible* happened, but at the same time, nothing great really happened either, and I'm still a little regretful about the whole thing.

[^6]: Over the summer my new goal is just to get a 6-pack. No reason in particular...

I do plan to continue running in college. I don't think I'll train as intensely as before at college, though I'll probably still show up for summer training when I'm in town this year. It's very possible that I've run my last 1600m race already, or at least one of my last three if some one-off miracle occurs and there's a track meet convenient enough that I can register for. I might try some actual distance races though like 5Ks, 10Ks, etc.

## AP Chemistry

Man, AP Chemistry was (is?) such a wild ride. To this day I have never done a practice MCQ, but based on my performances on FRQs I think I could probably get a 5. However I do lack some problem-solving experience, which is expected since I started self-studying literally less than two weeks ago.

Before I actually made a serious attempt, I was like "I have three weeks left I'm screwed, why don't I just give up." But then I tried for a bit, learned pKa/buffers/thermochem/the other stuff I didn't know pretty quickly, and now I actually think that a 5 is not only within reach, but honestly should be pretty easy. I'd told myself that you could learn the entirety of AP Calc BC in 2 months, the entirety of AP Stat in 2 weeks, etc, and I also [wrote about how most people suck at any given thing](/writing/essays/competence), yet I didn't really internalize either of these things until now.

Right now, I just need to memorize VSEPR and maybe review thermochemistry. That should be enough to get me a 5, although now I'm studying chemistry, it actually seems pretty interesting to me again :)

## Multivariable Calculus

My relationship with vector calculus has been a real doozy. First I thought, "huh the multidimensional analogs are pretty natural generalizations." Then I got to the four fundamental theorems of vector calculus: line integrals, Green's, Stokes, Divergence, and then I went "what the heck is this physics nonsense, this makes absolutely no sense and I still don't really know what an integral is why didn't I take real analysis," but now I'm learning that there's something deeper behind these four theorems.

Honestly I've been less engaged with the material than I hoped I'd be, and without a final I really might just check out mentally after the last unit test. Which is a bit sad, considering multivariable calculus is still math even if it's gross applied math with the applications (physics) neutered. But learning real analysis then vector analysis seems to be a better approach, skipping the non-rigorous calculation nonsense portion, and also discrete math is good to learn too. The opportunity cost of evaluating irrelevant integrals is just too high to justify actively doing it.

But we do have a final *project*, and it's something I've been thinking about doing for a long time. I've always wanted to write an article about numerical computation, history and present day included. Logs were invented so people could multiply big[^precise] numbers easily, but they did it first with trig tables. And the reason they did it was to perform calculations with astronomical data, which is how they derived Kepler's Laws. I think this is really cool stuff and want to share it with you guys too, and this has finally given me an excuse to do exactly that. I'm going to write the article, *then* actually care about meeting the project requirements, which are not too demanding anyway and won't compromise the article in any fashion.

[^precise]: Since size is relative (powers of 10 are irrelevant), what really matters is that these numbers need to be precise. This is one of the many misconceptions that my article will aim to clear up.

Also, have you ever wondered how your computer calculates (well really, approximates) $\sin 1$? It's not *just* Taylor Series, there's a lot that goes into it from the little I've seen. (This is going to be the present-day portion.)

## A trend

I've noticed that most of the time, I really don't share a lot of good news on my blog, and when I do there's always a tone of solemnity. This isn't too surprising, because nothing purely good *and* noteworthy has happened to me recently (life is rapidly getting more complicated), and even if it did, it probably wouldn't be interesting enough to write about.

I mean, since my life is about to be consumed by CS, it's very likely that my website turns into a math/CS blog for real. I don't think I've written about math without using it as an example for a philosophical framework --- when I write about math now, it's usually in-depth enough to be an article of part of a book --- and my CS posts are along the lines of, "how to use Git", "how to make a non-shitty website", etc.

While my CS posts have actual meat, unlike the "beginner-friendly" crap most article mills put out, it's often written as a high-level, cursory overview in order to remain accessible. But when CS occupies a lot of my headspace, I'll likely have ideas (maybe even rehashes of lectures) in my head that I'll just speedily put down as a post on this website. So the ratio of "personal things that happen" that I share is likely to decrease.[^gemlog]

[^gemlog]: Or it'll be an excuse to revive my gemlog. Since I only have personal stuff to share 4-5 times a month, I'll choose to populate this site with another article, but as the updates get more personal and more frequent in college (since more social things will probably happen there), I might decide to publish some stuff there instead. Precisely because no one is going to read it, unless they're techy enough to install a Gemini client (which probably means they installed Linux), and care enough to visit my gemlog. As far as I know, my readership consisted of one person until I stopped updating.

I think as you grow older, you start to realize that Noteworthy Good Things don't really happen to people that often. Life is usually complicated, and the only Noteworthy Good Things people generally share are marriages and the birth of a kid.[^thesis] Maybe if you're a celebrity or write books this list is longer, but generally life feels quiet because you learned to ride the waves. It's a little sad to think that, now I've mentally checked out of high school, I'll never get to feel the same highs I did when I first qualified for USAJMO or ran the 1600m under 6:00 in my first *real* track meet.

[^thesis]: I guess for me, the successful defense of my Master's Thesis will come first. Hopefully I stay on track to get a master's degree, since I don't really intend on doing a PhD and I'd feel bad only graduating with a bachelor's when the opportunity for a 4-year master's plan is right in front of me.
