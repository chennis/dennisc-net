---
title: Economic History Conference (2023)
date: 2023-09-10
---

This weekend, I went with *cursed schedule guy* to the [Economic History Conference](https://eh.net/eha/economic-history-association-2023-annual-meeting/) because it happened to be in Pittsburgh. A lot of big-name economics professors happened to be there, and a *lot* of working papers were presented.

Let's go through it, day by day.

Disclaimer: I know basically nothing about economics or economic history.

## Friday (Sep 8)

Technically you are supposed to register for the conference, and the fee is nothing to sneeze at: $75. We just snuck in, and no one really seemed to care, despite my babyface and lack of a name tag giving away that I was definitely not a grad student.

We were busy taking our fake (21-341 Linear Algebra), so we missed the first half of the presentations on Friday. But our other fake (21-300 Basic Logic) got cancelled, so we could make the second half of the presentations. We also met one of *cursed schedule guy*'s PhD Twitter mutuals (that's how he found out about this conference).

We went to **Session 5: American Political Economy**.

- The first paper was about the New Deal Era and how the legislative branch increasingly *delegated* its authority to the executive. As the New Deal is the part of US History I've studied most extensively, I found that I could mostly keep up... until the discussion started involving statistics. The methodology is not something that I could understand in a span of 30 seconds, especially since I wasn't used to the way economics papers presented their statistical results.
- The second paper was about fencing laws. For a bit of background, there is a principle called *Coase's Theorem* that stipulates private individuals can negotiate an optimal solution, given preconditions that never happen in real life, such as negligible transaction costs, etc. Fencing laws concern ranchers and farmers: should ranchers be responsible for fencing animals *in*, or farmers responsible for keeping them *out*, or some combination thereof? The paper analyzed the effect of these fence laws and their changes on farmers and ranchers.
- The third paper concerned the spread of Confederate Culture to the North and West. After the South lost the civil war, many former slaveholders migrated to the North/West for various reasons, and they brought their culture with them. The paper examines its presence (via a number of metrics, e.g. Confederate statues) and its effects. One of the important facts the paper established was that *former slaveholders disproportionately found themselves in positions of power*, which is how their culture had such an outsized negative influence.

Then we had a plenary session (plenary means "everyone attends") about how housewives provide much value to the economy even though they do unpaid work, and how measures to the productivity of the national economy (e.g. GDP) can be adjusted to better account for this sector of the economy.

After a short coffee break that we spent reading papers and talking to professors (note: I did not do much of the talking), we went to a different hotel for dinner and probably collectively bummed off at least $80 worth of food. (It was really high class.) Then we almost tagged along for the grad students dinner, but then the PhD Twitter mutual said "free-rider problem (also the grad students are paying for the food)", so we were like "I get it" and left. I was perfectly fine with it because I was absolutely exhausted.

## Saturday (Sep 9)

The plan was to attend the Historian's Breakfast at 6:30--8:00 AM, because history is something I know a lot more about (broadly) than economics. But due to a series of bad decisions, I ended up sleeping at around midnight last night, and decided that my health was worth more than attending the conference. So I barely made it to the second session of the day.

We went to **Session 10: Innovation, Finance, and Urban**, but because the bus was running late I could only make it to the second half.

- The first paper analyzed the effects of the loss of men in Japan after World War II. The methodology was very interesting: they used the change of the gender ratio and the death of male soldiers to find how many people died in the war. Also, because the US employed the strategy of island hopping, and regiments were made of people from the same prefectures, some prefectures saw heavy losses in men while others saw lighter losses in men. This lent itself easily to a comparative analysis by prefecture.
- The second paper concerned the effect of trade on US ports. As most history books will tell you, an increase in trade during the late 1800s-early 1900s coincided with industrialization and a decrease in agriculture.
- The third paper was a case study of Red Vienna, a large-scale public housing complex with cheap rent.

I was then going to attend the dissertation session as well, but then I got a splitting headache and realized I was just a couple of minutes away from collapsing. So I went back to my dorm and collapsed on the bed instead.

## Sunday (Sep 10)

I deliberately woke up late again to get breakfast. The bus was late *again*, and this time it didn't even let me off on the correct spot despite stopping there and my pressing the "Stop Requested" button. So I only made it for two papers. I attended **Session 16**:

- The second paper was about female hypergamy (a fancy word that means "marrying for higher social status"). The conclusion drawn was that, at least in England, hypergamy did not happen. The methodology was twofold: first, marriage registrations were used, and then for those with rare surnames not in the records, the average level of wealth for a person of that surname was used instead. The problem with this is that averaging data out tends to screw up correlation factors, but I'm not actually sure how impactful that is here.
- The third paper was about the link between the spread of electricity and birth rates. There were two opposing factors: electricity made it easier for children to be raised (since many manual tasks were replaced with machines, e.g. dishwashers), but electricity also created new skilled jobs, which increased the opportunity cost of having children as women had a comparative advantage in skilled jobs.

And that was the entire conference! It was a lot of fun, and I learned many things through osmosis. I probably need to study up on statistics, but economic history papers do not seem to have that high of a barrier to entry when it comes to reading them. Any layman could probably understand a good bit of the presentations and conclusions, even if the methodology is mathematically challenging.
