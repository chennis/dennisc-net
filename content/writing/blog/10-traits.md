---
title: Ten Things Effective Leaders Do
date: 2021-12-04
---

Leading is hard. I have to lead myself to bed each night without tripping over and falling on my face. Nobody is perfect, but recently I've been able to find the bathroom in the dark nine out of every ten times, so I thought I'd share my expertise with you.

## 1. Blame Everything on Consensus

Rejecting people is hard. So is doing things that aren't popular. But even harder is explaining why. Fortunately, effective leaders never have to explain anything.

"Why'd you ban me from the server?" Sorry, pal, it wasn't me --- it was the _group_ that banned you. You can ask me to explain and I'll "consult the rest of the team" until you stop.

## 2. Ask People to Do Work For You

As a leader, doing work is beneath you. That's why you have to get your subordinates to do it for you. But be careful, don't fall into the trap that many others have fallen into before. Don't work to make other people do work. This includes doing things like the dreadful "communication". Your time is better spent writing angry blog posts anyway.

## 3. Be Pissy When Things Don't Go Your Way

Sometimes, just asking people to do your work doesn't work. Fortunately, there is a simple solution for this.

Decision theory states that all animals weigh the potential gains and losses of an action before doing it. Humans fall into the animal kingdom, last time I checked my National Geographic. If you can make the potential losses (i.e. time and sanity) greater, maybe people will do what you want next time. It's not like they can kick you out, you're the _leader_ for crying out loud.

## 4. Take Second Chances

Your mama always told you everyone deserves a second chance. And as a leader, a brave pioneer, a daring venturer whose risk of failure is naturally higher, you deserve a lot more second chances than anyone else. So be brave and take them, even when no one else is brave enough to admit that you deserve it.

## 5. Take Breaks

Leading is hard work. So is writing this article, so I'll be taking a break from explaining this point...

## 6. Waste Time

Not yours, because it's too valuable to waste, but everyone else's. Find people who would be willing to do anything for you even if you have no clue what you need them to do. To prevent anyone else from figuring out you have no clue what you're doing, waste their time to distract them. They won't notice.

## 7. Hold Meetings

Everyone knows that important people meet often, and you're important, so meet often. As a bonus, you get to hear yourself talk. How cool is that!

As a double-bonus, you also get to fulfill the sixth point at the same time. Incredible!

## 8. Take Responsibility

Make sure you take responsibility for all of your successes. Don't worry about mistakes, those are what lesser leaders make.

## 9. Shift the Goalposts

After getting such good nuggets of wisdom it wouldn't be fair to expect yet another one from me. After all, this post was so good it might as well be worth ten of someone else's tips. Or even a hundred, pick whatever arbitrarily large number you like.
