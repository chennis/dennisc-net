---
title: I have achieved the impossible --- anti-knight
date: 2023-04-16
desc: On setting my first Anti-Knight sudoku puzzle.
---

Well, ladies and gentlemen, I did it. The one thing I never thought I'd do. I wrote an Anti-Knight puzzle, and with a little help from [Rangsk's solver](https://github.com/dclamage/SudokuSolver/wiki/fpuzzles-integration) to ensure that no cage I put broke the puzzle, I present you [6:13](/puzzles/6:13/ctc).

Given the name for this puzzle, I think it'd be very easy to use different cage totals and make this a recurring series. For example, if I wanted to use 10 and 15 cages, I could call it 10:15, etc. Kind of like a knock-off Killer Cages version of Florian Wortmann's Colorado series.

And the best part is, it only took somewhere around 2 hours. Given how long it's been since I set my last puzzle, I'm really proud of that.

More life updates, including the recounting of a track meet, my struggles in AP Chemistry, and roommate rizz, hopefully coming tomorrow :)
