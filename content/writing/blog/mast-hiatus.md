---
title: MAST is going on indefinite hiatus
date: 2022-07-29
---

In summary, MAST will no longer be offered. Applications will not be considered and content will not be developed.

For existing students: you will be getting a tarball of all the finished units as well as a separate tarball of all unfinished units.

A public explanation of why Math Advance has decided to place MAST on hiatus and what this means for its future is in order. This is said explanation.

We still stand by the claim that MAST's materials are some of the most thought-out and comprehensive lecture notes in the math contest community. However, our standards are not just "offer better material than everyone else" --- they are offering material that is as close to perfect as we can get. This even applies to the *code* that runs MAST and its content. Part of what makes Math Advance what it is now is our unrelenting strive for perfection, and these last few months have shown that we do not currently have the infrastructure or manpower to meet these high standards. Most math organizations (and organizations in general) would be content to keep offering a program like MAST under these conditions, and perhaps we would do more good than harm if we did so. But my personal philosophy as Math Advance's director is that it is okay to sacrifice the scope of our organization if it means the things we keep doing are as close to perfect as possible.

Currently we have our hands fill with the contest side of things: further developing mapm and its ecosystem (many thanks to [EvilMuffinHa](https://evilmuff.in) for agreeing to help out!) as well as rewriting the MAT website and its contest portal to align closer to my web development ethos. Personally, I am also working on a [Git hosting service](/writing/tech/glee-design) because I'm very picky about my infrastructure. In short, my manpower is limited and my priorities have shifted.

Teaching has, in part, been a selfish endeavor for me. Of course I enjoy seeing my students improve, but I also enjoy making my content as good as possible for its own sake. Since it's not currently my main interest (outside of my draft of an introductory competition geometry book), I hope you'll forgive me if I don't pursue it as frequently. Plus, my teaching philosophy has dramatically changed, so I need some time to collect myself and reorganize my materials in a way that makes sense to me.[^tutoring]

[^tutoring]: Personally, I still enjoy teaching, so I might do it in a lower-stakes environment like tutoring. So if you go to school in my area, I'd be willing to tutor the AMCs/AIME for $60 an hour. It sounds like a lot, but considering that [this price takes into account the time I need for lesson plans](https://blog.evanchen.cc/2016/02/07/stop-paying-me-per-hour/), plus the fact that the number of USAMO qualifiers tutoring is probably on the order of 1e3, I think it's pretty reasonable.

    I'm also planning on tutoring the SAT for the same rate, though if you're on this website you probably know it's more of "make some money" than "I have a passion for the SAT" (hint: nobody does). I think I'd be pretty good at it though.

As for the future of MAST? I don't think we're going to see that exact progrma back as is, though I am inclined to keep the name (so that way we can keep the logo too). However, I do hope to continue teaching with Math Advance in the future! It's more likely these materials will be used for shorter, more intensive math camps during the holidays. Or I might use them as bases for private tutoring. We'll see what happens.

I'm hopeful for the future. Perhaps this hiatus of MAST will not only represent the closing of a certain chapter for Math Advance but also the beginning of new opportunities. Putting MAST on hiatus, in a way, is like returning it to the staging area of a Git repository. Of course, any staged changes there may never make their way to remote regardless of how much work has been put into it, and just the same, there's the real possibility that nothing more will come out of the work we've put into MAST.

However it goes, I'm grateful to everyone for their support.

The original email sent to MAST students:
 
> I emailed this to the staff a while ago, and after careful deliberation I
> have decided that it is best to indefinitely suspend the MAST program.
> 
> Besides the reasons I have laid out in the original email, one more stands:
> we are in tech debt. The MAT website is a complete disaster as it stands,
> though I will not elaborate until after a rewrite is complete for security
> reasons. I'm also writing Git server software to improve Math Advance's
> infrastructure, and Math Advance is undergoing aggressive expansions on the
> contest side --- we plan to hold a Winter MAT (more details on this later).
> 
> The long and short of it is, I don't have the time/energy to spend on MAST,
> and frankly it is not my first priority anymore. When Math Advance's tech
> stack becomes more stable, MAST may come back in some shape or form!
> 
> Forwarded message from Dennis Chen on Thu Jul 7, 2022 at 8:26 PM:
>
> There's a good chance I end up forwarding this verbatim to the students, so
> I'm going to directly address them. And to the students, if you end up seeing
> this, note that this was originally a staff email --- so many of the concerns I
> bring up will be pertinent to them and only them.
> 
> MAST is waning, and though the lecture spree did redeem the end of Season 4,
> it is obviously not something sustainable at that level. We've been on
> minimum maintenance mode for maybe 10 months at this point, and it's about
> time we put a name to the decay that's been going on. Let's call a spade a
> spade: MAST has fallen in a state of disrepair because there is no one
> willing to work on it, including myself.
> 
> Many students may not know the difference between MAST and Math Advance, or
> that the latter even exists. So, to clarify: Math Advance is the parent
> organization of MAST, and we are also the organizers of the MAT
> (mat.mathadvance.org). Math Advance is still alive and well, but our interest
> (and our start) has always been with writing math contests, which is why we
> have recently put much more attention into it.
> 
> Part of this "lack of interest" is largely selfish. I've personally been more
> involved in writing programs that do useful things for Math Advance and the
> math contest community at large. The culmination of my last 6 months of work
> is in programs like mapm (mapm.mathadvance.org), the Math Advance website
> (mathadvance.org), the MAST website (mast.mathadvance.org), the design for
> the MAT contest PDFs, and so on. However, this shift to programming has
> largely whittled away at my familiarity with the math contest scene since I
> haven't been practicing or teaching as often.
> 
> I still am doing technical writing, but most of those efforts have been
> focused on a geometry book for beginners to contest math (mostly at the
> middle school level). As such, I am left with little time and energy to work
> on MAST materials, and my familiarity with them has honestly slipped past the
> point where I can recover. I still faintly remember the philosophies I put
> into Perspectives, but that's about it.
> 
> At the same time, I recognize that a lot of people have gotten value from
> MAST. I don't want to take too much credit --- but I have noticed that with the
> new batch of students, MAST has played a role in their improvements, however
> small it may have been. Many USAJMO qualifications, multiple MATHCOUNTS
> Nationals qualifiers (and some from really difficult states, too) --- I don't
> have the time to name you all in this email, but I'm proud of every one of
> you. My only hope is that you pay back the kindness, generosity, and patience
> you've encountered in math contests in some shape or form, even if it's not
> to the math community. (You're all really talented, and Math Advance would
> appreciate seeing some of that talent here :)
> 
> Beyond just winning titles in math contests, I'm glad to see that MAST has
> also formed a community. We've competed in a ton of math contests together
> under the Math Advance banner, played games together in voice chat, and had
> some of the silliest conversations. None of this is going away. If any staff
> are still willing next year, we will continue to organize teams for college
> math contests.
> 
> So here's what's going to happen: I probably will not host another round of
> applications, or actively give units to people anymore. Feedback on
> submissions definitely will not be happening, not that it has been
> consistently happening anyway. I will still be answering questions in the
> MAST Discord, whether they are about our handouts/contests or not.
> 
> What I will do is probably send a tarball with all of the MAST materials,
> both what has been migrated onto the website and the units which have not.
> There is no official guarantee for quality for the units which have not been
> vetted, but anecdotally speaking, most of them have some sort of value, even
> if they aren't quite up to par. This way I do not have to respond to any unit
> requests, which further reduces the workload I get from this program. (It's
> not like I really decline any unit requests anyway.)
> 
> If I end up doing this, I request that you do not distribute any of our
> materials to the wider math contest community. You guys have been really good
> about this, so this is more just to let you know that the redistribution
> policy is the same. I want to keep the option of using these materials for
> some other purpose open, possibly including the revival of MAST in some shape
> or form.
> 
> Thanks to everyone who has ever contributed material to our program. Thanks
> especially to Prajith, who has expanded our workable content base by writing
> solutions for many units. Your work will not go to waste --- though MAST may
> not exist the same way it did two years ago, I do have plans for these
> materials sometime in the future! One of Math Advance's primary goals is to
> teach, and I fully intend to continue doing that in some shape or form.
> 
> Finally, thanks to everyone who's donated the suggested $40 to Math Advance
> (no one chose to donate a different amount). Though the program you donated
> to, MAST, may not be around anymore, we will still use these funds to support
> Math Advance's greater mission of supporting math education. A couple of
> things that donations will help cover:
> 
> --- Prizes for math contests (which we will be hosting more of, as MAST's
>   hiatus will provide us with more focused energy in this regard)
> --- Server costs & domain costs
> --- Registration fees for college contests
> 
> I've learned a lot from teaching MAST for the last 2 years. I hope you
> learned just as much participating. Thank you to all the students and staff
> for supporting me for so long --- it's been a blast.
