---
title: Interregnum
date: 2023-04-04
desc: Thoughts on deciding what school to go to and that high school is ending.
---

*I wrote this in my notebook on April 1, but haven't had the energy to type this up until tonight. Not a good sign of how the rest of break will go, but oh well.*

Yesterday, my last college decisions came out.

Yesterday, my school's spring break started.

It seems fair to describe this month as an interregnum, before high school final exams and AP exams begin ramping up for the last time. Before I have to make the one decision that *really matters* off of very little information.

---

A long time ago, I had the idea to share my college applications and reflect on them in their entirety, hopefully providing useful advice to future classes applying for college. But now, I don't have the energy to do this, and nor do I think my college results are good enough to justify giving advice. In fact, given my stats, I did really badly. So I will merely describe rather than prescribe.

**Acceptances:**

- UCLA math
- UCSD undeclared (rejected from CS)
- UCI CS with honors
- UCSB CS
- UIUC math (rejected from CS)
- UT Austin CS, Turing Scholars[^turing]
- UWash "pre-major" with $5,400/yr scholarship (rejected from CS --- so I'm good enough to get a scholarship but not good enough to study CS...)
- CMU CS

[^turing]: If you haven't finished applying to college yet and you don't know what Turing Scholars is, do yourself a favor and Google it.

**Rejections:**

- UCB (applied for math)
- Cambridge math
- All Ivies except Dartmouth and Penn
- MIT/CalTech
- Stanford
- JHU
- Harvey Mudd
- GaTech (applied CS)
- Duke

So yeah, I had about a 33% acceptance rate, and taking out safeties/CS rejections (because I'm not going to UWash to study not-CS) plummets it to 10-15%. Interestingly, I expected to get screwed for CS and decently for HYPSM, and the exact opposite happened. Surprising, given my total lack of CS achievements. (No research/significant projects --- where "significant" means sufficient reach/fame --- and only USACO Silver, *before* difficulty inflated.)

---

In some sense I've won. If I knew in August that I'd get into CMU CS, I would've been really relieved --- relieved enough not to care about the absolute slaughter I went through. (Also I'd be really confused why future me is telling me about this Turing Scholars thing so enthusiastically.)

In another sense, I've lost. I expected to get in some of HYPSM, and there are definitely people who have a lot more choice than I do when deciding where to go.

But the thing about college apps is it always feels like you're losing. As long as I don't have to wonder, *what would've happened if I got into a top college?* everything will be fine.

---

CMU is apparently really strong at AI. Unfortunately I don't have much interest in it (mostly because I despise applied math).

Maybe I'll change my mind in 4 years. Hopefully I do.

---

For the last 4 years, I've been convinced that I was going to study math and major in math and become this professor guy in math that does Important Research.

But why was I so convinced? Probably because I figured out I was good at math a long time ago, decided "OK and now I will like math", and then went with it. So what if I got really good at CS in the next few months and the next few years, and just decided to myself, "now I will like CS"?

I find the theoretical stuff really interesting at least, even if I don't know a lot about it. I hear it's basically just mathematicians pretending to do Not Math. At the minimum, I should enjoy that...

---

I'm probably going to CMU. But whichever of CMU and Austin I went to, I'll feel like I'm missing out on something. CMU's core curriculum aligns really well with my views on CS education, and it's ranked #1 for CS --- that's gotta count for something. But Austin is a tech hub and sends a ton of people to quant and will be a boatload of fun that I will, with all probability, not experience.

If only I had two lives. If only I could experience both of these universities.

If only I could study math at UCLA too. Once upon a time I was really convinced I wanted to do math at a UC. Wouldn't it still be worth doing? What fundamentally changed in the last 4 months?

I *am* very excited about going to CMU. But some part of me *knows* that I'd also be really excited to go to Austin. What if I end up sitting alone in my room for god knows how many days and asking to myself, "what if I went to Austin instead, what if I didn't have 50 hours of work every week? What if I valued my mental state more than my superficial ego?"

The scariest thing is, I won't ever find out if the grass *was* greener on the other side. And there's a 50% chance that it is.

---

I don't know a lot of CS. Scratch that, I don't know a lot of anything, including math.

I want to study ahead over the summer.

I want to work on the things that interest me. Finish up some projects before school starts, because god knows I won't have the time to do that in school.

I want to get the ball rolling before it's too late.

---

I'm about to graduate. Some part of me is having trouble really believing this.

I have two to three more months to make some final memories with people in high school. Aside from a couple of outliers, I probably won't ever talk to them ever again.

Near the end of WARP, I was intellectually aware that I'd never see most these people again. But emotionally it never registered.

And I really mean never, not even now. I just said, "huh OK WARP is forever over", and went back to my normal life. For a week, normal meant sitting in the Lodge saying stupid shit with my friends. Climbing rocks. Swimming in the pool. But now it's just a distant memory, something that I know happened but don't really feel anything about.

I feel the same way about high school. Next year, I'll be looking back at all four years of high school, and hell, maybe even middle school, and just think "huh I guess these things happened. And I knew these people." I don't think I'll really *miss* high school, even though in the moment it was really fun. Junior year already feels like an eternity away. Hell, even first semester senior year. I feel like it was just yesterday that I was in middle school geometry, showing off like the immature brat I was. Intellectually I know that all of high school happened and I should probably treasure these memories or whatever. But on an emotional level, I'm finding it hard to feel much of anything. I mean, I'm excited for college and excited for more than 50% of my classes not to be a steaming pile of fucking nonsense. But I don't feel sad at all that I'm going to be leaving high school, even though high school has been nothing short of great.

It's weird that I'll cry over a fucking imaginary pink elephant dying in a kid's movie, but not over literally leaving everyone I have ever known in my entire life.

Y'know what? I think it's probably OK.

But if you're from the area and I know you, and you're reading this, let's hang out or something.

Before it's too late.

---

*Before it's too late. Before it's too late. Before it's too late.*

Why is it, when I've had for years to do these things, I decide to wait until it's almost too late?

I should study math and CS.

I should finish my geometry book and Git hosting software.

I should say goodbye to some people.

But thinking and doing stuff is hard, and this [Whispers Skyscrapers](https://logic-masters.de/Raetselportal/Raetsel/zeigen.php?id=0008RD) puzzle seems a lot more appealing.
