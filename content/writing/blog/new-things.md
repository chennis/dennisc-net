---
title: Trying new things
date: 2023-01-02
---

I feel like it's harder now to get people to try new things than it was when I was younger. Back then people would make an honest effort even if they didn't follow through with it, but now the answer is a non-committal "sure, I'll check it out" with zero followup.

A caveat: this is based on a sample size of $n=1$ (i.e. me), and I was pretty weird as a kid and "pushed" things on people (with varying degrees of success), whereas now I kinda don't. (It's not like I'm exactly the paragon of normality now, but at least now I can *pretend* to be normal when I have to.)

And I'm noticing this mostly because *I'm* the one who does this to people more often than I'd like. (In fact, I think being non-committal has been a pretty big issue of mine.) I can think of a couple of times where people asked me to join them in doing something or whatever, and since I don't want to say no, I just say "yes" without following up 90% of the time. So yeah, I want to work on that myself.
