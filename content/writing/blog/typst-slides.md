---
title: In 2025, use Typst to typeset your slidedcks
date: 2024-12-31
---

Back before I knew Typst existed, I wrote [Beam](https://dennisc.net/code/beam), a language which could transpile to TeX and then generate LaTeX beamer slides. For some obvious reasons this is quite a suboptimal idea, and really papers over the fact that LaTeX syntax, *particularly for typesetting slides*, is very cumbersome.

Well, it is the year 2025, and Typst is a very ergonomic typesetting language. For almost any reason, it is probably wiser to use Typst to typeset slideshows instead of beamer. I personally am using [slydst](https://typst.app/universe/package/slydst/) to typeset the slides for my puzzle course next semester.

That's all from me today. Cheers, and happy new year!
