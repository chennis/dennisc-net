---
title: Constructing "5"
date: 2022-12-05
desc: My thought process behind constructing the puzzle titled "5".
---

<style>
img {
    margin-left: 0;
    width: 40%;
    height: 40%;
}
</style>

*Spoilers ahead for [5](/puzzles/5), so please try it yourself first! It is GAS level and is probably around 2.5 or 3 stars in difficulty.*

This post will be a little different from my last one. Instead of listing the (ultimately useless) trial and error I did, which was not much, I will instead show the order which I added the final clues in. Since the solve process is fairly linear, it was easy to just layer clues on top of each other this time around. (This is in contrast to Alphabet Soup, which was much more intricate... a post on that coming soon!)

<img src="order.png" alt="Order that clues were added">

You will notice that two sets of clues were added at the same time: 5 and 6. This is because I was near the end and experimented with brute-force to find the most aesthetically pleasing set of constraints to finish the puzzle off.

So a couple of days ago, I decided I wanted to try writing a German Whispers puzzle and playing around with the number 5 (German Whispers also cannot have a 5 on them). I wanted to pay homage to this number 5, so I just drew a fat 5. Originally it was not as tall (1 row less on each side), but on paper it didn't look as good so I stretched it. I also knew it would be more constraining and make writing the puzzle easier.

Conveniently, XV pairs also cannot have a 5! I was going to use V's at first, but if I needed an X, I knew I could use it later as well. So I'm like, "Hmm let's force my first 5" and placed clue 2. Now 5 was fairly constrained: it must appear in row 4 for region 5 and so must appear in row 5 for region 6. Then I pushed the puzzle to its logical limits and got the 6 at the bottom, and some other good stuff

<img src="12.png" alt="The implications of clues 1 and 2">

There are some very constraining things to note: though 8 and 9 cannot resolve at this stage (there is no differentiator between the two at the moment), the 6's and 7's on the German whisper only have one degree of freedom. Also, a V in regions 1 and 6 would nicely differentiate where 5 was, and the V in region 1 would also take care of the 4 in region 4.

So hopefully it is quite clear why I added clues 3 and 4. I had to do something to differentiate columns 1/2 and columns 8/9, because at the moment those pairs are symmetric. Determining some 5's and the 4 seemed fairly natural. (As to why I chose to place them on the outside, well, I just thought it looked aesthetically better. I could've just as well placed the V in columns 2 and 8 instead of 1 and 9.)[^intent]

[^intent]: By the way, this is why you need humans to craft good puzzles, especially variant puzzles. When I put these clues here, the intent will eventually be communicated to the solver. Computers have no intent, and especially for variant puzzles, intent is necessary to make a good puzzle (or even a puzzle at all). Without intent it may not even be possible to construct around some trickier variants, like German Whispers, which force you to keep track of polarity, the natural restriction on 5, etc...

After adding each of clues 3 and 4, I logicked out the puzzle to take stock of where I was. After clue 4 I figured I was close enough to the endgame. I decided to place clue 5 to forcefully differentiate the 14 pair in column 3 and the numbers in the V of clue 3. Then I realized I really was close, started playing around with online sudoku solvers (essentially it was just a faster way to fill in candidates for each cell), and after trying a couple of potential XVs I settled on clue 6.

Then I decided to check that clue 6 actually made things satisfying to solve and didn't cause any unnecessary cheese. It did: it gave solvers a early 5 disambiguation in region 5 (the X clue), made the middle and end of the puzzle less convoluted (the V clue), and the combination of the two also somewhat quickly determines the location of 4 in region 5. (I mean, technically it determines everything, but you know what I mean.)

## Summary

So yeah. This puzzle was almost entirely constructed on paper and took about 3 hours. This was a much easier puzzle to construct than Alphabet Soup. (Alphabet Soup took my soul out of me. I consider it a miracle that I managed to finish it within a week, and that's a long time for one puzzle, I think.)

My general thought process for constructing a puzzle, and what happened for this one in particular, was

- I thought of a general theme (the 5, the German whisper, etc). From my experience writing math contest problems you don't just decide "OK I will write a problem", you have to stumble your way into the core idea. (At least, I can't just decide to write a problem --- I'm sure there are people who can.)
- I put down a couple of probing clues to get started.
- In the middle, I aggressively constrain things that are almost constrained.
- At the end, I use some computer assistance to determine which set of final constraints is most viable.

In particular, there was a lot less floundering than when I was writing X Marks the Spot, and since the puzzle was not as intricately connected as Alphabet Soup, it was not too hard to finish.

Hopefully this made my thought process behind the puzzle clear, and helps you in writing your own puzzles! If you do write a puzzle, whether it is because of my blog or for any other reason, please send them over --- I'd love to solve some nice puzzles :)

By the way, if anyone knows how to make German Whispers and XV sums in Penpa, please let me know :)
