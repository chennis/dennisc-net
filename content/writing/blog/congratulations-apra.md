---
title: Congratulations, Aprameya
date: 2023-04-27
---

Today, Aprameya, one of my former students and current colleagues[^better] in Math Advance was invited to MOP.

[^better]: There's gotta be a better word for this, but I can't think of it.

Let me tell you a bit more about him. When he was in 7th grade he applied to the MAST program, not having qualified for the AIME. But through his solutions to the application problems, I could see that he had a lot of depth and could solve really hard problems, and so I invited him to the program.

I was more right than I ever could have imagined. A few months later, he started writing geometry questions and sending them to me. At first I could handle the number he was sending me --- for about a few weeks or so (if my memory is correct). Then the output ballooned into something I couldn't review alone, so I invited him to the contest-writing branch of Math Advance in the hopes that the rest of the team could help review. And even after the rest of the team was looking at his problems, we still could barely testsolve *most* of his questions --- and our team has a ton of MOPpers and USAMO qualifiers. That's how hard they were, and that's how many he wrote. He's also helped out with MAST and wrote a couple pieces of advanced number theory content.

Two years later he qualified for the USAJMO. This year he qualified for the USAMO --- as a sophomore. He'd been grinding ISLs and not only did his depth extend, he also became more accurate and more articulate in communicating his mathematical ideas.

All this is to say: this is not particularly surprising news. Given his work ethic and his contributions to our program, it was really just a matter of time. Congratulations again, Aprameya, and thanks for all your contributions to Math Advance. Let's continue making more cool stuff like the geometry contest ~~that we've been planning for literally two years~~!
