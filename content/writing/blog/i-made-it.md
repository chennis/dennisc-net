---
title: I Made It
date: 2022-03-16
desc: USAMO 2022
---

A deluge of emotions are rushing through me as I type this post. The cutoffs are so low/I did not deserve to make it/ha I made it even after missing #1 on the AIME. I've been pretty much below average (or at least believed so) yet come close in pretty much everything. Missing the F=ma cutoff by 1 point (14 vs 15, missed questions 1 AND 4, which were both free). Gassing during the Dan Gabor 800m and not getting the T-shirt. I've been pretty objectively mediocre at physics/CS/running/whatever else I tried my hand in and until now I thought math was also on the list. I thought cutoffs would be >225 for 12A + I (so effectively 230+ for me) and believed I missed USAMO because I was stupid enough to miss question 1 on the AIME. (I forgot to double on the test.) But apparently not. I made it.

I don't know how much this will, say, help with college apps (I predict not much). I don't think much in my life will change, but one important thing has happened: I feel like I've finally won something in life. It's been a while since I really, truly felt like I've managed to best a challenge which really mattered to me, and finally I did it.

By the way, next Friday I also (hopefully) have a track event (representing our school at some fun run). So if that pans out, I am skipping three straight days of school. I am definitely looking forward to that.
