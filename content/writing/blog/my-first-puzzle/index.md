---
title: My First Puzzle
date: 2022-11-03
desc: Reflections on writing my first puzzle (X Marks the Spot) and what I intend to do with puzzle-writing in the future.
---

<style>
img {
    margin-left: 0;
    width: 40%;
    height: 40%;
}
</style>

*Spoilers ahead for [X Marks the Spot](/puzzles/x-marks-the-spot/penpa), so please try it yourself first! It is 4 or 4.5 stars in difficulty and is a thermo/arrows sudoku puzzle with no numbers given. This post won't make much sense until you've finished it anyway.*

*Many thanks to all my testsolvers! Without you, I would've posted a broken puzzle --- thanks for taking the time to do the puzzle even after the first version was broken.*

Recently I've been doing a lot of sudoku puzzles on sites like gm-puzzles, and I noticed that long thermos/long arrows are particularly constraining. (I mean, duh.) So I thought, "huh, what if I slammed down a long thermo and arrow, and had them intersect?"

<img src="1.png" alt="First step of puzzle construction">

This has the added bonus of creating the theme, "X Marks the Spot", and cool stuff *probably* happens because of it, so I'll build the rest of the puzzle around this. (This cool stuff ends up becoming the logic in the cross in the final puzzle.)

(You might see that the thermo doesn't match what's on the final draft. I only realize it doesn't need to be this long later.)

Now I have some ideas of bounding the center, determining a large portion of the thermo. I know this isn't enough though, so I just throw some random crap at the wall.

<img src="2.png" alt="Second step of puzzle construction">

You will notice that the two rightmost arrows seem very constraining --- and that is exactly so! Even though I randomly added them, I had the instinct that they would be the most helpful. So I do a bit of logicking and discover the following:

<img src="2-logic.png" alt="Some logic after the second step of puzzle construction">

This is based on the idea that the two thermos have to add up to at least 16. There are 5 numbers on in arrow in the rightmost column, and the five smallest numbers add up to 15. Add the other number on the arrow which is at least 1, and the sum of the circles is at least 16.

At this point, everything besides the thermos and the rightmost arrows is irrelevant. Note that A and B represent 1 and 2 in some order. The important problem is this: in the rightmost row, the smallest numbers on the bottom arrow are B, 3, and 4, meaning that the sum in the circle is 7+B. But also note that the sum in the center arrow is B+A+4+B=7+B, contradiction. Crap.

Well, what if we make the thermo a little shorter?

<img src="3.png" alt="Third step of puzzle construction">

(I removed the arrows that weren't doing anything at this point, because now I'd honed in on some actual ideas I could work around.)

Okay, now we have some numbers determined, but this isn't nearly enough. Time to constrain things a little more. This time, instead of throwing crap at the wall, I deliberately force 1 and 2 to not be in the center row.

<img src="4.png" alt="Fourth step of puzzle construction">

Now I come up with the idea to restrain the locations of 1 and 2 in the center row. I notice the 1 and 2 in the third column and immediately realize the most natural way to do it is with a thermo.

After some more logic, however, I find that it doesn't work.

<img src="4-fails.png" alt="Fourth step of puzzle construction fails">

There's nowhere the 5 can go in the center row (check the thermo). The solution I eventually come up with is flipping the thermo a la the final puzzle, but first I shorten the thermo, forget that I can have 2 in the row now that the thermo is shortened, and send this ill-fated first draft to my testsolvers.

<img src="draft-1.png" alt="Failed first draft">

However, this failed draft did give me something --- the ideas with the arrow on the bottom and the diamond arrow on top (since 7, 9 were restricted on the second row, 1, 2, on the third row, and 1, 2, 3, on the third column) which I thought were cool and still tried to preserve in the final draft.

Eventually I settle on doing the right thing --- flipping the thermo.

<img src="5.png" alt="Fifth step of puzzle construction">

At this point, I bring back the arrows, because I still think it's underconstrained anyway (it is) and want to add them back and adapt to the new circumstances. I quickly realize the bottom arrow is not constrained enough.

<img src="6.png" alt="Sixth step of puzzle construction">

So I bend the bottom arrow to constrain the first number to be 1 or 2 again.

<img src="7.png" alt="Seventh step of puzzle construction">

Now some logic:

<img src="7-logic.png" alt="Some logic after the seventh step of puzzle construction">

There are a lot of steps, many of which align with the final puzzle: the fact that 3 is in the bottom of the bottom arrow and so on. I won't lie, a lot of these deductions went over my head when I was constructing the puzzle at the end until I used a computer to find some common similarities between all the possible solutions.

After some more computer analysis, I concluded that adding a thermo would finish the puzzle.

<img src="/puzzles/x-marks-the-spot/puzzle.png" alt="Final puzzle">

The end.

## Future

I'd always intended this to be a one-off thing, at least until I finished. I wanted to make the most out of the ideas I had... and I would've followed my plan, had I not thought of an even more crazy puzzle idea. This one will take a while longer, as the logic is so intricate that even I'm currently stumped by it. Look forward to at least one more release!
