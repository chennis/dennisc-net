---
title: Wisdom Teeth
date: 2023-07-18
---

So I got two of my wisdom teeth pulled today. I have to come back in about a week to pull the other two.

I'm in China and they only do facial anesthesia. This seems objectively better. The rest of the operation was a little sus though. The person doing it stepped out of the room to do god knows what and was panicking about missing something *after* I got anethesiaed. Also the bottom right tooth seemed like an absolute struggle for them to pull out considering the top was removed in like 30 seconds.

Afterwards since I couldn't talk, I had to pantomime to communicate. It's like I was playing charades but five times worse.

I am typing this with only my left hand on my phone. My right hand is holding an ice bag to my face. Obviously I will be publishing this when I am home, at which point I will probably be fine.

I started writing a SCOTUS post before the op and my language was a lot less wooden then. (You can compare the results when it comes out.) Now it is stilted. I wonder why.

This must be what it feels like to suck at writing. And that, I feel, is far more painful than removing wisdom teeth ever could be.
