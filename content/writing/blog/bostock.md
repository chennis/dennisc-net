---
title: Bostock v. Clayton County analysis
date: 2023-07-20
---

I read the entire released opinion for Bostock v. Clayton County, including the dissents. Here is my analysis.

## Background

Several people were fired for being gay or transgender. Both the plaintiffs (the people fired) nor the defendants (the employers who had fired them) agreed that these firings were discrimination on the basis of sexual *orientation*. [Title VII of the 1964 Civil Rights Act](https://www.eeoc.gov/statutes/title-vii-civil-rights-act-1964) states:

> It shall be an unlawful employment practice for an employer ... to fail or refuse to hire or to discharge any individual, or otherwise to discriminate against any individual with respect to his compensation, terms, conditions, or privileges of employment, because of such individual's **race, color, religion, sex, or national origin**

Notice that sexual orientation and gender expression do not appear there (despite many efforts to amend Title VII to include it). Both parties agreed that Title VII applied to the employers. The question just is: if sexual orientation is not explicitly protected in Title VII, then is it protected all the same?

## Majority opinion, by Neil Gorsuch

The 6-3 majority (liberals + Gorsuch and Roberts; at this point Ginsburg was still on the court) ruled that Title VII prohibits discrimination on the basis of sexual orientation and gender expression, **because you cannot discriminate on the basis of sexual orientation/gender expression without discriminating on the basis of sex**. Rather than being a case of constitutional law, this is a case of statutory interpretation: SCOTUS ruled based on the text they had and not any constitutional considerations.

Gorsuch starts with a brief historical note on the Civil Rights Act of 1964: no one intended for Title VII to protect sexual orientation or gender expression. But,

> Those who adopted the Civil Rights Act might not have anticipated their work would lead to this particular result. Likely, they weren't thinking about many of the Act's consequences that have become apparent over the years, including its prohibition against discrimination on the basis of motherhood or its ban on the sexual harassment of male employees. But the limits of the drafters' imagination supply no reason to ignore the law's demands. When the express terms of a statute give us one answer and extratextual considerations suggest another, it’s no contest. Only the written word is the law, and all persons are entitled to its benefit.

Then he establishes the background of the case. Some gay and transgender people were fired after the companies discovered they were gay/transgender. District courts had different rulings. The background is not incredibly complicated or central, because everyone agrees what the facts are: the employees were fired for being gay/transgender. The entire case hinges on whether Title VII protects against this discrimination.

One major detail about the established facts:

> The only statutorily protected characteristic at issue in 
> today’s cases is “sex”—and that is also the primary term in
> Title VII whose meaning the parties dispute.  Appealing to
> roughly contemporaneous dictionaries, the employers say 
> that, as used here, the term “sex” in 1964 referred to “status 
> as either male or female [as] determined by reproductive
> biology.” The employees counter by submitting that, even
> in 1964, the term bore a broader scope, capturing more than
> anatomy and reaching at least some norms concerning gen-
> der identity and sexual orientation.  But because nothing in
> our approach to these cases turns on the outcome of the par-
> ties’ debate, and because the employees concede the point
> for argument’s sake, we proceed on the assumption that 
> “sex” signified what the employers suggest, referring only 
> to biological distinctions between male and female.

Now for the meat.

### But-for causation

The word "because" implies but-for causation. If X not being true would've prevented Y from happening, then X is a but-for cause of Y. In Gorsuch's words,

> A but-for test directs us to 
> change one thing at a time and see if the outcome changes.
> If it does, we have found a but-for cause. 

Then he follows up with an example:

> So, for example, if a car accident occurred both because the defendant ran a red light and because the plaintiff failed to signal his turn at the intersection, we might call each a but-for cause of the collision.

Notably, it is important that Congress chose to use "because of", and not "solely because of" or any stricter standard.

### Discrimination

Gorsuch spends a while rigorously defining discrimination, but for our purposes the major point is this:

**Discrimination is illegal on an individual basis, not just a group basis.** This is important because

> The consequences of the law’s focus on individuals rather than groups are anything but academic. Suppose an employer fires a woman for refusing his sexual advances. It's no defense for the employer to note that, while he treated that individual woman worse than he would have treated a man, he gives preferential treatment to female employees overall. The employer is liable for treating *this* woman worse in part because of her sex.

Therefore, firing both lesbians and gays doesn't mean the homophobia "balances itself out", provided we establish that firing people for being attracted to the same sex is sex discrimination. (We establish that next.)

### Why sexual orientation/gender identity discrimination is sex discrimination

Now here is the main point: take some man who is attracted to men but is otherwise inoffensive to the employer. Now change his gender, and we have a woman attracted to men. The employer has no reason to fire this hypothetical woman now, and so the man's sex is a but-for cause. This is, plain and simple, what "discriminating because of sex" *means*.

Similarly, take someone born as a man but who presents as a woman who is fired by an employer because of this. Someone born as a woman who presents at a woman would not have been fired. Sex is, again, a but-for cause.

Gorsuch then buttresses this with SCOTUS precedent: the "simple test", i.e. this but-for analysis, has held water for a number of unexpected applications of Title VII. This is but the latest.

### Common refrains

Gorsuch addresses two common refrains in his opinion. I'll let his own words do the trick here.

**Refrain 1:** Sexual discrimination must be *willful*. How can it be willful if the employer doesn't even know the sex of an applicant?

> Sup-
> pose an employer asked homosexual or transgender appli-
> cants to tick a box on its application form. The employer
> then had someone else redact any information that could be
> used to discern sex.  The resulting applications would dis-
> close which individuals are homosexual or transgender
> without revealing whether they also happen to be men or 
> women. 
>
> Doesn’t that possibility indicate that the em-
> ployer’s discrimination against homosexual or transgender 
> persons cannot be sex discrimination?
>
> No, it doesn’t. Even in this example, the individual ap-
> plicant’s sex still weighs as a factor in the employer’s deci-
> sion. Change the hypothetical ever so slightly and its flaws
> become apparent. Suppose an employer’s application form
> offered a single box to check if the applicant is either black 
> or Catholic. If the employer refuses to hire anyone who
> checks that box, would we conclude the employer has com-
> plied with Title VII, so long as it studiously avoids learning 
> any particular applicant’s race or religion? Of course not: 
> By intentionally setting out a rule that makes hiring turn 
> on race or religion, the employer violates the law, whatever 
> he might know or not know about individual applicants.

**Refrain 2:** In the *but-for* analysis, you changed a *gay* man into a *straight* woman! Two things changed at once! Now you can't prove it was sex discrimination rather than sexual orientation discrimination.

> When we apply the simple test to Mr.
> Bostock—asking whether Mr. Bostock, a man attracted to
> other men, would have been fired had he been a woman— 
> we don’t just change his sex.  Along the way, we change his
> sexual orientation too (from homosexual to heterosexual).
> If the aim is to isolate whether a plaintiff’s sex caused the
> dismissal, the employers stress, we must hold sexual orien-
> tation constant—meaning we need to change both his sex 
> and the sex to which he is attracted.  So for Mr. Bostock, 
> the question should be whether he would’ve been fired if he
> were a woman attracted to women. And because his em-
> ployer would have been as quick to fire a lesbian as it was 
> a gay man, the employers conclude, no Title VII violation 
> has occurred.

In response, Gorsuch makes the following analogy:

> Consider an 
> employer eager to revive the workplace gender roles of the 
> 1950s. He enforces a policy that he will hire only men as
> mechanics and only women as secretaries. When a quali-
> field woman applies for a mechanic position and is denied,
> the “simple test” immediately spots the discrimination: A 
> qualified man would have been given the job, so sex was a
> but-for cause of the employer’s refusal to hire.  But like the 
> employers before us today, this employer would say not so 
> fast. By comparing the woman who applied to be a me-
> chanic to a man who applied to be a mechanic, we’ve quietly
> changed two things: the applicant’s sex and her trait of fail-
> ing to conform to 1950s gender roles. The “simple test” thus
> overlooks that it is really the applicant’s bucking of 1950s 
> gender roles, not her sex, doing the work. So we need to 
> hold that second trait constant: Instead of comparing the 
> disappointed female applicant to a man who applied for the
> same position, the employer would say, we should compare 
> her to a man who applied to be a secretary. And because 
> that jobseeker would be refused too, this must not be sex 
> discrimination.

And he basically concludes with, "the conclusion in this hypothetical is ridiculous, the *simple test* has worked consistently for every other instance of sex discrimination where sex was a tangential cause, there is no reason for it not to hold here."

### Conclusion

I've left out a lot of details in order to summarize the majority opinion, and you should read the full text on your own time. This is a remarkably simple case, so the majority opinion stands at a slim (by SCOTUS standards) 37 pages. This section has mostly been summary instead of analysis because the majority opinion is just *correct*. There's nothing left out, and there's nothing wrong. The *dissents*, on the other hand...

## Alito's dissent

*Joined by Thomas.*

The dissent essentially goes like this: "No one expected Title VII to cover sexual orientation and gender identity so it doesn't". Never mind that interracial marriage wasn't intended to be covered by the 14th Amendment either. Apparently legislative intent overrides the clear plaintext of the statute. Purposivism as a legal framework is pretty sketchy anyway, so if that's the best Alito can muster, the dissent holds little water.

Alito appeals to originalism:

> our duty is to interpret statutory terms to "mean what they
conveyed to reasonable people at the time they were written."

Yet any reasonable person would conclude that but-for causation is enough, more than enough, to satisfy "because". It does not matter whether these but-for causes were anticipated by the legislature or not. (Gorsuch's majority opinion notes many examples such as motherhood where it not being expected to be covered by Title VII, and gender not being the *primary* cause, are irrelevant.)

Alito then spends some time saying, "sex was not construed to cover sexual orientation or genedr identity":

> Determined searching has not
> found a single dictionary from that time that defined "sex"
> to mean sexual orientation, gender identity, or
> "transgender status."

Nobody contests this. He even concedes this a page later. Now he gets to his main point:

> If an employer takes an employment
> action solely because of the sexual orientation or gender
> identity of an employee or applicant, has that employer necessarily discriminated because of biological sex?

Uh, yeah? The majority opinion literally covers this. Discriminating against a man who is attracted to men, when the employer would've had no issue with a woman being attracted to men, is sex discrimination.

I will not belabor the point: Alito's dissent makes no real counterargument to any of the majority opinion's arguments. He instead dances around the fact that you cannot define sexual orientation and gender identity without sex (and therefore cannot discriminate against either without discriminating against sex).

However, this quote is so awful that I had to just include it. Just one more...

> Contrary to the Court’s contention, discrimination because of sexual orientation or gender identity does not in
> and of itself entail discrimination because of sex.
> An employer
> can have a policy that says: "We do not hire gays, lesbians,
> or transgender individuals." And an employer can implement this policy without paying any attention to or even
> knowing the biological sex of gay, lesbian, and transgender
> applicants. In fact, at the time of the enactment of Title
> VII, **the United States military had a blanket policy of refusing to enlist gays or lesbians, and under this policy for
> years thereafter, applicants for enlistment were required to
> complete a form that asked whether they were "homosexual."**

Yikes. I mean, wow. This is impressive. Alito is appealing to authority, and the best part is, *he's not even appealing to the right authority*. If the Court was bound by the executive's interpretation of laws[^chevron], then what exactly is the Court supposed to do? Affirm illegal/unconstitutional executive actions because the executive said "trust me bro"?

[^chevron]: The Chevron Doctrine (which binds the judiciary to trust any "reasonable" interpretation of a statute by an executive agency) exists. I think it's problematic for both separation of powers and fair notice reasons.

> If an employer discriminates
> against individual applicants or employees without even
> knowing whether they are male or female, it is impossible
> to argue that the employer intentionally discriminated be-
> cause of sex.

Counterexample: An employer has a policy that men who like Barbies and women who like Hot Wheels cannot work at their institution. They apply this policy without knowing whether they are discriminating against a man who likes Barbies or a woman who likes Hot Wheels. So apparently it's "impossible" for this to be discrimination? Alito blathers on about this point for quite a while, ignoring the fact that it is very possible for this to occur. Helpfully, the majority opinion already provides numerous other examples.

I'm not going to analyze every quote from this dissent, because it is quite long and everything is just wrong in the exact same way. Summary: the dissent argues about things that do not matter and it does not feel like Alito even read the majority opinion.

## Kavanaugh's dissent

If Alito's dissent was aggressively wrong, Kavanaugh's is wrong in a more measured sense. For some reason, I'm a lot more sympathetic to this dissent.

Initially, he makes much the same arguments as Alito does: appealing to the history of the 1964 Civil Rights Act, and failed attempts to amend Title VI/VII to include sexual orientation and gender identity. He also appeals to separation of powers, essentially saying "it's not our job to make laws, just to interpret them".

However, his jurisprudence soon diverges from Alito's: where Alito claims that *no* reading of Title VII extends to sexual orientation and gender identity. However, Kavanaugh makes a significant concession:

> For the sake of argument, I will assume that firing someone because of their sexual orientation may, as a very literal matter, entail making a distinction based on sex. But to prevail in this case with their literalist approach... **The plaintiffs must establish that courts... adhere to literal meaning rather than ordinary meaning**... Or alternatively, the plaintiffs must establish that the ordinary meaning of "discriminate because of sex"... encompasses sexual orientation discrimination.

(Note: All parties agreed that the ordinary meaning of "sex" does not encompass sexual orientation. Therefore I only focus on the bolded portion.)

This is a very reasonable framework. Where Alito has his head in the sand and does not admit there is any reading of Title VII that agrees with the majority opinion, Kavanaugh understands that one exists. Instead, his primary focus is, "should we *prefer* the literal approach over the ordinary one?" This is a much more decisive question, and honestly, many of the people celebrating the literalist approach because of the result it brought would likely bemoan such a literalist approach in another case (e.g. a 4th Amendment case).

Kavanaugh's conclusion is where I diverge from him. The "reasonable interpretation" does not mean you pick some random passerby off the streets from 1964 and ask them. Even if the average person does not know "because" means but-for causation, the "reasonable person" would have to understand this. Given that, Kavanaugh fails to explain why discriminating on the basis of gender paired with whether they applied to be a mechanic or secretary, while unexpected to the (truly) average person, is prohibited under Title VII, yet discriminating based on sexual orientation somehow is permissible. It is not the majority's job to show that sexual orientation is somehow special when being included in Title VII; rather, it is the dissent's job to show that it is somehow special and should *not* be included. The dissent fails to do this, and this is why I disagree with Kavanaugh's conclusion.

Regardless, I am somewhat sympathetic to his claim that few really expected Title VII to apply to sexual orientation, and other (subtly different) cases have been decided differently by the Court. That is why I think Title VII should be amended to explicitly include it, so everyone knows how far it extends: all the way (more on this later; note how, for instance, Bostock makes no mention of bisexual people).

One last point: Kavanaugh mentions that Clinton's 1998 Executive Order explicitly mentioned sexual orientation, and somehow this reveals that Clinton thought sex discrimination didn't cover sexual orientation. This is wrong, because even if you think sexual orientation is covered in a *literal* sense, there's no harm in making doubly sure by explicitly spelling it out. Also, explicitly banning sexual orientation discrimination is good policy: it prevents the need for cases like Bostock v. Clayton County in the first place. A years-long litigation process would not need to ensue: the (prospective) employee can simply say, "Look, they said they fired me because I'm transgender and the law literally says it's illegal right here." Even if Kavanaugh is correct in assuming that Clinton thought sex and sexual orientation as distinct concepts, Clinton could've been wrong. For much the same reason as Alito's appeal to the US military's policies, Kavanaugh's appeals to authority and history hold little water.

## Title VII should be amended

The ruling is correct. However, this ruling is somewhat narrow and may not actually amount to a blanket ban on discrimination on the basis of sexual orientation. Consider this: a man is fired for being attracted to both men and women. Sex is not a but-for cause here, because if that man were a woman, they would also be fired for being attracted to both men and women. (Gorsuch's hypothetical, where a person walks in with a woman to a company gala, would probably still protect a bisexual woman from being fired because they are dating a woman, but makes no promises as to their *general* attraction.) Perhaps this would still violate Title VII under the eyes of this SCOTUS, but the best way to make sure is to amend the law.

We saw what happened when the legislature and the public took abortion rights for granted because of Roe v. Wade. I'm not suggesting that such a similar thing might happen here, but with civil rights you do not fuck around and find out. Especially when SCOTUS has been overturning lots of precedents, there is no reason not to buttress a narrow protection with a proper, comprehensive amendment to Title VII, rather than relying on a ruling whose wider consequences may not be immediately clear.[^dissent] *Regardless of your personal beliefs*, if you have any respect for the premise of Amendment 14 and Title VII, then you should understand that immutable characteristics *such as sexual orientation* ought to be protected just the same as race, etc.

The clauses in Title VI and VII about unlawful discrimination are identical. Just as this ruling applies to Title VII, it also applies to Title VI. Title VI should be similarly amended for the same reasons. Apparently I'm not the only one who thinks so: attempts to pass the Equality Act have occurred every two years, and Biden issued an executive order that extended "sex" to "sexual orientation and gender identity".

[^dissent]: This is one place where Alito's dissent has a point. However, the "sweeping consequences" of applying the law properly is no reason to not do so. The branch of government responsible for ironing out future related laws in response to a ruling is the legislature, not the judiciary.
