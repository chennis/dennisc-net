---
title: I've been receiving some interesting spam
date: 2023-02-17
---

*I wrote this a while ago but never got around to publishing it, which is why the dates are from last year.*

I want to make my website accessible so I make it really easy for anyone to go to the footer, find the mailto link, and send me an email. That's why I haven't employed tricks such as "dchen at dennisc dot net". Unfortunately, accessible to anyone includes spammers. Of the spam I've received, the most interesting is stuff from web/app developers, probably because a lot of keywords in my site suggest I'd be interested in that.

Let's take a look at this gem from Priyankshu, sent in 2022-05-05:

> Hi there,
>
> My name is Priyanshu, and we are a team of Professional IOS & Android Apps Developer with 7+ years' experience based in India. Do you need a great App adjusted or built from scratch?
> 
> Reply me if you need any kinds of help in mobile Apps.
> 
> Let me know what you think.
> 
> Kind regards,
> 
> Priyanshu

The very next day (2022-05-06), Ameli the Wordpress expert (ugh) jumps in:
 
> Hi,
> 
> I'm a web developer who builds amazing looking websites for small
> businesses. I'd like to have the chance to show you how I can improve for
> your business.
> 
> I'm an expert in the Wordpress website platform and many other platforms and
> shopping carts.
> 
> I can upgrade your existing website or build you a new one completely that
> has all of the modern features and functionality. Would you be available in
> the next few days for a quick consultation?
> 
> I'd like to share with you my ideas and some expert advice, share me your
> Requirements and any direct contact number.
> 
> Thank You.
> 
> Ameli

I'm sure these people are copy-pasting emails because there's no way they have the technical skill to run a script to automatically spam random people with emails. I'm pretty sure these emails aren't just spam but also (attempted) scams: it is likely you'll never see the app or website these guys promise you. (I mean, what kind of developer cold-emails people without a portfolio?)

If I'm bored one day, I might scambait one of these poor chaps (or figure out if they are, contrary to all appearances, legitimate). I'd set up a burner email like notspam@dennisc.net to do it and then delete it immediately after (unless I found more people to scambait).

Oh, and a ton of people are trying to tell me that my databases for dennisc.net have been hacked. Yeah, sure...
