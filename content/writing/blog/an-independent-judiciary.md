---
title: An Independent Judiciary
date: 2023-07-18
---

Liberal media outlets are crying foul after the SFFA case, claiming the judiciary is an outdated, partisan institution that solely exists as a policy instrument for the conservatives. They call for the day when the judiciary can again be *their* political instrument (and have been even during the 2020 election), whether through more legitimate means such as repeated political victories or less legitimate means such as court-packing. Never mind the lessons of history --- apparently, some of them want to repeat the same mistake as FDR.

In doing so, they reveal that they have little interest in an independent judiciary. It's just a policy tool, one that happens to be Theirs and not Ours at the moment. Of course, conservatives played politics with the judiciary too: they blocked the appointment of Merrick Garland and still reel over David Souter not ruling *exactly* as they expected him to. But currently the judiciary's opinions please the conservatives, so they do not cry foul.

An independent judiciary is vital. Who else is supposed to interpret the law? The legislature, whose response to any Constitutional challenge would be "trust us"? The executive, who would then shape the law according to his whims? If we let those who make or execute the law interpret its Constitutionality, then the Constitution merely becomes a series of suggestions.

Living in a constitutional democracy has costs. It means a church of homophobes can picket the funeral of a gay Marine (Snyder v. Phelps). It means cakeshops and website creators can refuse to express support of a gay wedding for religious reasons. (Masterpiece Cakeshop, 303 Creative LLC). These results are detestable. Even most homophobes would side with Snyder.

But the costs of not having a Constitution are far greater. If we do not apply the Constitution impartially and abrogate the rights of those we dislike, the minorities we seek to protect today could very well become the targets of tomorrow. The Constitution admits no exceptions[^standards], and for good reason. It is rights for all or rights for none.

[^standards]: This is not strictly true. There are various standards for exceptions, such as rational basis and strict scrutiny. However, none of them are "we don't like bigots", because it could easily turn around and become "we don't like minorities".

To be fair, the Supreme Court has not always held true to this principle. Famously bad examples of judicial overreach are Dred Scott, Plessy, and Korematsu. More recently, Bakke/Grutter and Roe[^roe] are examples too. (Given this, one can hardly fault the current Supreme Court for overturning these last two decisions.) These examples of judicial overreach do not show that the SCOTUS is "broken". Rather, they show the dire consequences of the judiciary being used in ways it was never meant to: as a partisan tool to buttress policy outcomes.

[^roe]: Yes, Roe. [Loren Rosson's post](https://rossonl.wordpress.com/2023/01/22/the-paradox-of-roe-v-wade-50th-anniversary-special/) explains it very well.

Given this, what are we to make of those who claim the justices' impartiality is a sham, and that the judiciary is just a political tool? There are those who want the judiciary to become a political tool --- just one that works for Us, not Them. They do not believe in an impartial judiciary whatsoever and do not understand its importance. These people lack an understanding of the law, and frankly, common sense.

How about the rest, who wish the judiciary was "impartial" but think it is not? Their concerns are entirely based on policy: they do not like the outcome of SCOTUS cases, so they cry foul. Never mind that they only pay attention to the "big cases" --- the ones they are personally invested in --- and ignore the cases that get less press coverage, where many "conservative" justices vote just as they would.

There are legitimate arguments that some of the judiciary's rulings are incorrect. I certainly do not agree with every SCOTUS outcome. However, any such argument would have be to based in constitutional law, rather than naked policy preferences. And that is how conversations about the Supreme Court *should be*. If you have a problem with a Supreme Court ruling, find where your interpretation of constitutional law disagrees with theirs. "But muh outcome" is no reason to disparage the legitimacy of one of the most important institutions in America. It is when they apply the Constitution inconsistently, rather, that the alarms really ought to be sounded.
