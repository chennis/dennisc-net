---
title: Separation
date: 2022-01-06
---

To get good at any sport, there are two things you have to do: train and compete. In training you extend your capabilities, while in competition you display them.

Now, this isn't quite the same when it comes to academic endeavors.[^math-contests] Take programming, for instance: except for some throwaway scripts --- which don't make you significantly better at programming anyway --- there is no "practice code." _All_ of your code is supposed to be neat, organized, and checked into Git. And you're not supposed to write bad on purpose.

[^math-contests]: Though there is a one-to-one correspondence in math contests and the like.

But training isn't supposed to be bad. You're still trying your best, whether it's with a piece of writing or a workout. Training is just different --- different in the way it's performed and different in its goals.

The reason I don't do daily writing prompts (and I always despised them in English class) is because almost all of the prompts are stuff I don't care about. Writing is actually quite similar to track and field: on the surface the events all look the same. But dig a little deeper and you'll see the intricacies behind them. Why a 100m runner cannot run the mile. Why an essayist like me can't describe his first pet.[^nopet]

[^nopet]: I've never even had a pet.

So why separate my writings in the first place, instead of just throwing them all in the same place? To answer this, it's worth identifying the consequences of neglecting to do this separation.

Around the spring of 2020 to the fall of 2021, the COVID pandemic was in full force for us, and we had not reopened or returned to in person school. Naturally, this meant that every athletic competition was cancelled. This totally eroded any separation between training and competing. Maybe my willpower isn't strong enough, but I totally stopped caring about a month in.[^willpower]

[^willpower]: And if it's that my willpower isn't strong enough, then all the more reason to rely on these tools like separation as substitutes.

Another reason is because it encourages me to follow through with the writings that are less likely to pan out.[^this] There's a certain standard when it comes to my essays: there has to be a point.

[^this]: Like this essay, for instance. Early visitors to my website will know how long I referenced this (not yet existent) essay, which goes to show that even with more relaxed standards, it had to go through a lot of rewrites and rethinking.

But even if there isn't a point, it can still be entertaining. I still read other people's updates on the last however many days of their lives. And hell, separating my blog from my essays gives me room to [mess around](/writing/blog/10-traits) from time to time. Even though this isn't my serious writing, I like to think it makes my serious writing better.

Was there a point in writing this post? Not really. It doesn't make any salient points about the world, or even writing. But I wanted to write this anyway, and that, to me, is enough to justify it. That's why I have a blog.
