---
title: Tentative plans for a puzzle book
date: 2023-01-12
---

I've invented some homemade variants while setting Sudoku puzzles that I think can be expanded on or further explored, like Kakuro Sudoku, Equations, and Close Neighbors (which will be renamed into Consecutive Neighbors).

These variants are worth further exploring, but at the same time, it'd be silly to have a puzzle on this website titled "Close Neighbors 10" (whereas it might make more sense in a book). So I tentatively plan to write a small book of puzzles with these custom variants.

I will try to make it suitable for beginner/intermediate solvers while still providing fun for more experienced solvers. Some of the things I'm planning to add are:

- brief explanations of the *implications* of the rules at the beginning of each section (the reader can choose whether to read it and "spoil themselves" or not)
- possibly puzzle hints for harder puzzles
- two challenge times (one for beginners, one for experienced solvers) for anyone who wants to time their solves
- answers to each puzzle, and a worked solution for each section.

Anyway, here are the puzzles I'm planning to include:

- Classic sudoku, depending on how approachable I want to make this book for absolute newbies (7)
- Consecutive Neighbors, which is what I'm calling the "Close Neighbors" genre (7)
- Dutch Neighbors, which is what I'm calling the "Distant Neighbors" genre (7)
- Averaging circles, which is what I'm calling the "That's What I Mean" genre (7)
- Digit Neighbor Sums, which is what I'm calling 7-Eleven (and any puzzles with a similar constraint, just with the digit and sum changed) (7)
- Thermo Sudoku, as an introduction for thermo placement puzzles (5)
- Thermo placement puzzles, like Alphabet Soup (2)

This adds up to 28 or 35 puzzles, depending on whether I include classic sudoku. These numbers are not strict, and is just a guideline for about how many of each type I will include. I might write more of a type depending on how I feel, and I will probably post final specifications once the book is ready for purchase. I will probably include the publicly available puzzles, because that way I can reasonably generate a preview of the book without revealing any more book-exclusive information.

In the off chance someone would like to submit puzzles for this book, email me and we can discuss details.
