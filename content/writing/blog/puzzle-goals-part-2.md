---
title: Puzzle-writing goals, part 2
date: 2023-01-29
---

I've been meaning to write this post for a long time, but every time I sat down to work I instead just wrote another puzzle. (Go figure.) But since it's about the end of January, I figured I might as well do it now.

I've finished the January goal --- in fact, I've pretty much blown it out of the water. With 8 puzzles posted to my website and 6 more kept private for various reasons, I've been able to write at a pace of around one puzzle every two days.

## Puzzle-book

For the [puzzle-book](puzzle-book) I discussed earlier this month, it might behoove me to reconsider which variants I include more of. Equations is particularly easy to set, while Consecutive/Dutch Neighbors is incredibly hard to set. Also, *if* I want to make this book entirely approachable (rather than targeting both beginners and experts), Clue Placement (i.e. Alphabet Soup + maybe other puzzles I'll set later) might not be appropriate. Every other puzzle (so far) seems like it'll be 2-3 stars of difficulty, so it'd be weird to force in a 5 star puzzle.

Also, I don't know how milkable the 7-Eleven concept is. I tried setting a puzzle entitled "24-7" where the orthogonal neighbors of each 7 summed up to 11, but no such grid exists. (Exercise for the reader: why?) Instead, to preserve the theme and many of its ideas, I forced the 7 to have orthogonal neighbors of 2 and 4.[^private] The Averaging Circles variant seems much more tractable, since we don't have to deal with global constraints, so global digit neighbor constraints will likely cover a broader range of puzzles in the book.

[^private]: This is one of the puzzles I've kept private --- gotta leave something for the book! Instead of using cages, this time I used thermos, so if that interests you, keep an eye on the book!

In general, I'm thinking that as long as my puzzles have unique, semi-related constraints, it will be fine. It is probably going to be difficult (and somewhat boring!) to literally write 7 puzzles with the exact same constraint, so the sections of puzzles in the book will likely be roughly grouped by theme, rather than strictly organized into constraints.

## New Year Goals

As for the rest of my goals, I think for February I should probably up it to four new variants. I originally was thinking five, but between writing this post in my notebook and typing it up I managed to use Renban lines, X-Sum, and Little Killers (speaking of the latter two, I really ought to complete my sudoku rules intro). So the goal will probably be a bit harder than initially anticipated, particularly since some of the constraints I have left are annoying global constraints like Anti-knight.[^ak] But there are a ton of other variants I didn't even mention, like Kropi, Sandwich, Odd/Even, Diagonal, Anti-Diagonal, and Indexing.[^original]

[^ak]: Don't get me wrong, anti-knight is pretty fun to solve when done right, but trying to write it is absolute misery. Particularly because if I use the anti-knight constraint, I'll have to center the puzzle around anti-knight deductions or at least base a large part of it off that, and that seems kind of annoying.

[^original]: My original draft of this post had Little Killers, X-Sums, and Renban. But I just had to make it harder on myself :D

On the other and, my goals for March and April were grossly easy. (Which is a good ting, because if I set low standards I am virtually guaranteed to meet them and not get discouraged, etc.) As I've already discussed, I've managed to set two Classic Sudokus, and a few days ago I also set a Star Battle. So maybe I should up the ante a little bit and change March/April goals to *themes*. Try to set 4-ish classic sudokus in March and 4-ish pencil puzzles in April, and increase the focus in those genres for those months.

## May Goal

I've only set one 5-star puzzle so far (Alphabet Soup), and I think it's much easier for me to set 2-3 star puzzles than 4-5 star puzzles. (1-star puzzles I can do, but I don't do it as often because the space to explore is smaller with easier puzzles.) So a new goal for May: *publish* puzzles on Logic Masters Germany such that I have at least one of each rating (1-5 stars). I say publish because I'm a pretty bad judge of difficulty (I originally thought Bird was a 2-star puzzle at most). So depth/range is one of my goals now.

(Of note: I've never written a 4-star puzzle.)

## Cracking the Cryptic

I know it's been a while, but Close Neighbors has been featured on Cracking the Cryptic too. I think the debut feature was a much bigger deal for me (since, y'know, first time), and probably I will get less surprised with each new puzzle featured.

Of course, it's always going to be a nice thing to see my puzzles make it, but I think it does not make sense to make a post announcing each new feature. So I will probably mention features when they happen if I'm already writing a puzzle blog post, and otherwise, I'll probably just silently update my puzzle page.

## Taking a break?

Maybe I should take a break from setting or reduce the pace to get my life kind of together. (Right now I'm barely even a functional human being since I spend so much time setting.) I say this, but I know with pretty high probability I won't intentionally do this. A couple of tings that will naturally make me slow down, though:

- Track and Field is about to officially start
- In February I'll be spending 10 days grinding out my hardware project for Hack Club
- In March I will be attending [WARP](https://warp.camp) and milking as much as I can out of that experience (which means I cannot spend hours setting --- in fact, I expect to set 0 puzzles in that timeframe)

There are other things I want to do, like finally finishing my Git server[^college-apps], pivoting back towards teaching for Math Advance, etc. So yeah, a steady pace of about 3-5 puzzles a month (and not 14) is probably what I'll converge towards.

[^college-apps]: Fuck college apps. Seriously. They completely killed any momentum I had going.

Thanks for reading, and thanks for trying my puzzles.
