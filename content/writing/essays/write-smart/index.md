---
title: Write for Smart People
date: 2022-02-24
---

Writing can help you sort out your thoughts, some of which you may not have even known you were thinking. Yet writing can also be expository, like in a textbook. In this regard, it is important to consider your audience --- if you make an unreasonable assumption about what they know or how they think, your writing will fly over their heads.[^egmo]

[^egmo]:
    This is why I am a little skeptical of claims like ["this text has no prerequisites"](https://web.evanchen.cc/geombook.html). It may technically be true, but if you have never done something intensive like olympiad geometry before you will get lost very fast.

    Although I think with EGMO, the qualifiers "written for competitive students", "any student with proof experience", etc are sufficient. Many people smarter than me have been able to start learning olympiad geometry through it first try. This textbook is written for smart people, and I think that's a good thing, even if "smart" precludes me.

    For an example of textbooks that aren't written for smart people, take any AoPS textbook besides Volumes 1/2, especially Introduction to Geometry. It assumes that people don't know what a point/line/angle is, or how to label/read a polygon, so it spends 50 pages on this. Nuances like $ABCD$ and $ACBD$ being different polygons are important, but these can be covered in a tenth of the space if you just state them straight out.

Traditionally these two goals of writing have been divorced. You are either writing for yourself, or for someone else. Diaries for you, articles and textbooks for other people. This results in writing that is nearly incomprehensible: think high school math textbooks. I doubt that many high schoolers have thought about their ideal math textbook, but if they did I bet it would look nothing like what we have now.

The only idea you can express properly is one you yourself have. So the closer you can get to writing for yourself, the better. And when it comes down to it, the closest you can get is writing for smart people.

## First Principles

In the fictional SCP Foundation there are a series of stories about [antimemetics](https://scp-wiki.wikidot.com/antimemetics-division-hub), entities that make you lose your memories. And the way the characters combat these _antimemetics_ is by preparing notes or clues such that they can rederive everything they knew, as quickly as possible.

And that's what writing is. You don't forget things nearly as quickly as in the SCP stories, but memory is still unreliable. Your working memory can hold maybe seven distinct pieces of information at a time. That's barely enough to keep a sentence or two in your head at once. In fact, when I write these essays, I typically write one phrase at a time, or even just a couple of words. That's because I can go back and retrace my stream of thought, in a way that I can't when I'm just thinking.

But writing is not merely a recording of thought. From a standpoint of artistry and efficiency, you are trying to move through _as fast as possible_, without leaving anything out. This implies that not all writing is equal: if one piece can express a point more clearly and succinctly than another, it is better, at least in that regard.

Fast doesn't mean short though. Fast means easy for whoever's reading. Think about code: ninety-nine times out of a hundred, you prefer the straightforward solution over the clever one-liner, even if it's a bit longer and runs slower. Because what you write shouldn't just make sense to yourself now --- it has to make sense to you five years in the future too. That's why it's useful to write for prosperity and to have an audience: you won't get caught up by any assumptions of any specific knowledge you have or your state of mind at the moment if you don't make them to begin with.

Information isn't only contained in the facts themselves; it's also contained in how they are arranged. And it is much easier for someone to process information through structure than isolated facts. Try to think of every formula for the area of a triangle that you can. You probably listed some subset of the following:

- $\frac{bh}{2}$
- $rs$
- $\frac{1}{2}ab\sin C$
- $\frac{abc}{4R}$
- $\sqrt{s(s-a)(s-b)(s-c)}$

This is the way every textbook teaches the area formulae and the way most students learn it. You'd be fortunate if a textbook even provided you such a list; all the textbooks I've seen separate them into separate _chapters_, if it even includes all five.[^examples]

[^examples]: AoPS Introduction to Geometry is really bad in this regard. Evan Chen's EGMO helpfully lists them all out and proves them (though I think its proof to Heron's is one of the more complex ones), but due to its target audience said section is quite terse.

Thinking about the area formulae this way is not correct. Instead consider the following chart, excerpted from the draft of my introductory geometry textbook:

<img src="/writing/essays/write-smart/area-chart.png" alt="Chart of area formulae">

If you already knew the proofs, their connections are self-evident through the chart. And if you didn't, now you know what to expect. In any case, memorization is wholly unnecessary, because the proofs suggest themselves.[^memorization]

[^memorization]: In writing this section of the essay I had to list out the five area formulae. I only succeeded in remembering them all when I pictured the chart in my head.

## Importing

You don't want to explain every concept you use from scratch or it's going to distract from the point you're trying to make. And not all words are created equal: certain words and concepts are more _expressive_.[^programming]

[^programming]: It is no coincidence that _expressiveness_ is also used to describe programming languages.

Think about importing a library in a program. In theory, you could reimplement it all on your own, but you have no reason to, and in any serious application that would be completely infeasible. So the same goes for writing. You couldn't possibly hope to explain every concept in purely theoretical terms, devoid of any context from the outside world. So we use examples, analogies, and shared knowledge in order to communicate our ideas.

See what I did there? By taking something familiar and comparing it to my idea, I was able to explain it more concretely.

All of this is contingent on the reader understanding the basics of programming, or at the least, being willing to look it up. This assumption precludes at least ninety percent of the world from reading my essays. That's okay, because I'm not writing for those ninety percent.

But the title of this essay isn't "Assume Your Audience Knows Everything You Do". For some readers, a couple of points in an essay might not land. Maybe even an entire essay. You want to write for smart people because you have reason to believe this will change.

This is why rereading is so important. You might not have gotten all the information or understood all of the examples the first time through. And if you're smart, you can expect to see something new with each reread. **This is true even if you're the one who _wrote_ the essay.**[^typo] It follows that [any good writing is worth rereading](/writing/essays/reread), because if a reader can absorb everything in one go, then it must not be very information-dense.

[^typo]: [Case in point.](https://gitlab.com/chennisden/dennisc.net/-/commit/8c5ab2aa36f1cf3cd130a5cb4826225bda4464e3)

You can trick yourself into getting very far even if you don't have anything to say when you're writing for stupid people. Think 99% of notes from private tutors (if you've ever had one): when you are alone and reviewing them, you realize they are just unintelligible chicken-scratch and you're better off just reading a textbook. You can't get away with this when you're teaching, say, math olympiad kids, because they're literally some of the best high schoolers in the nation. And nor do I think you would want to. There is a sense of respect you have when writing for smart people, and that alone makes your writing so much better. If you respect the time of your readers, you will respect your own time when writing.
