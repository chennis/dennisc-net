---
title: How to end an essay
date: 2022-09-10
---

Merely ending an essay with a summary or restatement of your ideas feels wrong. The main points of your essay should be [obvious from its structure](framework), and if you want your readers to refresh themselves you should make it [easy to reread](reread).

Nor does a "big takeaway" work. If your essay is long enough to have some sort of overarching conclusion to draw everything together, you shouldn't be saving your main points for the end anyway. Otherwise you'll leave the reader scratching their head while you're refusing to tell them what your examples mean, and by the time they get to the end they'll have forgotten. People can only remember so many things at once. 

End your essays with advice. It won't work for everyone who's reading, but it should be relevant for people who're facing what you're describing. Try to make it positive advice as well: describe what your readers should do instead of listing a subset of what they shouldn't.

What should your readers do after reading your essay? How should they change? In a way, this is a "big takeaway" --- with the extra restriction that it's specific and actionable, even if not immediately so.
