---
title: Use the most specific word possible
date: 2023-10-01
---

*The premise of this essay will seem extremely obvious, but this is a concept too few people have drilled into their head.*

Today a student in my Fall 2023 AIME course submitted some solution writeups which contained the following line:[^simplify]

[^simplify]: I'm simplifying this example (i.e. making up all of the details) because the actual factorization doesn't matter; just the word choice.

> We can simplify $x^2 + xy$ into $x(x+y)$.

And the gist of my feedback was,

> Instead of saying *simplify*, it's better to say *factor*.
>
> Imagine you are teaching a class to students. And you say "do a thing to this
> thing and get a thing" instead of saying "apply the function $f$ to the
> integer $x$ and receive a string". Very unspecific, right?
> 
> In my eyes, "we can simplify" is like that. It could mean a bunch
> of things: you could be expanding a factored polynomial, factoring an
> expanded polynomial, cancelling terms, etc. It's basically the same
> as "we do a thing to the equation". What thing?
> 
> When the word "factor" is used, it is much clearer.

To be clear, the proof was written very well (which is why I bothered to be nitpicky like this). But this is a good segue into some thoughts I have about proofs and communication, so I'll use it as an excuse to talk about them.

Being *clear* in a proof is the most important thing. Sure, it should be reasonably correct, in that it shouldn't claim anything that's untrue and should be reasonably[^logic] complete. But the primary responsibility of a proof-writer is to make it clear, "Hey this is what I am doing right here and now," at every step of the proof. Yes, compromises need to be made between being explanatory and being concise. But using the most specific word possible is being more explanatory without being any less concise. In other words, it is an improvement you can get for free, so take it.

[^logic]: "Reasonably" because fully proving some things like the Soundness Theorem would be a major pain in the ass. Especially in cases like these, the *vibes* of these proofs are more important than the details. No one cares about the details, they are trivial once you get the vibe down.

Pedantic? Yes. But when you're proving something, your job is not to say "and the things I said are *right*". It is for the reader to say, "I see what you did, I am convinced it is correct, and I get what you're doing." So even if being more specific doesn't make your proof more correct, it is still just as valuable.
