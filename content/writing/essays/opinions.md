---
title: Opinions
date: 2024-07-19
---

It's okay to have opinions about things you aren't skilled at. Good, even, because having fervently held, *correct* opinions is how you become skilled at something.

Without opinions all you have are a set of facts. Opinions are the backbone of a [framework](/writing/essays/framework). 

We've all seen those side-by-side factual comparisons of GitHub vs. GitLab, Postgres vs. Mongo, React vs. Angular, so on and so forth. And they are *boring*. It's like it was written by a person without a soul, because rarely is an actual opinion presented. And just as importantly, these comparisons are completely unhelpful.

If you search up "GitHub vs. GitLab" and click on the first comparison article you see, chances are **it won't even mention the nested directory structure**, let alone emphasize it as the main sell. Even [GitLab](https://about.gitlab.com/why-gitlab/) cannot articulate the main feature it has --- you know, the one that GitHub will *never* be able to replicate --- and instead chooses to populate their page with marketing nonsense. You can even go on Reddit, which for all its flaws at least has sentient human beings with opinions, and not a single person mentions this crucial difference.

And you're telling me the first person to realize this and write about it was [a high school freshman](/writing/tech/gitlab).

Opinions like these, fervently held and correct, lead to a better command of the facts. Pushing the idea that "Git hosts should store repositories like my filesystem" further led to me drafting the design of my own Git host (something I should actually finish). My geometry students learned the [area formulae](/writing/essays/write-smart/area-chart.png) in twenty minutes rather than two months because I had *really strong opinions* on how they should be presented.

 You're unlikely to fervently hold an opinion if it's uncontroversial. You might believe that Minecraft is an awesome game, but this is also deeply obvious to the rest of the world. It's not exactly the kind of thing you need to convince someone else of. So the balance of opinions you strongly hold will be *controversial*.
 
To be clear, by "controversial" I merely mean that it is not a widely held belief. It does not necessarily mean people are actively disagreeing with it. For instance, I believe that hand-crafted puzzles are incomparably better than computer-generated ones. No one who has seriously thought about logic puzzles would disagree with that statement. But the average reaction is still along the lines of "does it really matter that much?" In other words, it's not the kind of belief that people take for granted.

So if you have strong opinions and act on them, you are going to disagree with other people. And some of them will have more experience in the relevant field than you do. (Or at the very least, it will be something that a lot of experienced people have overlooked.) And I don't mean a milquetoast sense of disagreement like "Professor, I think you wrote a plus here when you meant to write a minus". I mean something far more fundamental.

Fortunately, this doesn't mean you are necessarily wrong. There are plenty of *gaps* in the world.[^startup] Sure, on the balance, people who have more experience in a field are typically more likely to be right. But all you have to do is be right *once*, confirm you indeed are right where everyone else is wrong, and then patch that gap to the benefit of yourself and those around you. Boom. All the sudden, having strong opinions --- even if they conflict with other people's --- no longer becomes a bad idea.

[^startup]: Startups are great evidence that these gaps exist, because people are willing to invest millions in the mere possibility that there is a gap to fill in, that the founders have identified it, and are able to execute so successfully that the investment pays off. For this to make sense, there better be a *lot* of gaps to fill.

---

Sure. If you are young, you cannot possibly have fifty years of experience building websites, by virtue of not having fifty years of experience *being alive*. And to address the elephant in the room, just so neither of us ignore the fact: I am not even twenty. I write about puzzles even though I haven't been constructing for very long, I write about math even though I don't have my bachelor's degree, and I write about programming even though I've never even held a full-time programming job. I am just some kid on the internet, screaming my thoughts into the void and hoping that someone comes out better for it.

In the grand scheme of things, I don't have a single clue what I am doing. There is a very good argument to be made that I should shut up, put my head down, and just listen to the experts. On the surface this would be a commendable display of humility. But the price is not thinking for yourself, and that cost cannot possibly be worth it.

But just between the two of us: I have a sneaking suspicion that even the older people have less of a clue than we might expect. I won't know for sure until I become older and hopefully wiser, if ever, but there are a lot of signs that often the "experts[^grift]" are clueless too.

[^grift]: Sometimes the "experts" are completely full of it. But it can be hard to tell the professionals apart from the grifters at a distance, especially in very noisy fields like programming and personal health.

- Who thought standardized testing was a good idea? You put a bunch of guys in a room who've been doing this their whole lives, and this is the best they can come up with? Seriously? You could not find a *single better way* to make sure kids were learning?
- AP Computer Science is still in Java, and I hope everyone involved in this decision drops a 45-pound weight on their foot.
- Why on earth are single-page applications still a thing?
- "You don't need a gym membership to get in shape." Thanks, mister health guru. You also don't *need* to ever read a book, you nitwit. But you don't go around telling people they don't need to read, now do you?
- Why are all you monsters still doing NYT Sudokus. You sick freaks.

Fundamentally, most people [are clueless about most things](/writing/essays/competence). And I'm not convinced this changes just because people get older. And it's almost certainly not the case that it changes *so fast* that *all your opinions* as a 20-something year old or whatever become *entirely worthless*, because everyone else has gotten it figured out already. I refuse to believe that, because there is no way that is true.

---

At this point, you might be guessing the takeaway is to start thinking for yourself and form your own opinions. But that'd just be preaching to the choir. Rather, the real takeaway is *not to be ashamed of having strong opinions*, even when they are in deep contrast with everyone else's, so long as you've done some reasonable amount of due diligence. In other words, take your own ideas seriously, even if (or perhaps especially if) no one else is saying the same thing.

It is okay to be wrong. And it is okay not to be ashamed of being wrong, much less just the *possibility* of being wrong. So long as you take proper care to not unduly harm or burden other people with them, holding strong opinions serves as an outlet for tremendous growth, even if you are wrong in the end. Because all it takes is being right *once*.
