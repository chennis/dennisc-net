---
title: ChatGPT, by ChatGPT
date: 2023-03-21
desc: ChatGPT's version of my essay claiming that ChatGPT won't actually threaten humanities classes (except the bad ones).
---

Prompt: Write an essay about why ChatGPT is not a threat to humanities classes because people should not be writing like a chatbot anyway. Compare this to how computers cannot generate good sudoku puzzles and are merely an assistant.

> As an AI language model, ChatGPT is designed to generate human-like responses to written prompts. However, this does not mean that it poses a threat to humanities classes. On the contrary, people should not be writing like chatbots, and AI language models such as ChatGPT can serve as useful tools for students and educators.
> 
> One of the primary goals of humanities classes is to teach students how to express their thoughts and ideas in writing. While AI language models such as ChatGPT can generate grammatically correct and coherent sentences, they lack the nuance and creativity that humans bring to their writing. Writing is a form of expression that reflects an individual's unique perspective, experiences, and voice. Chatbots, on the other hand, are designed to mimic human language without having an individual perspective.
> 
> Furthermore, the ability to write well is a valuable skill that is highly sought after in the professional world. Employers are looking for individuals who can communicate effectively in writing and express their ideas in a clear and concise manner. Relying solely on AI language models to write on our behalf would not only hinder our ability to develop this skill but also undermine the value of human creativity and individuality.
> 
> It's important to note that AI language models such as ChatGPT are not meant to replace humans in the writing process. Instead, they can serve as useful tools to assist individuals in their writing. Just as computers cannot generate good Sudoku puzzles on their own, they can serve as an assistant to those who enjoy playing the game. Similarly, AI language models can help students and educators in generating ideas, suggesting sentence structures, and correcting grammatical errors. This, in turn, can save time and increase productivity.
> 
> Moreover, AI language models can be used to analyze and evaluate written works. By inputting essays or articles into the system, students and educators can receive feedback on the clarity, coherence, and organization of their writing. This feedback can help them improve their writing skills and develop their own unique style.
> 
> In conclusion, ChatGPT is not a threat to humanities classes. People should not be writing like chatbots anyway, as writing is a form of expression that reflects an individual's unique perspective, experiences, and voice. Instead, AI language models such as ChatGPT can serve as useful tools to assist individuals in their writing, just as computers can assist Sudoku players. By using AI language models as an assistant rather than a replacement, students and educators can improve their writing skills while maintaining the value of human creativity and individuality.

Yeah, ChatGPT kind of sucks. Its weaknesses?

- Way too verbose
- Its tone is always the same; it doesn't know how to *emphasize* anything
- **It lacks domain-specific knowledge**, particularly about soft skills (e.g. creativity, intent)[^sudoku]

[^sudoku]: Just look at how badly it botched the explanation of sudoku puzzles! You don't even need to have written or done a single puzzle in your life to tell ChatGPT is full of shit.

ChatGPT as of now is just a toy, one that happens to have a modest level of utility for smaller/less high-stakes situations. And yes, *maybe* I could've gotten it to write like me if I engineered a prompt to do that, but that would've literally taken longer than writing it myself.
