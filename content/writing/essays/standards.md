---
title: Good writing is about standards
date: 2023-09-15
---

Everyone knows that *one person* who just can't clean up after themselves. You look at their room or whatever and it's super nasty, and any reasonable person would have gone "ick this feels wrong I need to fix it" by that point, but somehow they don't.

This is the kind of thing good writers want to avoid. A good writer can know that a sentence *feels* wrong, not because of some specific grammatical rule, but because it just seems nasty. Where other writers might think, "I have expressed my idea, this is fine", a good writer will only be satisfied with "I have expressed my idea and *it sounds right*."

A good writer also doesn't follow arbitrary rules just because some authority figure told them to. They have a good instinct for what conventions they should naturally adopt. For instance, it makes perfect sense for me to use the personal pronoun when signposting. When English teachers tell you not to use the word "we", they're wrong: *even English papers use "we"*. That's because it sounds right.

If you want to write well, develop good hygiene. Learn to be icked out when phrases or sentences are awkward.[^personality] Be aware of how your sentences sound. And above all, when your writing starts getting nasty, clean it up. Make it a compulsion. If you can't tolerate bad writing, you won't do it. It doesn't get any simpler than that.

[^personality]: Unfortunately, I am unsure of how much this skill can be developed. Tidiness is somewhat intrinsic, and some people cannot find it in themselves to care. 
