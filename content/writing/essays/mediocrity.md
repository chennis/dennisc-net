---
title: Settling for Mediocrity
desc: Why we no longer focus on the quality of our work and how to remedy this fact.
date: 2022-01-29
---

The other day I read something from GitLab that shocked me.

> **We care about what you achieve**: the code you shipped, the user you made happy, and the team member you helped.

On the surface this seems self-evident. The reason you have a job is to get something done. And yet this quote is surprising all the same.

Our culture currently places a disproportionate emphasis on effort rather than results. Rather than being a means to an end, effort is seen as the end itself: if you're doing your best, you ought to be taken care of. It is this reasoning that dooms people to fail their classes. If you're putting in effort for effort's sake, you're going to miss all the ways you could've gotten better results.

This reasoning is backwards. We should not expect people to push themselves to the very limit to be rewarded. Taking breaks and even slacking off is perfectly fine if you can still achieve the same results. Here's the rest of the quote:

> You don't have to defend how you spend your day. We trust team members to do the right thing instead of having rigid rules. Do not incite competition by proclaiming how many hours you worked yesterday. If you are working too many hours, talk to your manager to discuss solutions.

We shouldn't expect people to roll boulders up a hill just so they'll be taken care of. We should take care of people so they'll actually _get things done_. But by design we reward effort and not results, because in the past two things were true: results were proportional to effort, and it was easier to measure effort. Thus it became and remains the substitute for results.

## Roots

Up until the industrial revolution, the vast majority of people worked in agriculture, only producing enough food to live on --- and that's only if they were lucky. So when the agricultural revolution (which coincided with the industrial revolution) began and more food could be produced, large portions of the population were free to specialize for the first time.

It is no coincidence that public education expanded and later became mandated as the industrial revolution reached its climax. Yields in agricultural productivity allowed for public education to take root in our society. But these same forces with allowed for specialization also demanded it.

Education has always existed in some form, but it was only after the industrial revolution that it became systematic. But public education was not created to serve the interests of the students, their parents, or their community. It was meant to serve [factory owners](https://qz.com/1314814/universal-education-was-first-promoted-by-industrialists-who-wanted-docile-factory-workers/), and more recently in the United States, [governments, Wall Street](https://www.rollingstone.com/politics/politics-news/how-wall-street-profits-from-student-debt-225700/), and [big tech](https://www.businessinsider.com/google-apple-microsoft-competing-dominate-education-technology-market-2018-11).

It might be worth looking at some aspects of the school system through these lenses to get some historical context. Why do you get _suspended_ for being tardy?[^unenforced] Because factories had absolutely no tolerance for workers that showed up late. Why is the school day so regimented? Because workers are on the clock. Is general horseplay in elementary school really so dangerous? Of course not. But there is a place where you absolutely cannot be running around carelessly: the factory. To be sure, the higher up you go in the education ladder, the less true this is. But this is because higher education came later, and its main functions did not include training industrial workers.[^industry]

[^unenforced]: In all fairness, these draconian tardy policies rarely get enforced. But the fact they exist is a sign that schools do not trust their students, and the fact they even _threaten_ such action is a vestige of the industrial era.
[^industry]: This seems to be less true now, since more and more jobs have been requiring a degree of some sort.

In the early industrial revolution, excellent workers were not much more efficient than merely competent ones. And even if a worker is more efficient than another, they'd get worn down soon owing to the horrible labor conditions of the time. There was no reason to promote excellence; it took time, wasn't necessary, and didn't last. It was more prudent to spend money and influence to increase the number of competent workers.

In the factories, effort was pretty much the only easily mutable trait that affected productivity. Physical condition? Seeing how underfed the workers were and how frequent workplace accidents were, that's off the table. And besides, you can't force them to become fit. But you can get them to go just a little faster, and to do it right away. Why else would school push people to the ends of exhaustion? Surely no one believes that anyone can consistently study for seven good hours every day: six hours of school and one hour of homework, if we're being generous. But that's only for higher-level learning; these long, monotonous hours are perfect if you're preparing kids for the assembly line.

We're seeing a shift from physical labor into intellectual labor. And I really mean labor. It is menial, it is uninteresting, and perhaps most importantly, it is replaceable. The training process is longer, and it is less likely that any particular person can do a particular job, but it is fundamentally the same.

If we look at standardized testing, it's obvious that schools are meant to ensure a baseline of competence rather than promoting talent. You can't be telling me anyone actually believes [this](https://satsuite.collegeboard.org/media/pdf/sat-practice-test-1.pdf) is a good way to figure out who's good at math.[^pdf][^high-standard] It's not even enough to evaluate basic competence. But keep in mind that these math standards are too low for the present.[^rise] They were perfectly fine during the beginning and middle of the industrial revolution, which is exactly when the public education system was constructed. College Board is still stuck in the nineteenth century, right where the rest of the education system is.[^20th]

[^pdf]: The relevant section begins at page 36 of the PDF.

[^high-standard]:
    There will certainly be people who will chide me for calling the math section of the SAT easy. I am aware that many people struggle with this test. To those people I have two responses.

    1. The SAT section of math is objectively easy. Any other country's high school math exam is orders of magnitude more difficult. Many Americans are hopelessly bad at math, and that does not make the test any harder.

    2. There is no reason to assess those who do are bad at math and/or do not intend to pursue math. It does not make sense, even from an economic standpoint.

    In short, the SAT does not differentiate between people who will be working with math, while brutally punishing those who will not. That, not its difficulty, is the real problem with the SAT.

[^rise]: The math section of the SAT is a little harder than it was [a hundred years ago](https://www.erikthered.com/tutor/SAT-1926-Subtest-2.pdf), as some topics have been added, such as slopes, means, and standard deviations. But the test is still heavily influenced by its history, much of which is [far more significant](https://www.nea.org/advocating-for-change/new-from-nea/racist-beginnings-standardized-testing) than being historically easy.

[^20th]: College Board administered its first exam in the twentieth century, but the [noxious philosophy](https://en.wikipedia.org/wiki/Eugenics) that drove it originated in the late nineteenth century.

Think back to elementary school: no running on the blacktops, no doing anything but what the teacher tells you and when, and no going out to play until you're dismissed from your lunch table by the yard duty. Are these descriptors not out of place for a factory? In a factory, there's no such thing as "inspiring creativity": you're doing the same thing over and over. School is the same way: do it according to standard.[^whims]

[^whims]: The primary difference is now we get some degree of choice. You are occasionally given time to work on whatever you please. But it really speaks to how awful school is that you're not supposed to have agency by default. 

Report cards and grades have been characterized as obedience letters so often that we've forgotten [citizenship grades are real](https://core-docs.s3.amazonaws.com/documents/asset/uploaded_file/1384112/Citizenship_Grades.pdf).[^hs] This is very scary: trying to quantify the "social ability" of a child is already silly, but what's worse is these evaluations are taken at face value. We're enforcing discipline into our children at a young age: *but whose idea of discipline?*

[^hs]: Yes, this is a *high school*. Ugh.

It's easy to say that these "citizenship grades" are not a big deal because pretty much everyone gets a good score here. It's like participation points. This is less of a problem in high school, where everyone and everything is chill most of the time, but take this from someone who's been hosed by these "behavior grades" before: it ultimately ends up targeting people who have ADHD, autism, etc --- in other words, people who aren't "normal". I don't think it's on purpose, at least most of the time, but it's what happens.

But I don't think these standards are targeting anyone in particular. They just promote effort. Effort, according to the rules: the teacher's rules, the school's rules, and society's rules. You have to put effort in the right place and the right time exactly when you are told, because products have to be made and the assembly line isn't waiting for you, whether you like it or not.

## Paradigms

So industry is the origin of settling for mediocrity. But why does it persist? Inertia alone isn't enough to account for the wide effect it's had. It's the result of two separate paradigms: one defined by successes, and one by failures.

Inventors, innovators, and writers operate under a success-based paradigm. Take Thomas Edison as an example: the fact that he failed a thousand times doesn't take away from the fact that he eventually succeeded. But if a doctor or a pilot screws up once, no amount of successful operations or flights will save them. So they operate under a failure-based paradigm.

It is the latter of which causes people to turn towards mediocrity. And rightfully so. The world may be a better place because of the amazingly talented pilots in flight shows. But to me it is far more important that the pilot flying my plane has basic competence.

A potential point of confusion: in a success-based paradigm failure is the default and vice versa. Under a success-based paradigm, a success is _notable_; so it follows that success is not the expected result.

Up until very recently it made sense to look at the world through a failure-based paradigm by default. Now society has tried to move in the other direction. But the switch to a success-based paradigm might be just as harmful. At the least, it is certainly dishonest.

You know how teachers and counselors say a B isn't a big deal in high school? Sure, one bad grade might not be the end of the world. But they are insinuating that you can experiment and fail and adapt, and in reality, you don't have that sort of leeway with your grades.

And this isn't just for the students that get straight As. It's possible that people who usually get Bs and Cs view the few As they get as successes. But even the C students have to worry about passing all of their classes. In this sense, only the worst students have a standard of failure, and by then paradigms are the least of their concerns.

School follows a failure-based paradigm. But why is that so dangerous? Because to learn you have to break past your limits. If you only take classes you'll succeed in, what are you getting? Validation? Because those classes won't force you to think in a new way. And it is [frameworks](/writing/essays/framework), rather than facts, which really matter.

In math olympiads[^oly-details] the third problem is usually the hardest. Yet it is approachable and even solvable surprisingly often. The important thing is that you pretty much have to look at the third problem, try it, and potentially make some progress. Surprisingly often you can learn from the third problem even when you don't come close to solving it, and the only reason your brain is receptive to learning after the contest ends and solutions are posted is because you tried the problem for so long. Otherwise the context of the problem, why someone might try a certain approach, and so on would make no sense.

[^oly-details]: A math olympiad has two days with three problems in each day. What I really mean is the third problem of each day, but for simplicity's sake I treat each day as a totally separate contest here.

School is obscuring the existence of this third problem and giving people the choice not to look at it, which is a shame. It's accepted in math contests that you don't learn anything by doing problems well below your capacity. But the only classes you should take are the ones well below your capacity. That's why school is so boring: if it weren't, you'd be shooting yourself in the foot.

## Spontaneous Generation

Up until now I've never explained why settling for mediocrity is bad. And the truth is that it isn't, at least not intrinsically. There are times where half-assing something is not only a good option, but the best. Think about diminishing returns --- is it really worth it to put in your very best into school and get 98s, as opposed to breezing by with 96s?[^rank]

[^rank]: If your class rank is computed through a numerical average rather than a letter average, this does not apply (and you have my condolences).

In fact, this is a good heuristic for when to settle for mediocrity. Do you get something substantial out of the extra effort? If not, why bother putting it in?

I am not arguing that everyone should give everything their all. But there are benefits to working your hardest towards _something_.

Consider the smartest person you know. Chances are, they probably are knowledgeable in a variety of topics. In fact, anyone sufficiently good at anything ends up being a polymath. There are very few people who are only knowledgeable in one field. These people, by and large, are _polymaths_.

Where do they come from? Spontaneous generation is the idea that living organisms can be produced from non-living matter; that is, they appear with seemingly no cause. Scientists of the time did have some idea of when organisms would appear: around meat, cheese, and filth, for instance, but they didn't know _why_. Our ideas about smart people are much the same.

Just like with small organisms, smart people don't appear out of thin air. There is a force driving them to learn so much, and to learn it well. I don't think it's quite curiosity; curiosity only implies desire, not action, and it implies that there is some stimulus causing said curiosity. A more accurate descriptor for this force would be _whims_.

Traditionally we view people who succumb to their whims with disdain, and for good reason. Someone who neglects to do their homework because they feel like partying won't get far. But wait a minute. What about the people who neglect their homework because they're working on their research, or the people who drop out of school to build a company? These are two sides of the same coin.

The common model of studying is that you consciously decide to sit down and pull out your calculus textbook, setting aside time and energy every day to work on your differentiation skills. This model implies that Barry is better at calculus because he has more discipline and spends more time studying. When you realize that Barry _isn't_ so diligent (and in some cases, the last time Barry opened the textbook was in August!), you come to an even worse conclusion: Barry is just naturally smarter.

It doesn't help one bit that Barry probably _is_ smarter, because it obfuscates the important thing: intelligence isn't what lets people cruise through. And nor is it diligence.

Let me tell you a secret. None of the smart people you know are all that disciplined. In fact, they tend to have a very casual attitude with regards to their studying.[^parents] Sure, self-control has some role in success. But it isn't enough to account for everything. And above all, self-control is a conscious choice you have to make. And smart people don't have enough mental energy to constantly exercise self-control: they're too busy getting things done.

[^parents]: To all parents of smart children: leave them alone (unless they ask you to be involved). The abilities they exhibit have nothing to do with the math and everything to do with the cognitive independence they are exercising.

So how do people really get good at something? It's because they're drawn to it. More specifically, they can't go a day without doing it. As an anecdote, the night I finished the first draft of this essay, there were two things I wanted to get done: my English homework and this essay. I had maybe an hour left to do the homework[^belief] so it was only logical to do it first. But I didn't do that. I ended up opening both Google Docs and Vim[^no-coincidence] and I ended up writing this essay first. That's how strong your whims have to be.

[^belief]: At least I thought it was due the next day. Turns out it was due the day after.

[^no-coincidence]: It's no coincidence that I used Google Docs for the thing I half-assed, i.e. my English homework, while using Vim to write this essay. In my sophomore year when I started programming, I did my homework using Replit (since we had to submit with it anyway) and used Git + various IDEs for my pet projects.

All the literary greats have written dozens of novels. Anyone who knows at least three languages knows three more. To an outside observer talent is the explanation. But when all is said and done, ask yourself this: where are the people who've only gone halfway? You might know someone who's only done a couple of things. But chances are they're hard at work on the next.

Whims are sufficient for getting work done. But are they necessary? Because you have a limited amount of attention resources, yes.

Attention resources is a theory that states the energy used on fairly independent activities and decisions is finite. This intuitively makes sense. The internet isn't distracting because it makes an assignment take longer per se. It's because it takes effort to switch away from something like Reddit or Twitter.

Importantly, it is usually harder to start something than it is to continue something. You actually have to make a decision when you're starting something, whereas you only need to _not_ make the decision to give up to continue. Think of running: it's much easier to finish a workout if you're running the entire time versus if you start walking in the middle of it. In the second scenario, you have to summon up the motivation to start running again.

When you're doing SAT practice or writing an essay for English, you will constantly have to stop to "walk." You have to think of totally new and often disparate ideas for each new question and each new paragraph, and each time you are mentally switching contexts. The reason writing an English essay takes hours isn't because five hundred words take so long to write. Anyone could write that many words in solutions to the AMC 8 in fifteen minutes if they focus. It's because by the time you're done, you've almost depleted your store of attention resources.[^diff]

[^diff]:
    The difference between an English essay and this essay is largely in the structure. The ideas in an English essay are often disjoint, only depending on the thesis. To see this, imagine removing one of the body paragraphs and adjusting the signposting in the introduction and conclusion accordingly. (Typically these essays have a thesis of the form "claim + summary of each body paragraph" and you would have to remove the summary of the body paragraph, and similarly for the conclusion.)

    An English essay is like a tree with depth 1; the thesis is the root node and everything else is directly connected to the thesis. This essay is like a line; it goes from point A to point B and so on. So in effect, you have to transition twice as often: instead of going directly between your points, you have to make a pitstop at the thesis every time.

    I'm not claiming that either model is better or worse. In fact, if you are a lawyer trying to argue that your client is innocent, you better make sure everything ties back to that thesis. But writing tree-style takes more energy than writing line-style.

    (If you want to be pedantic, a "line" of connected vertices is technically a tree. But you know what I mean.)

Following your whims, by definition, takes the minimum amount of attention resources. There is very little you can do to change the amount of attention resources you have; the only substantial changes I can think of are getting good sleep and exercise. So you have to try and skimp on how many attention resources you are spending. That's why you want to manipulate your whims: to reduce the activation energy required to do what you want.

Just like polymaths, whims don't come out of nowhere. The first and most obvious ingredient is exposure. You don't need a lot of it, and it doesn't even need to be good. Even though AP Computer Science is the worst designed class I have ever seen[^calcbc], it was enough to get me seriously thinking about programming again --- first through USACO and then through creating applications --- and ultimately to follow through. I begrudgingly give it a little bit of credit for where I am now.

[^calcbc]: It's somehow even worse than AP Calculus.

The second ingredient is the hardest one to reproduce. It is a personality type that naturally gets led around the world. Maybe for a year they will be interested in Italian. And for the next three they'll be interested in writing books. Occasionally, they might even get led around on a wild goose chase that leads to a vast and sinister conspiracy --- with the subtle qualifier, of course, that it is completely fictitious.

The world is not kind to these people. It is filled with traps designed to ensnare them and exploit them for profit. I have no doubt that some of the smartest people in the world are currently wasting away in the bubble of social media. And so there's a third ingredient that's necessary: discernment.

For these sort of people, it's very easy to get invested into something, and very hard to get out. So you have to be able to tell what sort of work is meaningful, and what sort is nonsense.

There are no absolutes. But one of the heuristics I use is this: would it make a difference if I didn't do it? To be sure, even the word "difference" is a little vague. Would it make a difference if you didn't write your English essay? In the sense that your grade would tank, absolutely. Yet no one is under the delusion that English essays aren't nonsense.

For something to be meaningful, its purpose should be rooted in something external; it should not be self-referencing. Take the [Math Advance Problem Manager](https://mapm.mathadvance.org), for instance: it makes compiling contest drafts much easier for the [MAT](mat.mathadvance.org) team.[^people] But the reason you write an English essay is to get a good grade in the class. Yet the grade you get in the class is supposed to reflect how good your English essays are. It's cyclical.

[^people]:
    More careful readers might notice that I haven't explained why writing math contests is meaningful. It's because the MAT provides value to other people; through the contest, participants will actually learn something.

    Providing value to other people isn't the only way for work to be meaningful; the "meaning" an activity provides can just be the fun and frustration you have struggling through it (like in math contests), or the time you spend bonding with your friends over it (fishing, card games, Minecraft). But it is one of them.

But wait a minute. Grades get you into college, which determines your peer group. Surely that has to count for something.

Doing your English essay is certainly important because it indirectly determines your future. But does your English essay actually mean anything to you? How much do you care about the grade as opposed to what you've actually written? That to me is the difference between *meaningful* and *important*: do you care about the thing itself, or do you just care about the outcome?

None of this discernment will do you any good if you don't act on it. And acting on your tastes, I think, is really what it means to be picky.

Picky is a word with negative connotations. You don't want to be a picky eater or be picky about the color of your clothes. But that's being picky without discernment. You have to be picky and be discerning.

Whatever you're doing by default will most likely not be a good fit for you. Everyone else just makes too many assumptions about what you may or may not want to do. If you are picky but not discerning, you'll pick the wrong fights. If you are discerning but not picky, you'll be left feeling powerless. And if you are neither picky nor discerning, you'll get taken advantage of. Know what's worth fighting for, and be willing to fight for it.

It seems that you can't completely separate being picky about what's important and being picky about random things. Successful people tend to be eccentric, and they are quite picky about it. Otherwise they'd stop as soon as they got some pushback.

But to play devil's advocate, eccentric by whose standards? In the twentieth century there was a small group of people who fought against public smoking. They were certainly seen as weird during their time. Everyone else was smoking, so they ought to stop being a pain in the behind and just let people have their fun. But now we see that they were right.

Picky people aren't right all the time. The people who got Prohibition passed were very picky about the "morals" of the nation, and they ended up giving organized crime the greatest gift of their lives. Perhaps their mistake was being picky about other people whose actions did not concern them. Maybe that's the answer: be scrupulous when it concerns you or your work, and otherwise, live and let live.

So, to summarize: expose yourself to interesting things, see where it leads you, but know what matters and act on it. There's no guarantee that you'll become a lot smarter after doing this, but it might at least make it easier.
