---
title: Be around smart people
date: 2023-08-23
---

Everyone knows it's good to be around smart people. But why?

One common semi-misconception is that smart people will teach you things. Well, yes and no. They will talk about complicated ideas they understand, and you will not know a good chunk of them in advance. But come on. A five minute conversation could not possibly transmit all of the nuances of, say, the Spectral Theorem, or the requisite background in inner-product spaces, let alone its applications with positive operators, square roots, or SVD.

But being around smart people *primes* you to learn these things. You might be a curious individual learning about something like group theory, and you probably would not randomly encounter the Spectral Theorem before properly learning linear algebra, or the construction of the reals via Dedekind cuts before real analysis on your own. But if someone is telling you about orthonormal bases of eigenvectors and relating it to diagonal matrices, and afterwards you take linear algebra, it is hard not to go "Oh! So *that's* the Spectral Theorem. I get it now!" And even if you would've encountered the Spectral Theorem by looking around on your own (which is very possible --- this is why it is good to stay curious and do undirected reading on your own), there are many other things that you would not have seen.

You probably will not directly learn a lot just by hanging around smart people. You still have to stay curious, follow up on conversations, and read on your own. But if we consider intelligence to roughly be the ability to learn and solve problems quickly, then being around smart people actually makes you *smarter*. That on its own is not good for much. But if you also work hard, it just might provide the boost you need to learn --- and do --- great things.
