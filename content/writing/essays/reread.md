---
title: Reread
date: 2021-12-06
---

You know you've found something good when it's worth rereading.

It doesn't matter what it is. Even a press release handled well has a moment that you can't help but revisit. A police interview has that moment of catharsis that some of us love to watch over and over.

Hitchcock famously said that "suspense is when the spectator knows more than the characters." And indeed, anything worth reading --- or watching --- must have that same feeling. Not necessarily of suspense, mind you, but just the feeling that it's worth seeing the piece through even when you know its conclusion. After all, there are very few mystery novels that are stunning the first time around and absolutely boring the next.

That's not to say that reading something for the first time has no value --- obviously it does. So a good book, essay, movie, or game needs to be interesting enough to capture your interest the first time, even if you don't know or understand everything that's happened.

But there's a separate sort of value you get from rereading something. I feel that this isn't given as much attention as it really deserves. Even when it comes to documentation, how often something is reread tells you a lot. It tends to either be important or have some fundamental concept it's easy to get confused on.

Consider the `mount` command: is it `# mount /dev/sda2 /mnt` or is it `# mount /mnt /dev/sda2`? Turns out it's the former. And it has to be the former, because otherwise you can't run `mount` with one argument. And this makes mounting USBs without user intervention so much easier to handle because now you don't have to worry about where to mount it.

As a writer, I am good enough to make all my essays worth reading the first time around. But I don't think that's enough. In fact, I have several essays that I haven't published --- both on my old site and this one --- that I think were worth reading once. But that's the problem: it's _only_ worth reading once.

The day before publishing this essay I made a couple of user interface changes on this site. One of them was removing the boxes with the links in the writing subpages, but not the [main one](/writing). Why? Because no one will ever reread the descriptions.[^wrong] I might even remove it for the main writing page, because the sections are honestly pretty self-explanatory.

[^wrong]: Feel free to let me know if I'm wrong about this and you actually reread the descriptions. I won't revert the change either way since I've got enough writing to make the [all writing section](/writing/all) too visually bloated if I use those big boxes.

This is a new [idea](/writing/essays/framework) for me as well. Only for the last few weeks have I been writing with a focus on rereadability. The essays on my old site were littered with hyperlinks everywhere. I didn't notice at the time, but that was a good sign --- it meant that at least I was interested enough in my writing to reread it and reference it. And the reason to aggressively prune writing? To increase the proportion that's worth rereading. I've even started trying to minimize the number of footnotes in my essays, because often people don't find them worth reading even once, let alone rereading.[^mistake]

[^mistake]: Sometimes I wonder if it was a mistake to add footnote functionality to this site. I think the answer ends up being no, because footnotes give me space without breaking the flow of the text. And if you have more ideas than you have room for (which really should be the criteria for whether you write an essay or not), footnotes are a very helpful escape hatch.

Rereadability is the one word summary for what makes a good [hook](/writing/essays/hook). I don't know how rereadble the rest of this essay is, but I hope at least the hook is.[^conclusion]

[^conclusion]: I get away with bad conclusions because at the end of the piece there's nothing left to (re)read.
