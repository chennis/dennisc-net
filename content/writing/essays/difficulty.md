---
title: Hard Classes
date: 2022-01-26
---

Whenever someone asks how hard a class is, your mind should turn to two things: how much homework there is and how hard the tests are. Yet we often fail to separate the two, possibly because the two tend to be highly correlated. If the ests are harder, then of course there's more homework: the teacher has more material to cover, or it's more difficult, and thus you need more practice. Or the other way around: if there's a lot of homework, then there's more things to be tested on; ergo, the tests must be harder.

That's the common conception. But even if there's a relationship between the homework and tests, the two factors are totally orthogonal. Or at least, what they represent is orthogonal.

The amount of homework there is in a class represents its _required effort_. How likely you are to pass the class after putting in this required effort is the _risk_ of taking the class.

There is a lot of debate over how hard some classes are, like AP Statistics. But it turns out the disagreement is more narrow than its difficulty; it's based on the _risk_ of taking the class. No one, not even the people who perform terribly in the class, feels a particular need to sink dozens of hours a week into the class. You hit a barrier really quickly and returns begin to diminish. Rather, what differs from person to person is the risk of failing the tests; some people have less innate ability than others or less experience doing math, and so their risk is higher.

What's important is that these two components, risk and required effort, don't change much for a person. Sure, there is wide variance across different people, and these traits are not totally immutable. But over the span of a year, and maybe even all four years of high school, you do not often see significant variance. Particularly because the people in charge and the students themselves ignore that these two factors are what really matter, and instead waste time with a tutor without ever decreasing the inherent risk in harder classes or the amount of effort they require. For these traits change globally; it's a matter of habits and values, not of specific knowledge.

When people ask if a class is worth taking they are really asking about the potential reward. That is, what lies at the end if you manage to succeed? Maybe you learn something, or more frequently, it looks nice on your college applications. Implicitly they are also considering the cost of failure: tanking their grade point average. And really, that's all the factors that you need to make a decision: required effort, cost of failure, risk, and potential gains.[^order]

[^order]: I also think this is the order you should consider the factors in. If the required effort is too much, it doesn't really matter if you can afford to fail or if success is guaranteed. But earlier factors do not totally trump later ones, they are just weighted a bit heavier.
