---
title: Ideas and Mechanics
date: 2022-08-05
---

Teachers love to tell you that "your ideas are good, you just need to improve on your style" even though they're just saying it to be nice most of the time. But I do agree that there are two orthogonal components in writing.

I don't think the orthogonal components are ideas and style because so much of what falls under "style" really belongs to ideas. Rather, I think it's closer to ideas and mechanics.

Ideas are the core of the essay. I believe that a lot of the time choosing the right words falls into ideas. Often [you just need to choose the right words or concept for your readers to understand what you mean](write-smart#importing). A good heuristic for whether something falls into ideas is whether you have to concentrate and think about it. Ideas are things you can only come up with when you're in a flow state.

Mechanics, as the name suggests, is a more rigid process with less room for interpretation. You can improve the mechanics of an essay at just about any time. Often times when I am rereading my old writing I'll pull up my website and fix any mechanical mistakes I happen to come across. Sometimes it is as superficial as just fixing typoes or substituting one word for another.

Because writing is so broad --- you can write textbooks, essays, journal, and even text[^text] --- I'll limit this essay to writing meant for the general public. So I'm talking about biographies, pop science, and essays (like this one) but not journals or textbooks.

[^text]: Texting isn't always similar to writing (in fact most of the time it is not), but the thing I like about it is you can really easily shift to writing (mostly on accident). Textbooks explain concepts and I don't think explaining how to solve question 5 on the homework is significantly different. If you tell a story to a friend, is that really different from blogging or journaling either? True, texting is low focus and has low information density, but I still think it kinda counts sometimes.

	I don't think conversations are ever like writing (though conversations can spur good writing) because you don't really have the ability to refer to past bits of your conversation, whereas when texting you can reference a specific message.

This is how I think about my writing and what I do to get better. Maybe it will help you as well.

## Ideas

You can easily think of an idea to write about: what you did during school, had for lunch, or how the last sports meet went down. The problem is nobody cares what you had for lunch including yourself.[^exclude] What really takes effort is developing the meat of an idea, especially once you get in the habit of thinking and writing. Coming up with plausible ideas stops being the bottleneck pretty fast.

[^exclude]: If we exclude journaling, which this essay does.

Some ideas are not going to go anywhere.[^track] The main skill you need to develop with ideas is discernment, not coming up with them. Not only do you need to avoid pursuing unpromising ideas, but you also need the discernment to seize ideas worth writing about as well, especially since ordinary conversations and events can be really good catalysts for writing.[^ex]

[^ex]: I came up with the crux of this essay in a conversation with my friends from Math Advance.

[^track]: I wish I kept track of them because I'd have provided some examples if I did.

I can't tell you what ideas to think about, but I can offer a few heuristics.

The most obvious is that you have time to think, whether it's in the moments before you fall asleep, commuting to school, et cetera. If you find that you don't have time to think then make it. People waste an inordinate amount of time going through their phones or playing games. If you occasionally make an effort not to indulge in these distractions, you'll find that your mind will naturally wander to [whatever interests you most right now](http://www.paulgraham.com/top.html).

Sometimes I don't write my essays immediately when I think of them, but I just write up an outline with a pencil and leave it to be finished in a couple of days. This is mostly out of laziness but I find it serves as a good filter. You want to care and know enough about whatever you're writing on to be able to remember it for a few days.

I find that I can't actually think of many of the details of an essay even after I've thought of the idea. Most of the time the details I do write up are true enough to my initial idea, but sometimes the essay veers off into a whole different direction. At those times, I have to think about what the main idea actually is. At those times I tend to put the writing on pause, maybe browse the internet for a bit, and after about 40 minutes I come back with a renewed sense of purpose.

Above all, if you haven't written in a while or really need to write your college essays[^advice], just write. You only need to have a vague idea on what you're writing about, because writing is the process of roughly translating your thoughts to words. It's like deciphering an encoded message: even if you know how to translate it, you won't know what it means until you've done so.

[^advice]: I'd do well to take my own advice here.

If you want to develop your ability to think of ideas, you have to develop your ideas. The reason we write is because we don't have a good gauge of what we're thinking. How many times have you thought you had an idea for an essay, whether it be for school, college, or something else, only for it to fizzle out into nothing? But if you have all your ideas in front of you, it is really easy to [go back and make connections](reread).

## Mechanics

Knowing which words to pick or how to structure sentences is a more mechanical process. It's a little bit like programming: it requires creativity from you, but outside of a couple of key situations you do not need to actively be creative.

There's obvious things like making sure your sentences are grammatically correct or varying your choice of words. Strive for readability as well: make sure your sentences and [paragraphs](/writing/blog/paragraphs) aren't too long and [don't use bigger words than you have to](http://www.paulgraham.com/simply.html).

My personal philosophy for paragraph breaks is pretty simple: insert one whenever it makes sense. It's fine if you have really short paragraphs because of this.

For individual sentences, there's a main idea I want to convey and then [modifiers](modifiers) I want to add to make my statement as accurate as possible. Let's take the following sentence in this essay:

> People waste an inordinate amount of time going through their phones or playing games.

I originally was writing

> The amount of time people waste going through their phones is inordinate.

I say "was writing" because I never actually finished typing the sentence out; I immediately knew the wording was awkward. But I want to touch on why it's awkward: it's because the sentence builds towards the modifier as a conclusion. You want to either place the main observation or conclusion at the end of the sentence. Of course you can choose to style your sentences differently. But I believe that the best way to think about them is "how do I best fit my idea and modifiers together?"

If you look at really good writers, you'll notice they don't try to present ideas as something more than they really are. Modifiers are supposed to clarify your points, not hide your lack of knowledge.[^english] This is why choosing the right modifiers is often a question of ideas rather than words. I think skill with words and presentation is largely mechanical: you have an idea and its nuances, what's the best way to fit them together into a sentence? A paragraph? An essay?

[^english]: People misusing modifiers this way is probably why English teachers tell you to avoid them. But they sometimes shun modifiers so hard that they forget modifiers can enhance and clarify your writing rather than muddle it.

This is veering into the territory of ideas --- mechanics is just about fitting them together. So back to mechanics.

Surprisingly, the way you do mechanics doesn't really change from essay to essay. It doesn't even matter if it's your own writing. In English class I would edit at least half a dozen paper for each essay we were assigned (I got a reputation for being good at writing, though I don't really see how since I didn't show anyone my actual writing). Any mechanical changes I made to other people's essays would all fit the same style. Where ideas are supposed to be flexible, mechanics should be universal: your taste in mechanics should apply to your writing, my writing, and everyone else's writing. Is there any wording or syntax you disagree with in this essay? Why? And how can you apply the answer to improve your own writing?

Most people feel it's "inappropriate" to judge mechanics, partially because they vary from person to person and partially because they don't usually read with a critical lens. That's why the best way to improve your mechanics fast is to get friends who also write, proofread each other's work, and offer suggestions: it makes it appropriate and expected that you've got an eye out for mechanics because the only specific advice you can offer is on mechanics anyway.[^bad]

[^bad]: See [Two Kinds of Bad](two-kinds-of-bad): usually, fixable flaws are in the mechanics and unfixable flaws are in the ideas.
