---
title: Creating
date: 2023-01-15
---

Creating things is very difficult and frustrating. It's not a skill that can be taught. But it is a skill you can be *primed to develop*. This is the goal of this essay: to prime you to start building something that requires creativity.[^prime-squared]

[^prime-squared]: Well, really, this essay is priming you to be primed. Wanting to create something in general is not going to directly work, you have to have a specific idea. Which is why, if I wanted to be more accurate, I'd have to add another layer of indirection.

I personally write puzzles and math contests, so that is what I will use as my primary examples.

## Have an idea you want to share

The way you create something is by having a vague idea you want to share, and then continuing to make it more specific until you have a finished product.

What that means is, while wanting to create something is commendable, it is not enough. Instead of hyperfixating on a specific *goal*, like "I want to write a mock AIME", it is better to let it happen organically. Write problems based on ideas you are thinking about, and once you have amassed enough, then you can look at what you have and decide whether a mock AIME fits the ideas you've been exploring or not.

Conversely, if you have a specific idea, like "[how many consecutive zeroes can I get in a power of 5](https://artofproblemsolving.com/wiki/index.php/2016_USAJMO_Problems/Problem_2)", then quit reading this post or looking at general advice until you are done making whatever you want to make. It will still be here when you're done --- your idea, however, will probably disappear soon unless you flesh it out *now*.

## Empathy

When you are creating something, you need empathy for whoever is going to be experiencing it. This means when you are setting a puzzle, you cannot randomly spam Kropki dots until you stumble into something uniquely determined. You have to place each Kropki dot with intention: *what does this dot imply, and what will a solver make of it?* You can be a bit generous here, because not every solver will notice the logic you are trying to set, but an ideal solver should reasonably be able to solve it logically without too many layers of indirection in their deductions.

The same is true for writing: how will a reader interpret these words? Is the pacing alright? And the same is also true for math contests. For each problem, is there a logical, succinct, clean way to solve it? For the contest as a whole, can an inexperienced solver be engaged for the entire time? And can veteran math competitors still find value and joy in doing your contest?

You cannot just consider your goal to be "find something that *technically* works", whether it be a math problem that technically has a solution or a Sudoku puzzle that happens to be uniquely determined. You also need reason to believe that at least some of the people you're showing your puzzle to will discover the idea you were thinking about when you set the puzzle.

In a nutshell: how do you expect whoever's consuming your content to react?

## Surplus

Some people [can write 5 puzzles a week every week](https://artisanalsudoku.substack.com), while other people (and this is everyone when they start setting their first sudoku) have no idea how to even start. Or, how some people can solo write mock AMCs, while other people think math problems come out of thin air. So, what's the difference between someone who doesn't create anything and someone who can consistently create stuff? 

In my view, not much. It's not really habit or experience, because I can consistently program or write math problems or whatever for 6 months straight, but if I stop for two weeks to write my college essays[^ugh], I'm kind of back to square one.

[^ugh]: And this is why I hate college essays: they completely kill the momentum of high school seniors for a semester. No wonder everyone has senioritis second semester, sheesh.

I think the difference is just that *people who consistently create have a surplus of ideas*. What I mean is that, as I am setting a puzzle, or writing an essay, or programming a project, I have the next 5 things I plan to create roughly laid out. And then it will become 10 things, then 20 things, which I plan to do, and I will have to put many of them on the "I may get to this eventually" back-burner. If you need an idea to create something, then the way to consistently create things is to have ideas in reserve.[^log]

[^log]: This is why it's important to log your ideas. Secure your reserves!!

Notably, this surplus being the differences means *you have to keep your momentum going*. Consistency is largely not a product of experience, or even of any trait inherent to *you*, but a product of your previous ideas leading into your next ones. If at any point the ball stops rolling, you will have to get it started again. How experience helps is by making it easier to get started (again): when I first wrote math problems, it took me half a year to start writing good stuff, but now I think it'd only take a month or so to get back into the groove.[^on-off]

[^on-off]: I have been writing problems on and off (like once every 2 or 3 months), but that is a far cry from consistency. I am not consistently writing problems write now, because, well, there is [something else](https://puzz.dennisc.net) I am consistently writing.

## The difference between creating and solving

When you're doing a math test or solving a puzzle, you know for sure that there is some reasonable set of steps you can take to find the solution. But when you are (say) setting a puzzle, by default you have no idea whether there is a solution or not. You can develop intuition and tools to give you an indication, typically a negative one. Knowing that there are no solutions (i.e. your puzzle is overconstrained) is helpful, but it does not directly give you a solution.

This is important because it changes when you should give up. When you are doing a math test, the only reason to give up is if you don't know something. In other words, giving up indicates that there is a problem *that eventually should be fixed*. Giving up on *solving* a sudoku puzzle means you can improve your depth, or your consistency, or a variety of other things. But giving up on *setting* a puzzle means *there is a problem not worth fixing*.

Puzzles are inherently broken until you finish setting them; it's just a question of degree. So you have to ask yourself, how much work do I have to do to finish this puzzle, [is it easier to just start over](two-kinds-of-bad), and how good could the end result realistically be? And when you give up, the information it gives you is not always "you should focus on X". Often, it can be, "you should ignore idea Y because it turned out to be worthless in the end". Oh well.[^exec]

[^exec]: It's possible to just botch up execution of an idea! I tried (for about 10 or so minutes) to set a Kakuro Sudoku with a different layout before I started setting the one on my [puzzle page](https://puzz.dennisc.net/#kakuro-sudoku), and the puzzle I was trying to set would probably have been terrible, but that doesn't mean Kakuro Sudoku is a bad idea! It just meant that my layout was a bad idea. So when you are trying to figure out what you should do differently after giving up, think about the reason you gave up.
