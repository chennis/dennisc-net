---
title: Force Multipliers
date: 2022-01-06
desc: On working with more efficiency.
---

In the military, a force multiplier refers to something that makes something else more effective. A tool like GPS, on its own, will do absolutely nothing. But give this functionality to drones and you can [wreak havoc](https://www.cnn.com/2021/06/28/politics/us-airstrikes-new-iran-drone-attacks-avoid-surveillance/index.html).

No one is disputing the utility of the GPS. But for some reason, the effect of force multipliers on us, in a non-military context, is completely ignored.

Let's give a functional definition of a force multiplier first. A force multiplier is a tool that does not automaticlaly create anything of value, but can be used to create something of value with greater efficiency.

Take text editors, for instance. Text editors will not write code for you, but they will help you write code. There's a common refrain that text editors are just tools and they don't make you a better programmer. "Not everyone wants to set up a local TeX compiler!" they say. "Online editors just work." I've heard the same for programming languages and operating systems. Textbooks and studying strategies. It's all just personal preference. Hell, I've even heard people use this to justify typesetting books in Sans Serif fonts.

Of course you can write the same code with different text editors or the same program with a different language. Learn the same thing from a different textbook. Read the same book despite the differences in font selection. But these differences can make the experience more efficient or more cumbersome, in subtle or drastic ways.

Force multipliers have an effect that is incomparable to raw increases. Not better or worse, just not comparable --- at least, not in a general sense. This is the thing people tend to forget or just don't realize. You can't ask, "Will learning Vim or Python be more useful in general?" Because if you already know seven different programming languages, the eighth won't make as much of a difference, whereas if you don't even know how to program the whole point of Vim will be lost on you anyway.

You can still evaluate whether adding a force multiplier or raw ability is the better option in a specific scenario. This makes improvement somewhat of an optimization problem. Just be aware the answer can change based on the initial conditions.

## Force multipliers all the way down

Everything is a force multiplier, whose effectiveness can be increased or decreased like the others. Just the same as your fancy keyboard is useless if you can't even type fast, your fast typing abilities are useless if you don't have a keyboard.

Force multipliers can be categorized in a number of ways, but the most useful by far is mutual exclusivity. You can only type on one keyboard at a time. You can only use one text editor at a time. You can only read one book at a time. Now, this exclusivity is not literal; any smart-aleck could take two keyboards and begin slamming the keys. But it is effectively true; you won't type very fast on two different keyboards. Let's call such a group of force multipliers a _stratum_.

There are two types of tradeoffs that can be evaluated: intrastratum and interstratum. In English, that means within a stratum and between strata, respectively.

Intrastratum force multipliers can be compared in a more general sense. Vim or Emacs? A valid (though contentious) question, because you can't use both. Two spaces or four? Clearly the answer is two, but at least the options are in the same stratum. Replit or VSCode? What in tarnation are you talking about? One is an online editor+compiler suite and the other is just a local editor --- the two aren't even comparable.

The effectiveness of these force multipliers will vary from person to person. Someone might be very familiar with Vim and not know how Emacs works, or vice versa. Such a variance can lead to the misconception that there is no way to determine if a text editor is better than another, since everything is subjective.

As a thought experiment, consider any text editing program with the ability to save completely removed. Not only is it inferior to the original text editing program, it is inferior to practically _every_ text editing program. The ability to save is absolutely crucial, and an editor without it is worthless.[^value]

[^value]: This example is simplified, because part of the value of a broken object is how easily it could be fixed. If you wrote the world's best novel but accidentally substituted every capital O with a 0, it would still be worth a lot because you can easily find and replace these 0's.

Is it so hard to believe, then, that the ability to programmatically modify their text editor might be seen as essential for some people? Saving is part of what makes a text editor powerful --- without it, a text editor would have no power. Text editors can still have some power without configurable macros, but it certainly is less. And for some people, that's enough. But it would be wrong to say Vim is just as powerful than GEdit.

When making a general comparison between which force multiplier is more effective, you should almost always compare the maximum efficiency across people, not the minimum. Maybe the 90th percentile, because you should take into consideration how hard it is to use. There's a bit of nuance here because the users of powerful tools end up being self-selecting, but you should never use the average effectiveness over everyone, particularly not people who don't even use said tool.

Interstratum force multipliers have to be compared on a case-by-case basis. An example from math contests is the age-old debate between learning by reading books (theory) and working through contests or problem-sets (practice). Theory is the foundation for problems, and experience is crucial to do problems "in the wild." The people who stress the importance of doing books and those who focus on problems are both right.[^pride]

[^pride]: There is one group that is decidedly wrong, however: the people who are proud of never working through a book. It's one thing to never optimize a certain force multiplier; perhaps circumstances may have even made it the correct move. But it is another thing to wantonly ignore an entire class of force multipliers and suggest to other people they do the same.

Such a division between intrastratum and interstratum is not perfect. Judgements between any force multipliers will have to include some level of generality and some level of specificity, and they will never be perfect. And debates, both intrastratum and interstratum, both specific and general, can get ridiculous. But to argue that all intrastratum comparisons are subjective or that every interstratum question has a good answer is just as ridiculous.

## Organizations

Up until now I've been discussing force multipliers as having varying positive effects. But make no mistake: force multipliers can have a coefficient of less than 1.

I know a lot of people who refuse to work in groups because they think it's bullshit. To a large extent, they are right: most groups are astoundingly inefficient.

But how inefficient does a group need to be to make it not worth working in? There's a lot of things that are nice about it: being with other people[^unpleasant] and being able to increase your scope is nice. But you also lose some control in exchange for it. This threshold will vary person by person, but I'd approximate my answer to be about $1.1$, maybe even $1.2$, because I'm sort of a control freak when it comes to my work.

[^unpleasant]: On the flipside, if you're very inefficient, people will get mad at each other and it won't be very pleasant.

So what can you do to increase the coefficient of the force multiplier? It really depends on what kind of group you are. There are some groups that are essentially just brand names for a specific person, made in order to standardize both internal procedures and outward expectations. Single-person tutoring centers come to mind. Then there are groups that serve as a brand name multiple people can borrow from, each person adopting their own style on top of the established standard. (Again, tutoring centers).

What both of these groups have in common is that their value --- or potentially, their lack thereof --- is derived from their standards and procedures. Contrary to some beliefs, a bit of bureaucracy can be a good thing: it ensures a certain quality and consistency. Bad standards, however, are much more dangerous --- not only do they waste time, but they can individually force what could've been good products to become worse.

The people who do the best work are the ones that take pride in it. And if the force multiplier for the quality of work in an organization is less than 1, its efficiency be damned, the organization is completely screwed. So above all, you have to make sure people can do good work in such an organization, and only then should you focus on speed.[^math]

[^math]: For math organizations in particular, make sure you provide good tooling. For instance, MAST's website, LaTeX structure, and build script are all integrated, which makes it easier to write and update solutions. This encourages a high quality of solutions, which means that speed and quality are really not orthogonal.

But there's a final type of group: one that exists because this amassing of people is necessary to do something at all. By and large, math contest-writing groups fall into this category. It's very rare that one person has the technical, mathematical, logistical, and presentational skills to run the whole operation. These groups are a little more tricky to evaluate, because not having a force multiplier is not an alternative. Here the default coefficient isn't 1 --- it's 0.

But zero is clearly not the threshold. People still get annoyed with inefficient, mangled bureaucracies, even when there is no other alternative. The force multiplier still needs to have a coefficient greater than 1 for the organization to be healthy, because people can still tell if their work _could_ be better.

In fact, that gives a good definition for the coefficient of 1: as good as you feel you could've done on your own.[^arbitrary]

[^arbitrary]: Up until now, coefficients have been arbitrary, not in the least because some strata of force multipliers are necessary. You can't write text without a text editor, so what has a coefficient of 1? A text editor with no tools, like Notepad (Windows), TextEdit (Mac), or Nano/GEdit (Linux). This is because most people feel their base productivity is on a text editor with no tooling, and that any extra tools would increase productivity, rather than prevent its decrease.
