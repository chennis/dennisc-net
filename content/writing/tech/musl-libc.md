---
title: Check that your programs work in musl libc
date: 2022-06-07
---

Most programs are compiled with GNU's implementation of libc. This is not surprising, given that pretty much every Linux distribution uses glibc. In fact, the only ones I knew of that didn't were Alpine and Gentoo (which you can use glibc for anyway), and the only other one I should've known about was Void. (For completeness, here's a [list of musl libc distributions](https://wiki.musl-libc.org/projects-using-musl.html).)

So it's no surprise that a lot of programs break on musl libc. [calc](https://github.com/lcn2/calc), for instance, does not work on musl libc. (A good alternative is libqalculate, for everyone else on musl libc.) Even my programs had issues; [bootstrap](https://gitlab.com/chennisden/dotfiles/bootstrap) for my dotfiles and the MAT website[^source] used OpenSSL, which does not work well on Rust + Alpine. Thus I switched to rustls, which is better than OpenSSL anyways and works on all the hardware I need it to.

[^source]: Source will be released for MAT website when the rewrite is done.

There are plenty of merits to using musl libc which you can easily find online. The wisdom of using musl libc on a desktop environment may be debatable, since some software (mostly propreitary software that's been built against glibc, like Dropbox or Steam) won't run. This means you'll have to get glibc, one way or another. Thus Alpine, through no fault of its own, may not be suitable for casual desktop use if, for instance, your goal is to play games with minimal hassle: Ubuntu is a better choice for that.

In any case, if your programs do not support musl libc, it should not be a decision made out of ignorance. It's usually less of a big deal if you don't write a lot of C, but a lot of things in other languages can refuse to work for subtle reasons involving glibc/musl. And if you're, say, running Rust servers on Alpine, it's also important to understand that the reason your program is segfaulting is because Rust + OpenSSL does not work well on musl. If you want your programs to be reusable, part of that is making it portable, and if you want them to be portable, don't have them rely on glibc extensions.

## Sidenote

This post was almost called "Stop relying on glibc", but besides musl I know of no other libc's that are part of a major libre operating system.[^bsd] There are things that work in glibc and musl but not, say, dlls, but I could frankly care less about them.

[^bsd]: Note from the future: Embarrassingly enough I forgot about the BSDs and their own libc's. (Yes, I consider the BSDs to be major.)
