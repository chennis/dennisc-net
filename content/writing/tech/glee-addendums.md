---
title: Addendums to the design of a small self-hosted Git service
date: 2022-06-08
---

I've done a bit of thinking on what my [self-hosted Git service](/writing/tech/glee-design) might look at. I agree with all of the conceptual decisions I've made in my previous post, but I'm going to specify some of the technology I'm going to use.

For the API, I'm going to use Rust, period. I'm probably going to keep my Tide + Postgres + Redis stack that I'm using for MAT-web. (This means no wacky filesystem shenanigans that I described in the previous post.) I also am going to try adding GraphQL, since I think it'll be useful for the frontend.

I don't know about the frontend, which indicates that I really ought to learn how to do it. Maybe SvelteKit? It seems promising. Anyway, I'm going to set out a preliminary goal of 100KB or less per page request.[^compressed] If SvelteKit doesn't pan out, I might just server-side generate all of my pages, since

[^compressed]: This is compressed on a fresh page load.

- the frontend does not have to scale
- but it has to load fast for people using it.

I will have to test on some old hardware with a throttled connection, just to make sure

So when I call this Git service "small", it doesn't mean that the codebase necessarily will be small. Heck, look at CGit --- it has 10390 lines.[^wc]

[^wc]: This figure was gotten from `wc -l *.c *.h`. The actual significant lines of code is probably less than this.

With regards to authentication, I want to make things simple AND force people to do things the right way. To authenticate with the frontend, you will login with your credentials (duh). The only way I will allow authentication with Git is through an SSH key, because that is the only way you should be authenticating in Git to begin with. Username-password based solutions are hacky, stupid, and insecure.

Redirects are now low priority: in fact, I'll probably ship them out only after I start dogfooding (i.e. host the development of Glee on my Glee instance).

## CLI Tool

The Glee CLI is going to be written in Rust too, since I want to leverage [clap](https://docs.rs/clap/latest/clap). All the standard stuff, like making directories, creating a new repo, etc will be in it. However, the ergonomics are going to lean heavily on the filesystem model that Glee is using. This means that instead of `glee-cli group create PATH`, we will use `glee-cli mkdir PATH`. And of course, authentication will be based on SSH keys.

Of course, you will need some way of specifying a remote. There are two ways to do this: either have the user build from source and have the remote be specified compile-time (probably the worse option by far), or read the remote from a config file at runtime (probably what I will do).

A big feature that isn't in any other CLI I want to add is `fscopy`. Basically, if you run

```
glee-cli fscopy SOURCE TARGET
```

it should clone SOURCE from the remote to TARGET on your filesystem, using the default branches. The reason this is a big dealis because fscopy, unlike git clone, would be recursive by default.[^clone] This is definitely possible in GitLab (which is the only other Git hosting service that this would make sense for), but because GitLab doesn't lean into the "Git hosting service as filesystem" mentality that I will, I only thought of this while making the preliminary designs for Glee (i.e. writing this post).

There are other operations that fit better when you consider the fs model. For instance, in [GLab](https://glab.readthedocs.io/en/latest/) there is no way to change the permissions of a repo/group, and this is likely because it's hard to design. But for us, `glee-cli chmod -r PATH --view=user --push=admin` will do the job. (I intend for chmod to have an `-r` flag just because there are times you want to change permissions recursively and times you just want to change the default permissions for a directory. The difference between chmod and fscopy is that one is a write operation while the other is just a read operation.)

[^clone]: If you wanted something non-recursive you'd just use `git clone`.

By the way, here I used the name `glee-cli` because it's unambiguous what it does; it acts as the CLI for glee. But to make typing it easier, I might change it to `glee` or `gcli` when I actually write the CLI.

## Patches

Just like in Sourcehut, patches are expected to be sent by email. If you're hosting software that's big enough to need a coordinated way to send patches, it's probably a better idea to use something like Sourcehut.

For purposes totally unrelated to Glee, I've wanted to get a mailing list service for a while. Of course, reinventing the wheel here would be silly, so I will probably try [lists.sr.ht](https://lists.sr.ht/) (obviously it will be self-hosted). If nothing I find works well for me, I will write a mailing list myself, but I really would rather not resort to that.

## Licensing

AGPL-v3, duh. Yes, my previous stuff either uses MIT or BSD-3. But that's because its potential use cases were so limited that there really was no risk of exploitation, commercial or not. But since "Git hosting service" is quite broad, even though few people will use this specific design, any improvements better find their way back to me. There is no legitimate reason for a Git hosting service not to be open source.

## This will likely be my last big project

Given what's currently on my plate and the time I have before college, this will probably be the last big project I have time to do on my own, at least as a hobbyist. I don't even know if I want to work as a programmer as my day-job. Here's what's coming up before:

- Whatever maintenance of mapm is necessarily
- Maybe mapm-gui
- mat-web
- Maybe a MAST-web rewrite
- My geometry book (probably will happen in parallel --- not a software engineering project, but I will likely focus on that first)

I don't really want to do any of these besides mat-web and the geometry book, so I will probably get started on Glee soon. Making a new Git service will likely be a significant undertaking, even if I'm not offering it as a SaaS. I'm fine with this becoming my flagship software product for the next five years or so.
