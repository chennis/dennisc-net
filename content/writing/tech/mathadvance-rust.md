---
title: Math Advance is looking for Rust developers
date: 2022-06-24
---

The title of the post says that we are looking for Rust developers. Indeed, the language all Math Advance projects are written in is Rust, some of which interfaces with LaTeX. However, if you are willing to learn, Rust knowledge is not necessary! All that is needed is patience and diligence.

Due to some poor initial technical decisions by our tech lead (read: me), Math Advance has been straddled with a bit of tech debt. To summarize, we have stuff written in crappy not-Rust languages (typically Node JS) and want it written in Rust. It will be largely from scratch, though we will keep some legacy components of the MAT website (due to me not wanting to reimplement it all), so the tech debt is not something any new developers will be straddled with. There will be no crappy codebases to struggle through.

A list of Math Advance related projects, from my most to least recommended:[^note]

[^note]: Note this does not reflect my level of investment in the project, just how suitable I think it is for an outsider to Math Advance, or maybe even programming.

- Math Advance logo (not actually software): We have one for MAST and MAT but not one for MA as a whole. Currently I'm working through some prototypes but if you have any ideas yourself let me know!
- [mapm](https://sr.ht/~dennisc/mapm): mapm is the software we use to compile our contests. This is my top software recommendation because it's the one that sees the most real-world use today (read: I'm not the only one that uses it) and has the most flexibility in contributing! Some ways of contributing do not even require any Rust experience.

  The mapm ecosystem is currently in an alpha-state, where it is usable for our purposes and few others. Naturally, I would like that to change! Some areas of research include a GUI (which is helped by the fact I planned ahead and separated the core mapm library into another crate), a mapm-specific package manager for templates, more templates[^templates], and manpages/shell-completion for the CLI.

[^templates]: An AMC, AIME, MATHCOUNTS or Olympiad template would be helpful! Any popular contest format would really greatly benefit the ecosystem, so ARML, HMMT, etc are also on the table. It just requires some LaTeX skills.

- [mathadvance.org](https://mathadvance.org): When you go on our current landing page you learn absolutely nothing about Math Advance. Sexy, but impractical (and also JS! Blegh!) It's just going to be a Makefile and some custom config since a) it's gonna be a static site and b) I hate every static site generator out there. It's very simple, but design help is appreciated!
- [mast.mathadvance.org](https://mast.mathadvance.org): Currently only works with JS. I intend to rewrite this website so that everything is server-side generated, i.e. no JS. It will be one monolithic codebase written in Rust, much like what Glee is intended to be. No design skills necessary, we'll stick with the same design; it just needs to be re-implemented in not Tailwind.
- sysadmin: I've got a couple sysadmin scripts for MAST I should write/migrate. Improving the mailserver and setting up SourceHut mailing lists are on my to-do list as well; I'm currently using Mailcow and I don't like that it uses Docker, that kind of blackboxes things for me. Nothing super permanent can happen with regards to sysadmin though, because so much of what I need to do is based on GitLab which we'll hopefully replace with Glee soon anyways.
- glee: See my [initial post](/writing/tech/glee-design) and [followup](/writing/tech/glee-addendums). I did mention Svelte in the followup but my position has become more hardline: **no JS shenanigans**, which means no SvelteKit. We're going to use Rust (probably axum as web framework), plain old HTML, and CSS, tied together with a healthy dose of NGINX. The way I'm going to approach this project is very opinionated, so unless you are experienced, know my opinions, and share them, I recommend you look at something else. There's a lot in the Math Advance world worth looking at, eh?
- [mat.mathadvance.org](https://mast.mathadvance.org): It's currently a living nightmare; I don't recommend you work on this.[^no-help] We'll unfortunately still be using Javascript, because I am _not_ rewriting the frontend.

[^no-help]: Actually, I'm half-inclined to decline help from anyone who isn't already experienced with Rust AND Next JS, since this project won't teach you much valuable anyway. Or at least, the frontend won't.

Of course, the nature of open source is that it is distributed. You do not have to join the Math Advance team itself to send patches! Total commitment is not necessary, especially for mapm --- which is why, if you want to help but don't know what to pick, I seriously suggest you pick that.

If any of this interests you, shoot me an email! Alternate forms of contact OK too, if you have them.
