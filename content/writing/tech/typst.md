---
title: Consider learning Typst instead of LaTeX
date: 2024-07-21
---

If you do not already know LaTeX, you should seriously consider learning Typst in lieu of learning LaTeX.

- Typst is trivial to set up on your machine. LaTeX is a pain in the ass. Sure, you can install some partial distribution of LaTeX on your machine, but you are in for a world of hurt the second you want to install or upgrade a package. You can also install LaTeX as a user with the install script, which can be quite daunting for a complete computer newbie.
- Typst compiles much faster.
- And you don't have to use a single brain cell to get the compilation pipeline working. This is in stark contrast with LaTeX, which requires installing and configuring the external `latexmk` script just to get things to work right.
- No `\documentclass` or `\begin{document}` needed. Just type some text and equations and it will compile.
- Typst is *less* of a pain in the ass to write. Sure, I have to write `$x y$` instead of `$xy$` because otherwise Typst will think I am referring to a *variable* `xy`, but the commands make a lot more sense (e.g. `union` over `\cup`), I don't ever have to type a backslash, and writing a fraction does not make me sad because I no longer have to use `\frac`.
- It is much easier to design a simple document. My LaTeX document designs would take two days; my Typst design took me two hours.

Now, life isn't all sunshine and rainbows with Typst. I admit there are some pain points. `#show` rules behave somewhat strangely, especially if you are used to LaTeX. I also do not think it was trivial to do some more complex things, such as setting up footers to reflect the current chapter/section and then not having my footnote counter resets screw up the references. (By the way, the footer counter doesn't reset at all, which is frankly ridiculous.)

But here's the thing. You can install Typst right now, import [my package](https://gitlab.com/chennisden/dotfiles/typst/conversational)[^third-party], and compile a good-looking document in under ten minutes. Good luck doing that with LaTeX.

[^third-party]: I wish it were a lot easier to install packages from third-party repositories than it is right now. Unfortunately [this is not planned](https://github.com/typst/typst/issues/1554#issuecomment-1604330130), although the reasoning is very understandable.
