---
title: Beam's C implementation is done
date: 2022-04-30
---

After quite a bit of time, I've finally finished the C implementation for Beam as well. This is more portable for systems that do not have Rust, though I doubt that matters since LaTeX is quite hefty as well.

In any case, it was mostly for my own learning experience. And learn I did:

- File i/o
- Used `malloc()` and `free()`. Also learned about `realloc()`, though I intentionally did not fix segfaulting for slides/presentations that were too long (since long presentations are bad)- Learned about structs/enums/unions in C (and how to make a tagged union), though I never ended up using them
- The equivalent of `str.split()` in pretty much any other language
- Wrote a Makefile for the first time, learning about `.PHONY` and the `all` convention, including files like `config.mk`, and the `${DESTDIR}` variable.

And this is only a subset of what I applied in the code. Since it's my first program in C, the code probably sucks ass. If you want to make sure something works I'd use the Rust implementation because I don't suck as badly at Rust, this was just an educational exercise for me.
