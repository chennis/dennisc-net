---
title: Getting a new SSD and switching to Alpine
date: 2022-06-05
---

After working faithfully for 6 years, my Lenovo IdeaPad 710S laptop's SSD finally gave out: after using it for 30 minutes to an hour, I'd encounter i/o errors and the laptop would stop working pretty much entirely.

So, instead of throwing out the entire laptop, the rest of which worked fine, I did the sensible thing and just replaced the SSD. We were (as of the time of writing, "are") leaving for Chicago and so I wanted my SSD soon. Thus we went to Best Buy, even though I could've gotten a fully functional SSD for around $20 less if I bought it online. Since I was using a 256GB NVMe SSD, I figured I might as well get another NVMe SSD. The cheapest option was a 512GB SSD, so I ended up getting that.

Replacing the SSD was pretty straightforward: unscrew the cover, unscrew the SSD, replace the SSD, and put all the screws back in place.

If I wanted to, I could've cloned the old SSD onto the new one. However, I decided against it for several reasons:

- The old SSD is failing, so I had no idea how or even if the cloning would succeed.
- I'd have to put both SSDs inside the same machine (my desktop), and I didn't want to go to the trouble of disassembling that too.
- My old Bedrock system was pretty complicated, and I didn't feel like I gained anything from this extra complexity.
- All my important files are in Git repositories somewhere off my laptop.

So I decided to install Alpine Linux on my new SSD. In terms of how easy to setup Alpine is, it's a mixed bag in a couple of interesting ways. It's by no means "beginner-friendly", but for an experienced user the setup is so much easier than Arch Linux.

- Installation is straightforward: you just run `setup-alpine`. A little bit of Googling might be involved if you don't know what some of the options mean.
- Actually getting a graphical desktop environment and setting up networking is a bit hard, especially if your only experience is systemd/your distro took care of it for you before. I didn't try installing X11, but it seems fairly involved to get i3 working; getting Sway and Wayland to work, however, was much easier.

I like Alpine's apk a lot more than pacman. pacman has arcane letters where apk's operations are very simple to understand. I also like that apk also doesn't ask you to confirm where every other package manager does. I believe apk is also a bit faster, but in terms of how fast package managers are, apk and pacman are approximately as fast with everything else being a lot worse.

Alpine also takes a lot of cues from the BSDs. They use doas instead of sudo, and OpenRC instead of systemd. I like doas, and don't really care about OpenRC vs. systemd.

I've had pleasant experiences with Alpine, sans OpenSSL not working (I've replaced it with rustls in my projects). Someday I might migrate the Math Advance server to Alpine, in which case my dev and server environments will be identical.[^bsd]

[^bsd]: I've also been thinking about moving my server to FreeBSD, but the advantage of migrating to Alpine is that I can just export Mailcow from Ubuntu to Alpine. As far as I know, it's not possible to run Mailcow on FreeBSD.
