---
title: Setting up Gmail on aerc
date: 2023-04-27
---

A few days ago, I committed to CMU and got my university email: dennisc2@andrew.cmu.edu. I made an alias, reychen@cmu.edu, and if anyone wants to email my uni email I'd strongly prefer if you emailed that alias because come on, dennisc2 is probably the least sexy email address possible. I mean, why'd they have to put a *number* in my email?

Anyway, the Gmail web client is a disaster, so I wanted to get my CMU email on aerc. And since CMU uses Google Suites, the problem essentially reduced to getting Gmail to work on aerc.

## Tutorial

TL;DR follow the [commit message](https://git.sr.ht/~sircmpwn/aerc/commit/b0eaf5191c9bc5b128e347625b7eef998ba63c41) for the aerc patch that introduced aerc support. However, note that the token endpoint --- when URL-escaped--- is now `https%3A%2F%2Foauth2.googleapis.com%2Ftoken` instead.

Step-by-step:

- Go to [console.cloud.google.com](https://console.cloud.google.com) and create a project.
- Save the client id and client secret somewhere. (You can just copy it into a temporary text file or something.)
- Go to [developers.google.com/oauthplayground](https://developers.google.com/oauthplayground).
- Press the settings button and check "Use your own OAuth Credentials". Then, copy the client id and client secret that you saved.
- Authorize the `mail.google.com` API under "Gmail API v1".
- Exchange your authorization code for tokens and save your refresh token somewhere.

Now that you have all the information you need, go to aerc's `accounts.conf` and create a new entry with the following source:

```
source = imaps+oauthbearer://{username}:{refresh_token}@imap.gmail.com:993? \
client_id=XX&\
client_secret=XX&\
token_endpoint=https%3A%2F%2Foauth2.googleapis.com%2Ftoken
```

The `outgoing` entry is identical: just replace `imaps` with `smtps` and `imap.gmail.com:993` with `smtp.gmail.com:465` and you're good to go. Full example:

```
outgoing = smtps+oauthbearer://{username}:{refresh_token}@smtp.gmail.com:465? \
client_id=XX&\
client_secret=XX&\
token_endpoint=https%3A%2F%2Foauth2.googleapis.com%2Ftoken
```
