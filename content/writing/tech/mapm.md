---
title: Advanced mapm Tips
date: 2022-01-13
desc: Some tips on how to use the mapm contest creation software for more complex use cases.
---

This guide assumes you have a basic understanding of how mapm works. If you do not, read the documentation at [mapm.mathadvance.org](https://mapm.mathadvance.org/).[^pitch] Basic fluency with the command line is expected, and for Windows users, an understanding of the Unix commands _and_ the ability to translate them to whatever shell you are using.[^powershell]

[^pitch]: If you don't know what mapm is at all, it is a LaTeX contest compiler that allows you to create templates and compile contest drafts based on problem names, instead of manually changing LaTeX (in potentially many places).
[^powershell]: It is strongly recommended you use Powershell and not cmd.

## Collaboration

mapm is meant to be used with other people, but it is not opinionated on how you do this. There are two main ways: Dropbox[^cloud] and Git. I would recommend the former.

[^cloud]: "Dropbox" is just a catch-all for any cloud sync service.

The problem directory for a profile `foo` is `~/.config/mapm/problems/foo`. So you can symlink whatever directory contains your problems to the aforementioned path. For example, Math Advance uses `~/Dropbox/MAC/problems` as our problems directory, so we would run

```bash
ln -s ~/Dropbox/MAC/problems ~/.config/mapm/problems/mathadvance
```

as our profile is called `mathadvance`.

You can do something similar with Git; just don't forget to push and pull when appropriate.

## Multiple contest files for the same contest

mapm was designed for Math Advance contests, first and foremost --- this means it works best with MAT as well as "smaller" contests like the AMCs and AIME. Something expansive like HMMT would not fit well in a single contest file. Fortunately, there is a sane way to compile contests in mapm.

Let's use HMMT as an example. The Combinatorics and Geometry rounds have nothing to do with each other --- how hard problem X is on combo has no effect on how hard problem Y is on geo. Therefore, the two rounds can be considered independent, and thus should have separate contest files. It may also be worth noting that they do not need a separate template file; a `subject` template variable would suffice.

Here is a heuristic for this independence: do these rounds exist in separate PDFs? For ARML and MAT, where the subject and difficulty distribution of rounds should be taken as a composite, the entire individual test is in one PDF. Thus they should have one contest file.

## No solutions

Sometimes you might be lazy and not want to put solutions in a problem. If you're using a release of `mapm-cli` before v2.1.0, you can pass `solutions: []` into your problem file. If you're using a release after v2.1.0, the field is optional and the library will interpret no `solutions` field as an empty array.

## cli tricks

### Change default editor

When running `mapm edit`, the `EDITOR` environment variable is used. It is strongly recommended that you set it to a terminal editor, like nano or vim, so as to minimize context switching.

### List problem names

There is no built-in feature to list problems in the command line interface, but `mapm find show` should suffice; it finds every problem, and shows no tags. You may also pass filters into `find` for a filtered list.
