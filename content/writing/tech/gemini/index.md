---
title: Gemini
date: 2022-03-20
---

I now have a Gemini capsule![^website] The domain is the same: dennisc.net.

[^website]: The equivalent of a website in Gemini is a capsule.

If you don't know what Gemini is, it's an [internet protocol based on simplicity](gemini.circumlunar.space). It totally separates content (server responsibility) from style (client responsibility). So you can read my website however you want: through a terminal, a GUI, through light mode, etc.

I recommend [Amfora](https://github.com/makeworld-the-better-one/amfora) for terminal enthusiasts and [Lagrange](https://github.com/skyjake/lagrange) for everyone else; at least, those are the two I have looked at. I am sure other Gemini clients are also amazing, I just haven't had the chance to look at them yet.

<img src="lagrange.png" alt="Viewing dennisc.net with Lagrange">

<img src="lagrange-outline.png" alt="Lagrange outline">

Reproduced below is my first status update. I don't think I will mirror this content again, I want to keep some content gemini-exclusive.

<hr>

I've got a ton of things on my notepad's to-do list that I'd like to keep in a more permanent medium. (Some of them I have recently checked off.) So here they are, roughly separated by topic.

## Writing

I've had a couple of thoughts about pedagogy that I've never really gotten around to writing. One is that group lessons are probably more valuable than individual lessons, despite the latter costing far more per hour. I think this is only true in some limited circumstances: you have to be teaching smart people, they have to be self-motivated, and they have to be sufficiently extroverted. People tell you the main value in schools like MIT isn't really in the professors or the classes or whatever, but in the peer group. Doesn't the same hold true in general?

Recently I also finished porting over the ARU-Telescoping unit for MAST, and the solutions all follow the same template:

> Algebraic expression A can be rewritten as B, which obviously telescopes to value C.

There's no padding where I explain the motivation of the solution. There is motivation, obviously, but I have omitted it. I've considered adding it before, but in the end decided not to.

Whereas in my geometry textbook, I sometimes explain in great detail where each step comes from, even writing out obvious statements like

> Angle ABC and angle ABD are congruent where B, C, and D are all collinear.

Of course, this is in the context of a chain of equalities to show that some angle is equal to another, so said statement clarifies the chain of equalities a bit more.

Just like how some books and movies deliver a message more effectively by implying it, rather than outright stating it, I think some non-fiction texts might also do the same. Brevity can be good, and so can omitting details, since it can be used as a tool to magnify the most important parts of the text.

So there is something that makes it right to omit motivation for telescoping and makes it wrong to omit it for geometry. What is this difference? I don't know what it is, and if you have any ideas I'd be glad to hear them.

## Glee, the (hypothetical) Git server

=> https://www.dennisc.net/writing/tech/glee-design Design for a small-scale self-hosted Git service

Recently in my investigations with Gemini, I've heard whispers in the wind that authentication and primitive read/write access might be possible with Gemini. I don't have any ideas on how the details would work, but if all the pieces line up I am interested in writing a Gemini frontend for Glee in addition to (or possibly in replacement of) a web frontend.

The graphical frontend would likely just be for read access anyway, I don't care about giving people a GUI to set permissions or whatever. That stuff should be done with a command line that works around the API (even if the graphical read-only frontend is on Gemini, the API exposed for writing to permissions files would probably be on HTTP). But if too large a subset of functionality can't be easily created with Gemini, I think I will just not use Gemini for Glee, period.

## Math

I've been telling myself that I should look at the CAMO 2022 for a while, and maybe now is the time to do it, what with it being two days away from USAMO.

=> http://cmc.ericshen.net/CMC-2022/CAMO-2022.pdf CAMO 2022

I also need to finish solutions up for the GQU-Triangles MAST unit. That's sort of been blocking me from porting other over stuff.

Also, sometime after the USAMO, I would like to give a lecture on my ARU-Telescoping unit to the MAST kids. (I would've done it before the USAMO had I not qualified.)

## latex-lint

I have a small project called latex-lint which I use with Vim. Currently all it does is indent through latexindent + move punctuation outside of math mode. Eventually I want to add functionality to do this:

```Git diff replacing double dollar signs in LaTeX
-$$1+1=2$$
+\[1+1=2\]
```

=> https://gitlab.com/chennisden/dotfiles/latex-lint

## mapm

I've been telling myself for a long time that I need a GUI of mapm to really make adoption widespread across Math Advance. But it seems that even with just a CLI, adoption is pretty high across the active problem writers and test compilers. Many thanks to everyone who has begun using it, particularly Prajith Velicheti, Aprameya Tripathy, and Kelin Zhu, who have done a lot of work migrating problems to mapm, providing installation manuals and helping people through the process, and using it for their own projects. (I am somewhat deliberately obfuscating who each description is attributed to.)

Of course, I have also been writing my own manuals and maintaining the documentation for the project. Though mapm is still in an alpha stage right now, I think it is far better than any other publicly available alternative. I encourage you give it a try if you want to compile your own math contests. The (informal) spec is super short (just like Gemini) and very extensible.

=> https://gitlab.com/mapm/zero-to-mapm From Zero to mapm
=> https://mapm.mathadvance.org Comprehensive mapm documentation

Just like how I recently wrote a "Why latexmk" article on what niche the latexmk script fills, I should do the same for mapm. A lot of open source projects have this great, technical description of what it does, and no two sentence summary on why you would use it. mapm is among that number and hopefully I can change that soon.

=> /latexmk Why latexmk, mirrored on Gemini
=> https://dennisc.net/writing/tech/latexmk Why latexmk, on the web

As for the GUI: I wonder how much I need to write this utility, versus just wanting to write it to fully take advantage of our tags and buckets system. It is still my next major programming project, though I can already see I am inventing new things to do (like programming the build script for this gemlog) instead of actually writing the GUI.

If you are interested in using mapm, join us at Zulip. Unfortunately you are required to make an account, but unless I've screwed up, anyone should be able to join.

=> https://mapm.zulipchat.com mapm Zulip organization

## Linux (mostly Sway)

I had planned on switching to Sway on my desktop, but it uses Nvidia drivers. (I know, sue me, --my-next-driver-wont-be-nvidia.) Nouveau kind of really sucks and I don't care enough to get it to work properly. But my laptop, which is a Lenovo IdeaPad, is basically the Linux gods' gift to humanity. So I do have Sway on my laptop.

Previously my twm was i3, which is largely compatible with Sway, but there's a couple of little Sway-exclusive things (like the holding tray for windows) that I hadn't really configured yet. Plus, I'm using Polybar for desktop but that doesn't exactly work on Sway as far as I know (at least not without X, which sort of defeats the point).

Also I'm really surprised people recommend Arch so much over Alpine. I mean, I'm one of them, just because Arch is so much more popular that chances of you fixing a problem are much higher (everyone and their mom has a pacman post, the only thing it lags behind in converage is Ubuntu). But besides bleeding edge + more package coverage, I think Alpine is just better in a lot of ways. Although no AUR is a dealbreaker for a lot of people, I'm already using Bedrock, so really no reason not to have my base running Alpine. I won't deal with that now, but hopefully in a few months I look back on it and take care of that then.

<hr>

I hope you will consider installing Gemini and checking out my gemlog.[^lightmode]

[^lightmode]: Especially if you hate light mode. Because guess what! You can choose to use a dark mode client for Gemini, unlike in the web where it's super hacky. If light mode happens to be your cup of tea, you can use that too.
