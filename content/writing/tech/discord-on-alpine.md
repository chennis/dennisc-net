---
title: Discord on Alpine Linux
date: 2022-11-03
---

So I finally got Discord (and the official client, not 6cord or gtkcord) to work on Alpine Linux! Should work on any musl libc distribution, but I haven't tested for any besides Alpine.

For those of you who don't have experience with musl libc distributions or proprietary software, this is a big deal because **most proprietary software is compiled with GNU libc** (glibc for short). To vastly oversimplify, these programs will be expecting functionality provided by glibc and glibc only.

I don't love the official Discord client. It takes up way too much space and I like working in the terminal regardless (which is why I'm often on 6cord). But each of these unofficial clients have their own problems. 6cord takes a while to switch between channels and the voice chat doesn't work.[^voice] gtkcord4 is doing some weird stuff with keyrings (i.e. not remembering my login) and despite many attempts, it refuses to respect my theming preferences (maybe this is just a GTK problem). It's technically against TOS too (not like I really care though). Still, I'm looking for any excuse to ditch Discord, or at least its official client. Hopefully one day this will happen.

[^voice]: Chances are that it's just a me problem and I haven't configured voice properly. But still, it doesn't just "work by default" (not that the official client does either).

Anyway, here's how I did it:

- install gcompat and patchelf through `doas apk add gcompat patchelf`
- [install Discord as a tarball](https://discord.com/api/download?platform=linux&format=tar.gz) through `wget -O discord.tar.gz https://discord.com/api/download?platform=linux&format=tar.gz` (or click on the link)
- `cd` to whatever directory contains `discord-0.0.x.tar.gz` (`x` is the version number). This file will be called `discord.tar.gz` if you fetched it using `wget`, in which case replace `discord-0.0.x.tar.gz` with `discord.tar.gz`.
- `tar -xvzf discord-0.0.x.tar.gz`
- `cd Discord/`
- `patchelf --set-interpreter /lib/ld-musl-x86_64.so.1 Discord`
- `patchelf --add-needed libgcompat.so.0 Discord`

You may need to run `ldconfig` and/or reboot for this to work.

To run Discord, do `./Discord --no-sandbox`. I'm not sure if the `--no-sandbox` flag is necessary on Xorg-based systems, but it probably is required on Wayland (definitely is for Sway). Blame Discord for shipping an out-of-date version of Electron /shrug
