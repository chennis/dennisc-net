---
title: How to sync Dropbox on Alpine Linux with rclone
date: 2022-06-08
---

The official Dropbox client is fairly difficult to setup on Alpine. If all you need is file syncing and you're fine with using a CLI to do it, you can use [rclone](https://rclone.org/docs/).

Run

```
# apk add rclone
```

to install rclone.

Then run

```
$ rclone config
```

and you will be greeted with a series of interactive prompts.

First you start making a new remote.

```
n) New remote
d) Delete remote
q) Quit config
e/n/d/q> n
```

Then you need to give this new remote a name; it hardly matters what it is, but for this guide we'll just call it "dropbox".

```
name> dropbox
```

Then you will be asked what kind of storage you want to use. Look for the option that says "Dropbox" (as of the time of writing, it is 12), and input that.

```
Option Storage.
Type of storage to configure.
Choose a number from below, or type in your own value.
...
Storage> 12
```

Then you will be asked about "OAuth Client Id" and "OAuth Client Secret". Just leave those blank by pressing Enter as rclone instructs. You'll be asked if you wanted to edit the advanced config and use auto config too. Just press Enter to use the defaults, unless you have a reason not to.

Finally, you will be prompted to allow rclone OAuth access to Dropbox. Do this. If all went well, you should get a success message afterwards.

To sync, run

```
$ rclone sync dropbox: ~/dropbox
```

As long as this command is running, your local and remote Dropbox directories will be synced. If you want, you may set up a daemon to keep this persistently running.

More detailed usage instructions are also available at [rclone's Dropbox page](https://rclone.org/dropbox/), and of course, the man page for rclone.
