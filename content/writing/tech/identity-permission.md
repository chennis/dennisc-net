---
title: Let's separate identities and permissions
date: 2022-08-25
---

I use ZX2C4's [pass](https://www.passwordstore.org/) to manage my passwords and finally got around to changing my Gmail passwords. The night I did this (which is the night I'm writing this post) my dad needed to do something on my Google Fi, which requires him to be logged into my Google account.

Currently the easiest way to do this would be to share my password with him. Well, drats. How do I share my password with him? I'd have to send it to him (bad idea), he'd have to copy it from his texts (mildly annoying), then he'd have to switch to the Google Fi app (unnecessary context switch), and finally he would be able to paste it.

Technically in some cases it is possible to exercise proper access control, like with Google Brands (for YouTube and presumably Google Plus before it died), but Google sucks so much at documenting their services that even many veteran Google users (read: pretty much the entire world) don't have any idea something this essential exists. We need a culture shift where it's *expected* that different people can do something on the same account without sharing passwords.[^google]

[^google]: I don't think you should be able to share access to Gmail accounts, whereas there are other things where access sharing should work. This is one of the reasons I don't like monolithic services, they're awkward when they try to fit largely unrelated items together.

Maybe we *shouldn't* be sharing passwords. It's a terrible idea for security and it's inconvenient to boot. The same way that multiple people can SSH into the same user of a server, maybe we should allow multiple people (using separate *identities*) to perform actions with the same account (same *permissions*). This is what Glee does: there are multiple users (identities) which are assigned a role: admin or user, depending on the permissions I want them to have.

Access control is a hard problem. GitHub and GitLab manage to mess it up royally and make it a major pain to use, which is why you have people spending hours and even creating entire teams just to manage access control. It's also why I'm building my Git service with access control in the center of my mind. It might not be easy to design a solution that works elegantly, but can we please, please not have the default solution to "two people want to do things on one account" be "your use-case is unsupported (so you have to share passwords, deal with it)"?
