---
title: The midpoint of simplicity and complexity for web design
date: 2022-07-29
---

Take the union of the styling for dennisc.net, [mathadvance.org](https://mathadvance.org), and [the styling I've revealed for Glee's registration page](/writing/tech/glee-solidify). The style for these websites is unique --- I would be pretty surprised if you've seen a design that feels like this before.

Compare this to basically any site with a big, flashy marketing homepage. Think [GitHub](https://github.com), [GitLab](https://gitlab.com), or [Prisma](https://prisma.io) --- they all feel the same. Sure, there are lots of differences between the sites, but those mostly lie in the details.[^content] These websites do look nice and all, but in a sea of complicated web design a simple website can be a nice break from it all.

[^content]: It doesn't help that the content layout (or rather, the lack of content) is also common between these three sites; I'd personally appreciate a summary of the actual technical merits of the product rather than some faffing.

Why do all complex websites look the same? There's two reasons for that feeling. The first is that even if there are $10^5$ unique complex website designs versus $10^3$ simple ones, everyone is going for complex these days. The second is that people actually pay a lot of attention to complexity: it's one of the first things people notice, especially if they don't understand the nuances of web design. Think about art: if you don't really know art all that well, you probably mentally categorize paintings into three kinds: the normal ones, the super minimalist ones, and then the plain weird ones (think cubism).

Of course, there's another reason websites like mine feel so rare on the internet: because a lot of "simple websites" literally do not have any CSS styles whatsoever, so you get the ass-ugly default browser theme. Most the other people who design websites like these do it a la [perfectmotherfuckingwebsite](https://perfectmotherfuckingwebsite.com/) to make a point. So on personal and business websites, you either get people applying a lot of styling or none whatsoever.[^ugly]

[^ugly]: Or it can just be incredibly ugly like my personal website was circa 2018. It is possible to apply the wrong styles, and that's another reason you should be conservative about adding them: it's hard to mess up if you aren't really doing anything.

Yes, it may seem like your CSS isn't doing anything and you haven't really scratched your design itch if it has very few classes and declarations. But you know, not adding a bunch of stuff is a design decision too, and it's not one people take very often. There are very few websites with more than a page's worth of content that look like this, so whatever you make is bound to be unique. Plus, this may just be my personal taste, but I think websites with a tasteful amount of styling are the ones that look the best.
