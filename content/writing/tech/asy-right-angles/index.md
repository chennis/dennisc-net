---
title: Drawing right angles in Asymptote
date: 2022-08-09
---

## Background

Most people in AoPS use AoPS' olympiad.asy module to draw their diagrams. It provides functionality such as `circumcenter(A,B,C)`, `rightangle(A,B,C)`, and so on --- it provides methods for common olympiad geometry constructions.

But olympiad.asy is not a standard Asymptote module, it is an external library. And there already is a module in the standard library that does what olympiad.asy does, and arguably better. It is geometry.asy.

As an analogy, consider using an external library in your programming language of choice for string replacement rather than using the standard library. It doesn't matter if the external library is written well, because the standard library exists and it works well too. Why rely on an external dependency when you don't need to?

By the same reasoning, you should avoid using olympiad.asy. Its functionality can be completely replicated by the standard geometry.asy module, which you have installed with Asymptote whether you know it or not.

## Drawing a right triangle in geometry.asy

Unfortunately geometry.asy's `perpendicular` function is not the most intuitive. The function (simplified to only the arguments we care about) looks like this:

```latex
perpendicular(pair vertex, pair direction, pair offset)
```

The vertex is where the right angle will be drawn. The offset determines one of the non-base vertices of the "right triangle" (only the direction is relevant, not the magnitude), and the direction determines the angle bisector relative to the line formed by `vertex` and `vertex + offset`.

<img class="asy" src="rightangle.svg">

A really simple example:

```latex
import geometry;

perpendicular((0,0), NE, (1,0));
```

It will generate the right angle above (without any of the extra labels, lines, dots, etc).
