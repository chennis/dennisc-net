---
title: How I update my website on my server
date: 2022-07-31
---

If you're self-hosting, it's actually surprisingly hard to find the "right way" to update your website on push. Ideally you would be self-hosting Git and would be able to directly add a `recieve-pack` hook to build whatever you need server-side. But for the following reasons:

- not self-hosting a Git server,
- not having the necessary build tools on your server (and there might actually be good reason for this: do you really want to install TeX Live on your server?)

automatically building stuff on your website becomes a major PITA.

So let me suggest another approach: instead of automating "server receives stuff => server builds", why not automate "client pushes => client builds => client sends build output to server"? Particularly for smaller sysadmins like myself, the server is not actually more powerful than the client: I'm running a low-resource Vultr cloud instance, but other people might be self-hosting with their own hardware like a Raspberry Pi. For us, building client-side may actually be the better solution.

Here's how it works. I write a pre-push hook (I would've preferred post-push, but it doesn't really matter and post-push hooks don't exist) that looks like this:

```bash
make
tar -czhf dennisc.tar.gz build/*
scp dennisc.tar.gz HOST:dennisc.tar.gz
ssh HOST "tar -xvf dennisc.tar.gz && rm -rf dennisc.net && mv build dennisc.net"
```

where `HOST` is the IP of my server. In order to automatically authenticate into SSH, you need to set up an SSH key and configure `HOST` to use said SSH key in `~/.ssh/config`. What my SSH config looks like is

```
Host mathadvance
    Hostname 45.32.70.22
    IdentityFile ~/.ssh/mathadvance
    User mast
```

(I'm revealing the IP of our server since it's public information anyway: `ping dennisc.net` will get you it.)

Anyway, what this hook does is it builds my website using the Makefile, creates a zipped tarball from the build output, copies the tarball to my server, and extracts the tarball/moves it to the proper location. It's nothing complicated, though it is mildly annoying that I have to set this up on all my devices.
