---
title: An Embarrassing Gaffe
desc: I apparently let my domain expire. Oops.
date: 2023-09-02
---

So I didn't renew my domain on time, and apparently it expired. This is really bad, especially because there was some advertiser-parker on my website for several hours *after* renewal and because students were looking at my website for the application PDF. Oops.

The reason this happened was because I didn't have auto-renew on for God knows what reason, and I just let it get to today without paying any attention. Maybe I should have Namecheap send emails to the addresses I actually check (i.e. dennisc.net and mathadvance.org). Well, hopefully that's never happening again.
