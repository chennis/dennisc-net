---
title: LaTeX for Beginners
date: 2021-11-23
desc: A beginner-friendly introduction to the LaTeX typesetting language.
---

People who want a high-level overview of how TeX works may want to read [A High-Level Overview of TeX](/writing/tech/tex-overview) instead, or in addition to, this post. This guide prioritizes people who bother to locally install LaTeX [(do your homework, people)](https://web.evanchen.cc/faq-latex.html#L-2b). All the information for Overleaf users is still there, but it's included as an afterthought.

## Equations

Writing equations in TeX is quite simple. All you have to do is surround whatever math you want to write with the `$` symbol. For example, `$1+1=2$` will return $1+1=2$.

The main draw of TeX for math, though, is the ability to render complex equations with precise (even if not always so simple) syntax. For instance, instead of writing `(1+1/sqrt(x))/(2+1/(2x))` and expecting your reader to parse it, you write `\frac{1+\frac{1}{\sqrt{x}}}{2+\frac{1}{2x}}` and get $\frac{1+\frac{1}{\sqrt{x}}}{2+\frac{1}{2x}}$. The tradeoff, though, is that when you're _writing_ is much harder for you to parse. (Although over time you get used to it.)

When you're new to writing equations, expect to Google aggressively. The [AoPS LaTeX Symbols Page](https://artofproblemsolving.com/wiki/index.php/LaTeX:Symbols) provides a decent selection of some common symbols (as well as some symbols frankly no one cares about). Sometimes there's a symbol whose name you don't know, making it hard to Google --- in that case, use [Detexify](https://detexify.kirelabs.org/classify.html). There's also [the Comprehensive LaTeX Symbol List](http://tug.ctan.org/info/symbols/comprehensive/symbols-a4.pdf), which is exactly what it sounds like.

I suspect that most people reading this already know how to write equations in LaTeX though. (And if you don't, it's pretty easy to learn, it takes a week to master tops.) Generating a document, though, is a little harder...

## Document Structure

A LaTeX document is contained in a single `.tex` file, which has exactly one documentclass and any number of packages.[^simplification]

[^simplification]: This is a bit of a simplification. You can use `\input` to break apart long documents into several files (it's recommended for books, you have one file per chapter). Also, part of the document is also contained in the classes (`.cls`) and packages (`.sty`) you import.

Here is a simple example of a LaTeX document.

```tex
\documentclass{article}
\begin{document}
    Did you know that $1+1=2$?
\end{document}
```

Notice that we have a documentclass --- we must have one, though it does not need to be `article` --- and we surround the contents of the document with `\begin{document}` and `\end{document}`. All of this is mandatory.

Anything that comes before `\begin{document}` is part of the **preamble**. You should import packages in the preamble, after `\documentclass` but before `\begin{document}` What you do after `\end{document}` almost never affects the compilation process, but don't put anything there anyway.

This document is a bit boring. You can include a title, author, and date like such:

```tex
\documentclass{article}

\title{Title}
\author{Author}
\date{Date}

\begin{document}
    \maketitle

    Did you know that $1+1=2$?
\end{document}
```

### Sections

You can split your article into sections with the `\section` command like thus:

```tex
\begin{document}
\section{Did you know...}

Did you know that elephants are pink? That $1+1=2$?

\end{document}
```

## Compiling

Compiling is the process of turning your TeX document (the `.tex` file) into a PDF. The compiler has nothing to do with the text editor. Personally, I think you do yourself a disservice if you are invoking the compiler through a graphical interface; just type `pdflatex path/to/tex` in the terminal instead.

(I will not explain how to compile Overleaf documents since it is self-explanatory.)

To get the compiler, [install TeX Live](https://www.tug.org/texlive/acquire-netinstall.html). It really isn't that hard. If you're using Windows, it will take around 4 hours for a full install.[^fix]

[^fix]:
    But you can actually mitigate this. The reason the install is slow is because the TeX Live installer struggles to find the closest download mirror, so the mirror you get can be pretty far. This issue does not appear on Linux.

    A download mirror is just where you download TeX Live from. Obviously, the closer it is to you, the quicker the download will happen.

If you are using Linux, you have the choice of installing TeX Live with your package manager. I don't think it really matters if you use the "vanilla" installer or use your package manager, but if you want things to "just work" I recommend using the official installer.[^vanilla]

[^vanilla]:
    If you're using a Debian-based distro, you're going to get the 2019 version and miss a good number of packages. If you're using Arch, tlmgr is bugged. More importantly, however, you can customize the installation path, which is critical because TeX Live works much better if you install it in the `$HOME` directory rather than in root.

    For example, you might want `texliveonfly` to work without asking for `sudo` on every install.

Then, with your terminal in the **same directory** as your tex file, run `pdflatex file.tex` (where `file.tex` is replaced by the name of the actual file). You _can_ in theory run it in another directory but that's also where all the compiled files will go, and that's not great.

If you want to compile asymptote, make the `~/.latexmkrc` file and copy the following text into it:

```bash
sub asy {return system("asy \"$_[0]\"");}
add_cus_dep("asy","eps",0,"asy");
add_cus_dep("asy","pdf",0,"asy");
add_cus_dep("asy","tex",0,"asy");
```

Then, instead of running `pdflatex file.tex`, run `latexmk file.tex`.

### Where is latexmkrc? (On paths and the home directory)

When I write `~` in `~/.latexmkrc`, it means the home directory.[^home] This is a Unix convention and will not work for Windows! However, it is convenient as a shorthand so I use it anyway.

[^home]: This is typically `C:\\Users\USERNAME` for Windows, `/Users/USERNAME` for Mac, and `/home/USERNAME` for Linux.

If you open up a new shell, it will start in the home directory, you can just run `notepad .latexmkrc` (Windows) or `nano .latexmkrc` (Mac/Linux) from there and copy in the contents.

## Pretty Documents

If you want your documents to be pretty, typically you will want to import custom `.cls` or `.sty` files. The `scrartcl` class also has good defaults.

To import a custom `.cls` or `.sty` file, you have two choices:

1. Put the package in the same directory as the TeX file. (This is one of many reasons you should run LaTeX in the same directory as your TeX file.)

   I only recommend this if you plan on using the `.cls` or `.sty` file only for that project, AND that project is a throwaway.

2. Put the package in the `~/texmf/tex/latex` directory. (If this directory does not exist, create it.)

   A package does not have to be a `.cls` or a `.sty` file. It can be a [directory containing sty and cls files](https://gitlab.com/dennistex/bounce). In this case, you copy the entire directory into `~/texmf/tex/latex`.

I recommend using [https://gitlab.com/mathadvance/tex/mast](mast.cls), which actually has some (admittedly bad) documentation, or [https://gitlab.com/dennistex/bounce](bounce.cls). Note that the latter has no documentation, but the commands are all the same as `mast.cls`.

If you are using Overleaf, [upload the files](https://www.overleaf.com/learn/how-to/How_to_upload_files_to_a_new_project) to your project. You have to do this for every project, there's no way around it.

## Gotchas

Watch out for the following potential "gotchas" that might make your document unreadable.

- Never use double dollar signs like `$$1+1=2$$` for display math mode, always do `\[1+1=2\]`. [This is why.](https://tex.stackexchange.com/questions/503/why-is-preferable-to?noredirect=1&lq=1)
  - Unlike the double dollar sign, single dollar signs are still OK. You can write `\(1+1=2\)` instead of `$1+1=2$` if you want, but there are advantages and disadvantages either way. (I use the single dollar sign.)
- Equations are part of sentences. Do not write
  ```tex
  This is an equation.
  $$1+1=2$$
  It implies that $2+2=4$.
  ```
  Instead, write
  ```tex
  The equation
  \[1+1=2\]
  implies that $2+2=4$.
  ```
- Keep punctuation outside of math mode.
  - Don't write `$a, b, c$` because 1) the spacing will be off and 2) LaTeX might overflow that equation past the margins (when there's no reason it has to, since you can put a linebreak between two variables in this example). Instead, write `$a$, $b$, $c$`.
  - You do want to enclose intermediate punctuation (e.g. commas in between a list of variables) in math mode when it **does not** make sense for a linebreak to be inserted. For instance, `\{1, 2, 3\}` is perfectly fine because you cannot accept a linebreak anywhere in between.
- Never use two `\[\]` equations in a row; use
  ```tex
  \begin{align*}
  x+2&=y+4 \\
  x&=y+2
  \end{align*}
  ```
  instead.
- Never use `sin` instead of `\sin`, the difference is pronounced: $sin$ vs $\sin$.
- If you want a paragraph break between text, you must include an empty line in between:

  ```
  Paragraph 1

  Paragraph 2
  ```

  The following will just insert a space in between:

  ```
  Paragraph 1
  Paragraph 2
  ```

  and you will get `Paragraph 1 Paragraph 2` as your output.

- Never use `\\` or `\vspace` or anything similar in the document itself. Such manual formatting should be left to commands and environments, ideally defined in a class or package.
- Similarly, there are very few circumstances where you should manually be using `\noindent` in the document.
- Do not use `\textit` or `\textbf` for emphasis, use `\emph` instead and redefine `\emph` to look how you want it to.

You will notice that the second half relates to the WYSIWYM philosophy of LaTeX. That comprises more than half the bad TeX people write.
