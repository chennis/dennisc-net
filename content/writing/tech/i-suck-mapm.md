---
title: "I suck: a mapm bug"
date: 2022-02-18
---

With release 3.1.1 of the mapm library has come a critical fix to how filtering works. Before, the command would fail with `>1` filter; it would only care about the first. Here's the [commit](https://gitlab.com/mathadvance/mapm/lib/-/commit/bfd6d58df4f0b8e390c7ab71ca4d5c9523206dbf) that fixes this: the bug was so stupid that I can't even begin to cringe hard enough at it.

As a matter of analogy let's say you have an array of true/falses and want to determine whether every value is `true`. What I did was essentially (and this example code is written in `C`, though it is so basic I would be shocked if it didn't work in C++ and maybe even Rust) the following:

```c
void allTrue(bool bools[], int size) {
  for (int i = 0; i < size; i++) {
    return bool[i];
  }
}
```

and obviously this isn't what you want to do, you want

```c
void allTrue(bool bools[], int size) {
  for (int i = 0; i < size; i++) {
    if (!bool[i]) {
      return false;
    }
  }
  return true;
}
```

It's so obvious an error that there's no way this should've slipped past me, but it did. And this is the price I pay for getting lax on doctests. (So reminder to future self tomorrow, write the goddamn test for this function. Ya?)
