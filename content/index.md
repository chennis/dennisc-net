---
title: Dennis Chen's Website
desc: Webpage of Dennis Chen, CMU SCS class of 27.
---

Hey there! I'm Dennis, and I'm studying computer science and mathematics at Carnegie Mellon University.

My interests change pretty frequently, but some things I like to do as of now:

- Logic puzzles (mostly sudoku)
    - Puzzle construction
    - Sudoku competitions
    - [My Logic Masters Germany account](https://logic-masters.de/Raetselportal/Benutzer/eingestellt.php?name=dennischen)
- Teaching
    - Fall 2024: I will be a CS251 TA
    - I taught contest math through my first three years of high school and the fall semester of my freshman year at college
- Math
    - Abstract algebra
    - Set theory/formal logic/model theory
- Computers
    - Rust
    - Functional programming
    - Linux
    - Typesetting (LaTeX/Typst)
    - System administration
    - Networking protocols (e.g. HTTP)
    - HTML/CSS and web design
- Writing

I also did design work before. The first draft of the MAT logo was by me. William changed it to a purple color scheme, which is currently on the final logo. I also have amateurish book, handout, and CV designs, the last two of which have been used semi-often by other people.

## Hobbies

- [Variant Sudoku](/sudoku-variants) puzzles follow typical sudoku rules, but typically with a twist. Sometimes you may have to "color" cells that you know are the same but don't know the value of yet, and sometimes you will even have to construct the regions of the puzzle yourself. I have some [beginner recommendations](puzz-rec).
- [Hanabi](/hanabi) is a cooperative card game where you see everyone's cards except your own. You give clues (color, number) to your teammates so they learn the identity of their cards, and they give you clues so you learn your cards. The goal is to play stacks of cards like in solitaire --- the default is 5 stacks of 5 different colors, but this can change.
- K-Pop
    Favorite group: ATEEZ

    Favorite album: minisode 2: Thursday's Child

    Favorite song: Opening Sequence (TXT)

    - Seventeen
    - Stray Kids
    - Tommorrow X Together
    - ATEEZ
    - P1Harmony
    - aespa
- Rock climbing (I am not good)
