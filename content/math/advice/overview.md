---
title: What are math contests, and should you do them?
desc: An introduction to math contests and whether they might be a good fit for you.
---

## What are math contests?

> Math contests are tests with creative math questions that only use high-school level math.

This is how math contests are usually presented, but there are a couple of caveats. Most of the "high-school" math included in competitions is usually not taught in high school, unless your high school is the best of the best. Specifically, there are four main categories:

- Algebra
- Combinatorics
- Geometry
- Number Theory

Of these, combinatorics and number theory are the ones you are least likely to be familiar with. Combinatorics, otherwise known as "Counting and Probability," is about counting things. For example, "if Alice has four shirts and six pairs of pants, how many outfits consisting of one shirt and one pair of pants can she wear?" Number Theory is mostly about integers, whether it's remainders or the integer solutions to an equation. For example, "find all integer solutions to $n_1^4+n_2^4+\cdots+n_{14}^4=1599$" ([USAMO 1979/1](https://artofproblemsolving.com/community/c5h419967p2371549)).

If you do decide to participate in math contests, you will have to practice specifically for them. They are a kind of math different from high-school math, and even college math. Expect to learn a lot of new things along the way!

## Should you do math contests?

The short answer: only if you feel like it.

The long answer: There are a myriad of external reasons someone might want to do math contests, ranging from "it will help my college apps"[^deca] to "it will help me learn how to think". Some of these sound less noble, and some sound more noble, but at the end of the day you actually have to be drawn to solving math contest problems. Otherwise you will give up quickly. But fret not --- if you really enjoy it, the process will just keep getting easier.

[^deca]: If your goal is solely to get into college, consider spending your time in a better way. Your school's DECA club probably would not mind another member :)

Doing math contests requires a lot of dedication, and it is more than likely you will fail. I say this not to discourage you, but as a warning that the returns diminish very quickly. 5% of people who take the AMC 10 qualify for AIME (a fairly prestigious math invitational), which means the other 95% do not. Though how invested each competitor is varies, be aware that even getting any notable achievements may take two years --- and that's if you are lucky.

To make a decision, you should probably take a look at some problems and see if they interest you; looking up "AMC 10 problems" is a good way to start. But I want to make a case for math contest problems myself:

<details>
<summary>Andy the unicorn is on a number line from $1$ to $2019$. He starts on $1$. Each step, he randomly and uniformly picks an integer greater than the integer he is currently on, and goes to it. He stops when he reaches $2019$. What is the probability he is ever on $1984$?</summary>
<div>Note that Andy will go from a number between $1$ and $1983$ inclusive to a number between $1984$ and $2019$ inclusive exactly once. Moreover, this is the only step on which he can land on $1984$. So we examine this step and this step only. There are $2019-1984+1=36$ numbers Andy can land on between $1984$ and $2019$, and Andy has an equal chance of landing on any one of them, so the probability is $\frac{1}{36}$.</div>
</details>

Math contest problems are nice! Don't worry too much about the baggage of contests or results --- as long as you enjoy working on these sorts of problems, there is always a place for you in contest math.

### Math contests are one of many ways to do math

Math contests are a fun way to learn more math and sharpen your critical thinking skills, but they are by no means the only way. Studying undergraduate/graduate level math in high school, doing original research, and heck, even binging 3Blue1Brown are all valid ways to expand your knowledge in math. I know several people who, relative to their prowess in math research, did not excel in math contests.

There are plenty of reasons math contests might not be for you. Maybe it's the type of problems they ask. Or maybe it's the time limits.[^usamts] Whatever the case, you do not have to give up on math just because math contests are not your cup of tea. If you genuinely enjoy math, there are other avenues you can pursue.

[^usamts]: Although on that note, [USAMTS](https://usamts.org/) is an approachable proof-based contest where each round lasts a month, which really encourages thinking deep. You could probably start a round with no experience in math contests, and with enough work, solve a good number of problems by the end of the month. [KoMaL](https://www.komal.hu/home.h.shtml) is also a good long-form contest.
