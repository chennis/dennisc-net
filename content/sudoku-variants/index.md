---
title: Common Sudoku Variants
desc: The rules of common sudoku variants, like Killer Cages and Anti-knight.
---

<style>
img {
    margin-left: 0;
    width: 40%;
    height: 40%;
}
</style>

*Puzzle shown: [Classic Sudoku](https://app.crackingthecryptic.com/sudoku/94Qq6qGjh2) by Cracking the Cryptic*

There are 9 rows, 9 columns, and 9 $3\times 3$ regions. Each square must be filled with a digit from $1$ to $9$, and digits may not repeat in the same row/column/region.

In almost every sudoku puzzle, these rules will apply.

<img src="/sudoku-variants/row.png" alt="Image of Sudoku row.">

<img src="/sudoku-variants/col.png" alt="Image of Sudoku column.">

<img src="/sudoku-variants/region.png" alt="Image of Sudoku region.">

## Local Constraints

### Killer Cages

*Puzzle shown: [Killer Sudoku](https://www.gmpuzzles.com/blog/2022/05/killer-sudoku-by-serkan-yurekli-4/) by Serkan Yürekli*

Orthogonally connected cages are drawn. No digits in the same cage may repeat, and if a sum is indicated in the cage, the digits in the cage add up to said sum.

<img src="/sudoku-variants/killer.png" alt="Image of killer cage">

### Thermo

Digits must be strictly increasing from the bulb of the thermometer to the end.

<img src="/sudoku-variants/thermo.png" alt="Image of thermo">

### Arrows

Digits on the arrow must add to the digit in the circle. Digits may repeat if permitted by other Sudoku rules.

<img src="/sudoku-variants/arrow.png" alt="Image of arrow">

### Renban Lines

Renban lines must contain consecutive digits in any order.

<img src="/sudoku-variants/renban.png" alt="Image of renban line">

### German Whispers

Two directly connected digits on a German whisper must differ by 5 or more.

<img src="/sudoku-variants/german-whisper.png" alt="Image of German whisper">

### X/V

Two digits separated by an X must sum to 10. Two digits separated by a V must sum to 5. (These digits are orthogonally adjacent.)

<img src="/sudoku-variants/xv.png" alt="Image of XV pairs">

### Kropki

If two orthogonal digits are separated by a white dot, they must be consecutive. If two orthogonal digits are separated by a black dot, one must be double the other.

<img src="/sudoku-variants/kropki.png" alt="Image of Kropki dots">

### Little Killers

The digits on the diagonal indicated by a Little Killer must add to the sum indicated by the Little Killer. Digits on the diagonal may repeat.

<img src="/sudoku-variants/little-killer.png" alt="Image of a Little Killer">

### X-Sums

The first X digits in a row or column in the direction of an X-Sum, where X is the first digit seen in that row or column, must add to the indicated sum.

<img src="/sudoku-variants/x-sum.png" alt="Image of an X-Sum">

### Odd/Even

Gray circles must contain odd digits, and gray squares must contain even digits.

<img src="/sudoku-variants/odd-even.png" alt="Image of Odd/Even clues">

### Between Lines

Digits on a between line must lie between the digits on the endpoints. Digits may repeat on the line.

<img src="/sudoku-variants/between-line.png" alt="Image of a Between Line">

## Global Constraints

### Anti-knight

A square must contain a digit different from every other square a knight's move away (2 over and 1 across).

<img src="/sudoku-variants/anti-knight.png" alt="Image of Anti-knight constraint">

### Anti-king

A square must contain a digit different from every other square a king's move away (sharing a vertex).

<img src="/sudoku-variants/anti-king.png" alt="Image of Anti-king constraint">
