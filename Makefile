.PHONY: all html rss writing

all: build static/git-talk/slides.pdf static/resume.pdf static/reports/contest-creation.pdf static/reports/summer-mat-2022.pdf static dirs html rss writing

build:
	-mkdir build

clean: build
	rm -r build/*.html

dirs:
	-mkdir build/writing
	-mkdir build/writing/essays
	-mkdir build/writing/tech
	-mkdir build/writing/blog
	-mkdir build/code
	-mkdir build/math

html:
	./generate-html.sh

rss:
	./generate-rss.sh

# Generates summary pages in HTML
writing:
	./generate-writing.sh

static: build
	-ln -s $(PWD)/static/* build/
	cd static && find . -name '*.typ' -exec typst compile {} \; # Compile typst -> PDF

static/reports/contest-creation.pdf:
	cd static/reports && pdflatex contest-creation.tex

static/reports/summer-mat-2022.pdf:
	cd static/reports && pdflatex summer-mat-2022.tex

static/resume.pdf:
	cd static && pdflatex resume.tex

static/git-talk/slides.pdf:
	cd static/git-talk && rust-beam slides.beam
